﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class UiButtonHoverComponent : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private TextMeshProUGUI ButtonLabel;
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        ButtonLabel.color = DataValues.UiButtonHoverTextColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ButtonLabel.color = DataValues.UiButtonRegularTextColor;
    }
}
