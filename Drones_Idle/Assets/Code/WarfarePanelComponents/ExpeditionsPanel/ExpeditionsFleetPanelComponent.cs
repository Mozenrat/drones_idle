﻿using System.Linq;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ExpeditionsFleetPanelComponent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI FleetNameLabel;
    [SerializeField] private TextMeshProUGUI FleetCompositionLabel;
    [SerializeField] private TextMeshProUGUI FleetAttackPower;
    [SerializeField] private TextMeshProUGUI FleetHullStrengthTotal;
    [SerializeField] private TextMeshProUGUI FleetShieldCapacityTotal;
    [SerializeField] private TextMeshProUGUI FleetShieldHpCurrent;
    [SerializeField] private TextMeshProUGUI FleetHullHpCurrent;
    [SerializeField] private Image FleetShieldHpBar;
    [SerializeField] private Image FleetHullHpBar;

    [SerializeField] private TooltipProvider FleetCompositionTooltip;
    
    private VesselFleet thisFleet;

    private readonly StringBuilder stringBuilder = new StringBuilder();

    public void InitFleetPanel(VesselFleet fleetObject)
    {
        if (fleetObject == null) return;
        
        thisFleet = fleetObject;
        FleetCompositionTooltip.InitTooltip(GetTooltip);
        FleetNameLabel.SetText(fleetObject.FleetName);
        
        UpdateLabels();
    }

    public void UpdateLabels()
    {
        if (thisFleet == null) return;
        
        float shieldPercentFloat = Mathf.InverseLerp(0f, thisFleet.FleetShieldCapacity, thisFleet.CurrentShieldHp);
        int shieldPercentInt = Mathf.RoundToInt(shieldPercentFloat * 100f);

        float hullPercentFloat = Mathf.InverseLerp(0f, thisFleet.FleetHullHitPoints,
            thisFleet.CurrentHullHp);
        int hullPercentInt = Mathf.RoundToInt(hullPercentFloat * 100f);
        
        FleetCompositionLabel.SetText("Fleet Vessel Count: {0}", thisFleet.FleetTotalVesselCount);
        FleetAttackPower.SetText("{0:1}", thisFleet.FleetAttackPower);
        FleetHullStrengthTotal.SetText("{0:1}", thisFleet.FleetHullHitPoints);
        FleetShieldCapacityTotal.SetText("{0:1}", thisFleet.FleetShieldCapacity);
        
        FleetShieldHpCurrent.SetText("{0:1} ({1:0}%)", thisFleet.CurrentShieldHp, shieldPercentInt);
        FleetShieldHpBar.fillAmount = shieldPercentFloat;
        FleetHullHpCurrent.SetText("{0:1} ({1:0}%)", thisFleet.CurrentHullHp, hullPercentInt);
        FleetHullHpBar.fillAmount = hullPercentFloat;
    }

    private string GetTooltip()
    {
        stringBuilder.Clear();
        
        foreach (var fleetMember in thisFleet.FleetComposition)
        {
            var vesselInfo = DataInstanceFactory.GetCombatVesselInstance(fleetMember.Key);
            stringBuilder.Append(vesselInfo.VesselName).Append(": ");
            stringBuilder.Append(fleetMember.Value).AppendLine();
        }

        return stringBuilder.ToString();
    }
}