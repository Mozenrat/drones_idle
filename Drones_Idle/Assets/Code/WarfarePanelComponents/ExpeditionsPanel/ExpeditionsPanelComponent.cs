﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class ExpeditionsPanelComponent : MonoBehaviour
{
    [SerializeField] private TMP_Dropdown PlayerFleetSelector;
    [SerializeField] private TextMeshProUGUI WarzoneNameLabel;
    [SerializeField] private Button PreviousZoneButton;
    [SerializeField] private Button NextZoneButton;

    [SerializeField] private GameObject NoEnemyContainer;

    [SerializeField] private ExpeditionsFleetPanelComponent PlayerExpeditionsFleetPanel;
    [SerializeField] private ExpeditionsFleetPanelComponent EnemyExpeditionsFleetPanel;

    private WarfareController warfareController;
    private MothershipEntity mothershipEntity;

    private PlayerVesselFleet selectedPlayerFleet;
    private WarfareZone ActiveExpeditionZone => selectedPlayerFleet.ActiveExpeditionZone;
    
    private Dictionary<int, PlayerVesselFleet> playerFleetsIndex;
    private float labelUpdateInterval;
    private float labelUpdateIntervalMax = .02f;

    private void Awake()
    {
        warfareController = GlobalGameStateController.Instance.WarfareController;
        mothershipEntity = GlobalGameStateController.Instance.MothershipEntity;
        playerFleetsIndex = new Dictionary<int, PlayerVesselFleet>();

        selectedPlayerFleet = mothershipEntity.ActivePlayerFleets.First(); //TODO
        
        PlayerFleetSelector.onValueChanged.AddListener(OnPlayerFleetSelectionChanged);
        
        PreviousZoneButton.onClick.RemoveAllListeners();
        PreviousZoneButton.onClick.AddListener(MakePreviousZoneActive);
        NextZoneButton.onClick.RemoveAllListeners();
        NextZoneButton.onClick.AddListener(MakeNextZoneActive);
    }

    private void OnEnable()
    {
        PopulatePlayerFleetSelector();
        
        warfareController.OnWarfareNewFightStarted += OnWarfareNewFightStarted;
        warfareController.OnWarfareFleetDied += SyncUiOnDeadFleet;
        
        SyncWarfareLabels();
    }

    private void OnDisable()
    {
        warfareController.OnWarfareNewFightStarted -= OnWarfareNewFightStarted;
        warfareController.OnWarfareFleetDied -= SyncUiOnDeadFleet;
    }

    private void Update()
    {
        labelUpdateInterval -= Time.deltaTime;
        if (labelUpdateInterval < 0)
        {
            labelUpdateInterval = labelUpdateIntervalMax;
            PlayerExpeditionsFleetPanel.UpdateLabels();
            EnemyExpeditionsFleetPanel.UpdateLabels();
        }
    }

    private void OnPlayerFleetSelectionChanged(int value)
    {
        selectedPlayerFleet = playerFleetsIndex[value];
        SyncWarfareLabels();
    }

    private void OnWarfareNewFightStarted(object sender, WarfareController.OnWarfareNewFightStartedEventArgs e)
    {
        if (e.PlayerFleet != selectedPlayerFleet) return;
        
        EnemyExpeditionsFleetPanel.InitFleetPanel(e.EnemyFleet);
        
        NoEnemyContainer.SetActive(false);
        EnemyExpeditionsFleetPanel.gameObject.SetActive(true);
    }

    private void SyncWarfareLabels()
    {
        WarzoneNameLabel.SetText(ActiveExpeditionZone.ZoneName);
        PlayerExpeditionsFleetPanel.InitFleetPanel(selectedPlayerFleet);
        EnemyExpeditionsFleetPanel.gameObject.SetActive(false);
        NoEnemyContainer.SetActive(true);
    }

    private void SyncUiOnDeadFleet(object sender, WarfareController.OnWarfareFleetDiedEventArgs e)
    {
        if (e.PlayerFleet != selectedPlayerFleet) return;
        
        EnemyExpeditionsFleetPanel.InitFleetPanel(null);
        EnemyExpeditionsFleetPanel.gameObject.SetActive(false);
        
        WarzoneNameLabel.SetText(ActiveExpeditionZone.ZoneName);
        NoEnemyContainer.SetActive(true);
    }

    private void MakeNextZoneActive()
    {
        var found = warfareController.AvailableWarfareZones.Find(item =>
            item.ZoneTier == ActiveExpeditionZone.ZoneTier + 1);
        if (found == null) return;
        
        warfareController.ChangeActiveWarfareZoneForPlayerFleet(found, selectedPlayerFleet);
        SyncWarfareLabels();
    }

    private void MakePreviousZoneActive()
    {
        var found = warfareController.AvailableWarfareZones.Find(item =>
            item.ZoneTier == ActiveExpeditionZone.ZoneTier - 1);
        if (found == null) return;
        
        warfareController.ChangeActiveWarfareZoneForPlayerFleet(found, selectedPlayerFleet);
        SyncWarfareLabels();
    }

    private void PopulatePlayerFleetSelector()
    {
        playerFleetsIndex.Clear();
        
        List<TMP_Dropdown.OptionData> fleetOptions = new List<TMP_Dropdown.OptionData>();
        for (var i = 0; i < mothershipEntity.ActivePlayerFleets.Count; i++)
        {
            var fleet = mothershipEntity.ActivePlayerFleets[i];
            playerFleetsIndex.Add(i, fleet);
            fleetOptions.Add(new TMP_Dropdown.OptionData(fleet.FleetName));
        }

        PlayerFleetSelector.options = fleetOptions;
    }
}