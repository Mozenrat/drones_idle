﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManageFleetsComponent : MonoBehaviour
{
    [SerializeField] private GameObject FleetListItemsContainer;
    [SerializeField] private Button CreateNewFleetButton;

    [SerializeField] private FleetEditComponent FleetEditComponent;
    [SerializeField] private GameObject FleetEditBlank;
    
    [SerializeField] private FleetListItemComponent FleetListItemPrefab;

    private List<FleetListItemComponent> ExistingListItems;
    
    private void Awake()
    {
        ExistingListItems = new List<FleetListItemComponent>();
        PopulateExistingFleetsListItems();
        
        CreateNewFleetButton.onClick.AddListener(CreateNewFleet);
    }

    private void OnEnable()
    {
        FleetEditBlank.SetActive(true);
        FleetEditComponent.gameObject.SetActive(false);
    }
    
    private void CreateNewFleet()
    {
        FleetEditBlank.SetActive(false);
        FleetEditComponent.gameObject.SetActive(true);
        
        FleetEditComponent.InitFleetEditComponent(null, this);
    }

    private void PopulateExistingFleetsListItems()
    {
        foreach (var item in ExistingListItems)
        {
            Destroy(item.gameObject);
        }
        ExistingListItems.Clear();
        
        var fleets = GlobalGameStateController.Instance.MothershipEntity.ActivePlayerFleets;
        foreach (var fleet in fleets)
        {
            var listItem = Instantiate(FleetListItemPrefab, FleetListItemsContainer.transform);
            listItem.InitFleetListItem(fleet, this);
            ExistingListItems.Add(listItem);
        }
        CreateNewFleetButton.transform.SetAsLastSibling();
    }

    public void EditSelectedFleet(PlayerVesselFleet fleet)
    {
        if (fleet.ActiveExpeditionZone.ZoneTier != 0)
        {
            NotificationController.ShowNotification_Static(DataValues.ManageFleetsOnlyEditInStaging, true);
            return;
        }

        FleetEditBlank.SetActive(false);
        FleetEditComponent.gameObject.SetActive(true);
        FleetEditComponent.InitFleetEditComponent(fleet, this);
    }

    public void FinishedEditingFleet()
    {
        PopulateExistingFleetsListItems();
        FleetEditBlank.SetActive(true);
        FleetEditComponent.gameObject.SetActive(false);
    }
}