﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VesselQuantitySliderComponent : MonoBehaviour
{
    [SerializeField] private Slider QuantitySlider;

    private PlayerVesselFleet stagingFleet;
    private FleetEditComponent fleetEditReference;

    private VesselType thisSliderVesselType;
    private int thisSliderVesselQuantityTotal;

    private void Awake()
    {
        stagingFleet = GlobalGameStateController.Instance.MothershipEntity.MothershipFleetStaging;
    }

    private void OnDisable()
    {
        QuantitySlider.onValueChanged.RemoveAllListeners();
    }

    public void InitVesselQuantitySlider(PlayerVesselFleet fleetVessels, VesselType vesselType,
        FleetEditComponent fleetEditRef)
    {
        int leftSideQuantity = fleetVessels.FleetComposition.ContainsKey(vesselType)
            ? fleetVessels.FleetComposition[vesselType]
            : 0;
        int rightSideQuantity = stagingFleet.FleetComposition.ContainsKey(vesselType)
            ? stagingFleet.FleetComposition[vesselType]
            : 0;

        thisSliderVesselType = vesselType;
        fleetEditReference = fleetEditRef;
        thisSliderVesselQuantityTotal = leftSideQuantity + rightSideQuantity;
        
        QuantitySlider.minValue = 0;
        QuantitySlider.maxValue = thisSliderVesselQuantityTotal;

        QuantitySlider.value = leftSideQuantity;

        QuantitySlider.onValueChanged.AddListener(SliderValueChanged);
    }

    public void InjectValue(int value, int newMax)
    {
        QuantitySlider.maxValue = newMax;
        QuantitySlider.SetValueWithoutNotify(value);
    }

    private void SliderValueChanged(float sliderValue)
    {
        int intSliderValue = Mathf.RoundToInt(sliderValue);
        fleetEditReference.OnEntityRowDataChanged?.Invoke(null,
            new FleetEditComponent.EntityRowDataChangedEventArgs
            {
                VesselType = thisSliderVesselType,
                LeftSideQuantity = intSliderValue,
                RightSideQuantity = -2
            });
    } 
}