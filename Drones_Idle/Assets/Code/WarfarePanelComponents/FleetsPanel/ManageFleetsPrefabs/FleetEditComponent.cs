﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FleetEditComponent : MonoBehaviour
{
    public class EntityRowDataChangedEventArgs : EventArgs
    {
        public VesselType VesselType;
        public int LeftSideQuantity;
        public int RightSideQuantity;
    }

    [SerializeField] private TMP_InputField FleetNameEdit;
    [SerializeField] private GameObject FleetVesselsEditContainer;
    [SerializeField] private GameObject UnassignedVesselsEditContainer;
    [SerializeField] private GameObject VesselQuantitySliderContainer;
    [SerializeField] private Button ConfirmFleetChangesButton;

    [SerializeField] private SelectedFleetVesselsListItemComponent FleetVesselsListItemComponentPrefab;
    [SerializeField] private UnassignedVesselsListItemComponent UnassignedVesselsListItemComponentPrefab;
    [SerializeField] private VesselQuantitySliderComponent QuantitySliderComponentPrefab;

    public EventHandler<EntityRowDataChangedEventArgs> OnEntityRowDataChanged;
    
    private PlayerVesselFleet thisFleet;
    private MothershipEntity mothershipEntity;
    private ManageFleetsComponent manageFleetsComponentReference;

    private Queue<FleetEditEntityRow> fleetEditEntityPool;
    private List<VesselType> traversedTypesListNonAlloc;

    private Dictionary<VesselType, FleetEditEntityRow> activeEntityRows;

    private void Awake()
    {
        mothershipEntity = GlobalGameStateController.Instance.MothershipEntity;
        var enumLength = Enum.GetValues(typeof(VesselType)).Length;
        
        fleetEditEntityPool = new Queue<FleetEditEntityRow>(enumLength);
        traversedTypesListNonAlloc = new List<VesselType>(enumLength);
        activeEntityRows = new Dictionary<VesselType, FleetEditEntityRow>();
        
        //Fill ui objects pool with length based on possible vessel types
        for (var i = 0; i < enumLength; i++)
        {
            var selectedFleetListItem = Instantiate(FleetVesselsListItemComponentPrefab, FleetVesselsEditContainer.transform);
            var stagingListItem = Instantiate(UnassignedVesselsListItemComponentPrefab, UnassignedVesselsEditContainer.transform);
            var slider = Instantiate(QuantitySliderComponentPrefab, VesselQuantitySliderContainer.transform);

            FleetEditEntityRow entityRow = new FleetEditEntityRow(selectedFleetListItem, stagingListItem, slider, this);
            fleetEditEntityPool.Enqueue(entityRow);
        }
        
        OnEntityRowDataChanged += OnRowDataChanged;
        ConfirmFleetChangesButton.onClick.AddListener(SaveFleetChanges);
    }

    public void InitFleetEditComponent(PlayerVesselFleet fleet, ManageFleetsComponent manageFleetsComponent)
    {
        if (manageFleetsComponentReference == null) manageFleetsComponentReference = manageFleetsComponent;
        
        //Hide existing ui objects
        foreach (var row in activeEntityRows.Values) row.HideRow();
        activeEntityRows.Clear();

        if (fleet == null) 
        {
            var newFleet = new PlayerVesselFleet();
            newFleet.FleetName = "New Fleet";
            newFleet.ActiveExpeditionZone = WarfareData.GetStagingZone();
            
            fleet = newFleet;
        }
        
        thisFleet = fleet;
        FleetNameEdit.text = fleet.FleetName;

        PopulateListItems();
    }

    private void PopulateListItems()
    {
        traversedTypesListNonAlloc.Clear();
        
        //First render list items for existing vessel types in the selected fleet
        foreach (var thisFleetVessel in thisFleet.FleetComposition.Keys)
        {
            var entityRow = fleetEditEntityPool.Dequeue();
            int inputMax = thisFleet.GetVesselCount(thisFleetVessel) +
                           mothershipEntity.MothershipFleetStaging.GetVesselCount(thisFleetVessel);
            entityRow.InitRow(
                vesselType: thisFleetVessel,
                selectedFleetVesselQuantity: thisFleet.GetVesselCount(thisFleetVessel),
                inputMax: inputMax,
                selectedFleet: thisFleet);

            traversedTypesListNonAlloc.Add(thisFleetVessel);
            fleetEditEntityPool.Enqueue(entityRow);
            activeEntityRows.Add(thisFleetVessel, entityRow);
        }

        //Then render list items for all the rest vessel types that are in staging area only
        foreach (var stagingFleetVessel in mothershipEntity.MothershipFleetStaging.FleetComposition.Keys)
        {
            if (traversedTypesListNonAlloc.Contains(stagingFleetVessel)) continue;

            var entityRow = fleetEditEntityPool.Dequeue();
            int inputMax = thisFleet.GetVesselCount(stagingFleetVessel) +
                           mothershipEntity.MothershipFleetStaging.GetVesselCount(stagingFleetVessel);
            entityRow.InitRow(
                vesselType: stagingFleetVessel,
                selectedFleetVesselQuantity: thisFleet.GetVesselCount(stagingFleetVessel),
                inputMax: inputMax,
                selectedFleet: thisFleet);
            fleetEditEntityPool.Enqueue(entityRow);
            activeEntityRows.Add(stagingFleetVessel, entityRow);
        }
    }
    
    private void OnRowDataChanged(object sender, EntityRowDataChangedEventArgs e)
    {
        if (e.LeftSideQuantity == -2)
        {
            var totalCount = activeEntityRows[e.VesselType].GetTotalInitialVesselAmountInRow();
            activeEntityRows[e.VesselType].InjectRowValues(totalCount - e.RightSideQuantity, e.RightSideQuantity);
        }
        else if (e.RightSideQuantity == -2)
        {
            var totalCount = activeEntityRows[e.VesselType].GetTotalInitialVesselAmountInRow();
            activeEntityRows[e.VesselType].InjectRowValues(e.LeftSideQuantity, totalCount - e.LeftSideQuantity);
        }
        else
        {
            activeEntityRows[e.VesselType].InjectRowValues(e.LeftSideQuantity, e.RightSideQuantity);
        }
    }

    private void SaveFleetChanges()
    {
        if (!ValidateFleetChanges()) return;
        
        thisFleet.FleetName = FleetNameEdit.text;
        foreach (var activeEntityRow in activeEntityRows)
        {
            var shipType = activeEntityRow.Key;
            var amountChanged = activeEntityRow.Value.GetRowChangeDelta();
            
            if (amountChanged == 0) continue;
            
            if (amountChanged < 0)
            {
                thisFleet.RemoveShipFromFleet(shipType, Mathf.Abs(amountChanged));
                mothershipEntity.MothershipFleetStaging.AddShipToFleet(shipType, Mathf.Abs(amountChanged));
            }
            else
            {
                thisFleet.AddShipToFleet(shipType, amountChanged);
                mothershipEntity.MothershipFleetStaging.RemoveShipFromFleet(shipType, amountChanged);
            }
        }
        
        if (!mothershipEntity.ActivePlayerFleets.Contains(thisFleet))
        {
            mothershipEntity.ActivePlayerFleets.Add(thisFleet);
        }
        manageFleetsComponentReference.FinishedEditingFleet();
        mothershipEntity.OnPlayerFleetDataChanged?.Invoke(this, new MothershipEntity.PlayerFleetChangedEventArgs(thisFleet));
    }

    private bool ValidateFleetChanges()
    {
        if (FleetNameEdit.text == String.Empty)
        {
            NotificationController.ShowNotification_Static(DataValues.ManageFleetsNameCantBeEmpty, true);
            return false;
        }

        foreach (var existingFleet in mothershipEntity.ActivePlayerFleets)
        {
            if (existingFleet != thisFleet && FleetNameEdit.text == existingFleet.FleetName)
            {
                NotificationController.ShowNotification_Static(DataValues.ManageFleetsNotUniqueName, true);
                return false;
            }
        }
        
        int rowsChanged = 0;
        int allowedRemovedTypes = activeEntityRows.Count - 1;
        int actualRemoved = 0;
        foreach (var entityRow in activeEntityRows)
        {
            var amountChanged = entityRow.Value.GetRowChangeDelta();
            var vesselTypeRemoved = (entityRow.Value.GetInitialVesselAmountInSelectedFleet() + amountChanged) == 0;

            if (vesselTypeRemoved)
            {
                actualRemoved++;
                rowsChanged--;
            }
            if (amountChanged > 0) rowsChanged++;
        }

        if (actualRemoved > allowedRemovedTypes)
        {
            NotificationController.ShowNotification_Static(DataValues.ManageFleetsAtLeastOneShip, true);
            return false;
        }

        if (rowsChanged >= DataValues.FleetStatMaxVesselTypesInFleet)
        {
            NotificationController.ShowNotification_Static(
                $"{DataValues.ManageFleetsMaximumShipTypesExceeded}{DataValues.FleetStatMaxVesselTypesInFleet}",
                true);
            return false;
        }

        return true;
    }

    private class FleetEditEntityRow
    {
        private readonly SelectedFleetVesselsListItemComponent SelectedListItem;
        private readonly UnassignedVesselsListItemComponent UnassignedListItem;
        private readonly VesselQuantitySliderComponent BoundSlider;

        private readonly FleetEditComponent FleetEditComponentRef;
        
        public FleetEditEntityRow(SelectedFleetVesselsListItemComponent selectedListItem,
            UnassignedVesselsListItemComponent unassignedListItem,
            VesselQuantitySliderComponent slider,
            FleetEditComponent fleetEditRef)
        {
            SelectedListItem = selectedListItem;
            UnassignedListItem = unassignedListItem;
            BoundSlider = slider;

            FleetEditComponentRef = fleetEditRef;
            
            HideRow();
        }

        public void InitRow(VesselType vesselType, int selectedFleetVesselQuantity, int inputMax, PlayerVesselFleet selectedFleet)
        {
            SelectedListItem.gameObject.SetActive(true);
            UnassignedListItem.gameObject.SetActive(true);
            BoundSlider.gameObject.SetActive(true);
            
            SelectedListItem.InitVesselListItem(vesselType, selectedFleetVesselQuantity, inputMax, FleetEditComponentRef);
            UnassignedListItem.InitVesselListItem(vesselType, inputMax, FleetEditComponentRef);
            BoundSlider.InitVesselQuantitySlider(selectedFleet, vesselType, FleetEditComponentRef);
        }

        public int GetRowChangeDelta()
        {
            return SelectedListItem.PeekDeltaFromInitialQuantity;
        }

        public int GetInitialVesselAmountInSelectedFleet()
        {
            return SelectedListItem.PeekInitialQuantity;
        }

        public int GetTotalInitialVesselAmountInRow()
        {
            return SelectedListItem.PeekInitialQuantity + UnassignedListItem.PeekInitialQuantity;
        }

        public void InjectRowValues(int leftSideValue, int rightSideValue)
        {
            SelectedListItem.InjectValue(leftSideValue);
            UnassignedListItem.InjectValue(rightSideValue);
            BoundSlider.InjectValue(leftSideValue, leftSideValue + rightSideValue);
        }

        public void HideRow()
        {
            SelectedListItem.gameObject.SetActive(false);
            UnassignedListItem.gameObject.SetActive(false);
            BoundSlider.gameObject.SetActive(false);
        }
    }
}