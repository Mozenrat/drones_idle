﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectedFleetVesselsListItemComponent : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Image ItemBackgroundImage;
    [SerializeField] private TextMeshProUGUI ListItemVesselNameLabel;
    [SerializeField] private TMP_InputField ListItemVesselQuantityInput;

    private ICombatVessel thisVesselInstance;
    private FleetEditComponent fleetEditReference;

    public int PeekDeltaFromInitialQuantity => int.Parse(ListItemVesselQuantityInput.text) - initialQuantity;

    public int PeekInitialQuantity => initialQuantity;
    
    private int initialQuantity;
    private int inputMaximum;

    private void OnDisable()
    {
        ListItemVesselQuantityInput.onValueChanged.RemoveAllListeners();
    }

    public void InitVesselListItem(VesselType vesselType, int quantity, int inputMax, FleetEditComponent fleetEditRef)
    {
        inputMaximum = inputMax;
        initialQuantity = quantity;
        thisVesselInstance = DataInstanceFactory.GetCombatVesselInstance(vesselType);
        fleetEditReference = fleetEditRef;
        
        ListItemVesselNameLabel.SetText(thisVesselInstance.VesselName);
        ListItemVesselQuantityInput.text = quantity.ToString();
        
        ListItemVesselQuantityInput.onValueChanged.AddListener(QuantityChanged);
    }

    public void InjectValue(int value)
    {
        ListItemVesselQuantityInput.SetTextWithoutNotify(value.ToString());
    }
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        ItemBackgroundImage.color = DataValues.RecipeListItemBackgroundHoverColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ItemBackgroundImage.color = DataValues.RecipeListItemBackgroundRegularColor;
    }
    
    private void QuantityChanged(string value)
    {
        if (ListItemVesselQuantityInput.wasCanceled) return;

        if (int.Parse(value) > inputMaximum)
        {
            ListItemVesselQuantityInput.text = inputMaximum.ToString();
            return;
        }

        fleetEditReference.OnEntityRowDataChanged?.Invoke(null,
            new FleetEditComponent.EntityRowDataChangedEventArgs
            {
                VesselType = thisVesselInstance.VesselType,
                LeftSideQuantity = int.Parse(value),
                RightSideQuantity = -2
            });
    }
}