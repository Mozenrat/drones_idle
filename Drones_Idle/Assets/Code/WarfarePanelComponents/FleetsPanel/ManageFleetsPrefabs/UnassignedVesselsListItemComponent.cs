﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UnassignedVesselsListItemComponent : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Image ItemBackgroundImage;
    
    [SerializeField] private TextMeshProUGUI ListItemVesselNameLabel;
    [SerializeField] private TMP_InputField ListItemVesselQuantityInput;

    private ICombatVessel thisVesselInstance;
    private PlayerVesselFleet stagingFleet;
    private FleetEditComponent fleetEditReference;
    private int inputMaximum;
    
    public int PeekInitialQuantity => stagingFleet.GetVesselCount(thisVesselInstance.VesselType);
    
    private void OnDisable()
    {
        ListItemVesselQuantityInput.onValueChanged.RemoveAllListeners();
        GlobalGameStateController.Instance.MothershipEntity.OnPlayerFleetDataChanged -= OnPlayerFleetDataChanged;
    }
    
    public void InitVesselListItem(VesselType vesselType, int inputMax, FleetEditComponent fleetEditRef)
    {
        inputMaximum = inputMax;
        thisVesselInstance = DataInstanceFactory.GetCombatVesselInstance(vesselType);
        stagingFleet = GlobalGameStateController.Instance.MothershipEntity.MothershipFleetStaging;
        fleetEditReference = fleetEditRef;
        
        ListItemVesselNameLabel.SetText(thisVesselInstance.VesselName);
        ListItemVesselQuantityInput.text = stagingFleet.GetVesselCount(thisVesselInstance.VesselType).ToString();
        
        ListItemVesselQuantityInput.onValueChanged.AddListener(QuantityChanged);
        GlobalGameStateController.Instance.MothershipEntity.OnPlayerFleetDataChanged += OnPlayerFleetDataChanged;
    }
    
    public void InjectValue(int value)
    {
        ListItemVesselQuantityInput.SetTextWithoutNotify(value.ToString());
    }
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        ItemBackgroundImage.color = DataValues.RecipeListItemBackgroundHoverColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ItemBackgroundImage.color = DataValues.RecipeListItemBackgroundRegularColor;
    }
    
    private void OnPlayerFleetDataChanged(object sender, MothershipEntity.PlayerFleetChangedEventArgs e)
    {
        if (e.PlayerFleet == stagingFleet)
        {
            var currentQuantity = int.Parse(ListItemVesselQuantityInput.text);
            var newQuantity = stagingFleet.GetVesselCount(thisVesselInstance.VesselType);

            if (currentQuantity != newQuantity)
            {
                ListItemVesselQuantityInput.text = (currentQuantity + 1).ToString();
            }
        }
    }
    
    private void QuantityChanged(string value)
    {
        if (ListItemVesselQuantityInput.wasCanceled) return;
        
        if (int.Parse(value) > inputMaximum)
        {
            ListItemVesselQuantityInput.text = inputMaximum.ToString();
            return;
        }
        
        fleetEditReference.OnEntityRowDataChanged?.Invoke(null,
            new FleetEditComponent.EntityRowDataChangedEventArgs
            {
                VesselType = thisVesselInstance.VesselType,
                LeftSideQuantity = -2,
                RightSideQuantity = int.Parse(value)
            });
    }
}