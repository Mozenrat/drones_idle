﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FleetListItemComponent : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    [SerializeField] private Image ListItemBackground;

    [SerializeField] private Image FleetCommanderIcon;
    [SerializeField] private TextMeshProUGUI FleetNameLabel;
    [SerializeField] private TextMeshProUGUI FleetVesselCountLabel;
    [SerializeField] private TextMeshProUGUI FleetStatusLabel;
    
    private ManageFleetsComponent manageFleetsReference;
    private PlayerVesselFleet thisFleet;

    public void InitFleetListItem(PlayerVesselFleet fleet, ManageFleetsComponent manageFleetsComponent)
    {
        thisFleet = fleet;
        manageFleetsReference = manageFleetsComponent;
        
        UpdateLabels();
    }

    private void OnEnable()
    {
        GlobalGameStateController.Instance.MothershipEntity.OnPlayerFleetDataChanged += OnPlayerFleetChanged;
        if (thisFleet != null) UpdateLabels();
    }

    private void OnDisable()
    {
        GlobalGameStateController.Instance.MothershipEntity.OnPlayerFleetDataChanged -= OnPlayerFleetChanged;
    }
    
    private void OnPlayerFleetChanged(object sender, MothershipEntity.PlayerFleetChangedEventArgs e)
    {
        if (gameObject.activeSelf && e.PlayerFleet == thisFleet) UpdateLabels();
    }

    private void UpdateLabels()
    {
        FleetNameLabel.SetText(thisFleet.FleetName);
        FleetVesselCountLabel.SetText("Vessel Count: {0}", thisFleet.FleetTotalVesselCount);
        if (thisFleet.ActiveExpeditionZone.ZoneTier != 0)
        {
            FleetStatusLabel.SetText("Status: In Expedition");
            FleetStatusLabel.color = DataValues.FleetListItemStatusInExpeditionColor;
        }
        else
        {
            FleetStatusLabel.SetText("Status: Staging Area");
            FleetStatusLabel.color = DataValues.FleetListItemStatusInStagingAreaColor;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ListItemBackground.color = DataValues.FleetListItemHoverBackgroundColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ListItemBackground.color = DataValues.FleetListItemBackgroundColor;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        manageFleetsReference.EditSelectedFleet(thisFleet);
    }
}