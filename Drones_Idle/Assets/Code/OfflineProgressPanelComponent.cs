﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OfflineProgressPanelComponent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI ErrorInfoLabel;
    [SerializeField] private Button ContinueButton;

    private void Awake()
    {
        ContinueButton.onClick.AddListener(OnContinueButton);
        GetComponent<Canvas>().sortingOrder = 10;
    }

    private void Start()
    {
        if (GlobalPersistenceController.IsNewGameStarting)
        {
            gameObject.SetActive(false);
        }
        else
        {
            InitGameLoadedPanel();
        }
    }

    private void InitGameLoadedPanel()
    {
        gameObject.SetActive(true);
        if (GlobalPersistenceController.GameLoadingException != null)
        {
            ErrorInfoLabel.SetText(GlobalPersistenceController.GameLoadingException.Message + "\n" + 
                                   GlobalPersistenceController.GameLoadingException.StackTrace);
        }
        else
        {
            ErrorInfoLabel.SetText("No Exceptions while loading game\nTODO: offline progress on this panel"); //TODO:
        }
    }

    private void OnContinueButton()
    {
        gameObject.SetActive(false);
    }
}