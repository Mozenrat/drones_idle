﻿using System;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DroneUiComponent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI UnoccupiedDronesText;
    [SerializeField] private TextMeshProUGUI BandwidthUsageText;
    [SerializeField] private Button ButtonPlus;
    [SerializeField] private Button ButtonMinus;

    private PlanetContents thisPlanet;
    private bool initialized;
    private int numFreeOld = -1;
    
    private readonly StringBuilder stringBuilder = new StringBuilder(300);

    private float labelUpdateInterval;
    private float labelUpdateIntervalMax = .09f;

    public void InitDroneDropdown(PlanetContents planet)
    {
        thisPlanet = planet;
        ButtonPlus.gameObject.SetActive(false);
        ButtonMinus.gameObject.SetActive(false);
        var tooltipProvider = UnoccupiedDronesText.gameObject.AddComponent<TooltipProvider>();
        tooltipProvider.InitTooltip(GetTooltipString);
        initialized = true;
    }

    public void InitDroneDropdown(PlanetContents planet, Action plusAction, Action minusAction)
    {
        thisPlanet = planet;
        initialized = true;

        ButtonPlus.onClick.AddListener(() => plusAction());
        ButtonMinus.onClick.AddListener(() => minusAction());
        var tooltipProvider = UnoccupiedDronesText.gameObject.AddComponent<TooltipProvider>();
        tooltipProvider.InitTooltip(GetTooltipString);
    }

    private void Start()
    {
        if (initialized)
        {
            UpdateOccupiedText();
            UpdateBandwidthUsage();
        }
        else
        {
            Debug.Log("Failed to Initialize DroneUiComponent!");
        }
    }

    private void Update()
    {
        labelUpdateInterval -= Time.deltaTime;
        if (labelUpdateInterval < 0)
        {
            labelUpdateInterval = labelUpdateIntervalMax;
            UpdateOccupiedText();
            UpdateBandwidthUsage();
        }
    }
    
    private string GetTooltipString()
    {
        stringBuilder.Clear();
        stringBuilder.AppendLine($"Unoccupied drones on this planet: {thisPlanet.PlanetDroneManager.GetUnassignedDronesCount()}");
        stringBuilder.AppendLine($"Total drones on this planet: {thisPlanet.PlanetDroneManager.GetAllDronesData().Count}");
        stringBuilder.AppendLine("");
        stringBuilder.AppendLine("Occupied drones breakdown:");
        
        // foreach (var role in (DroneRole[])Enum.GetValues(typeof(DroneRole)))
        // {
        //     if (role == DroneRole.None) continue;
        //     stringBuilder.AppendLine($"    {role}: {thisPlanet.PlanetDroneManager.GetOccupiedDroneCount(role)}");
        // }
        
        return stringBuilder.ToString();
    }

    private void UpdateBandwidthUsage()
    {
        float planetUsage = thisPlanet.PlanetDroneManager.TotalPlanetBandwidthUsage;
        BandwidthUsageText.SetText("{0}/{1} MBit/s", planetUsage, GlobalGameStateController.Instance.MothershipEntity.GlobalBandwidthLimit);
    }

    private void UpdateOccupiedText()
    {
        int numFree = thisPlanet.PlanetDroneManager.GetUnassignedDronesCount();
        int dronesTotalOnline = thisPlanet.PlanetDroneManager.GetAllDronesData().Count;
        if (numFree != numFreeOld)
        {
            UnoccupiedDronesText.SetText("{0} / {1}", numFree, dronesTotalOnline);
            numFreeOld = numFree;
        }
    }
}