﻿using System.Collections;
using System.Collections.Generic;
using Code.Utility;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StorableCategoryItemComponent : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Image ListItemBackground;
    
    [SerializeField] private TextMeshProUGUI ItemNameLabel;
    [SerializeField] private TextMeshProUGUI ItemUnitsLabel;
    [SerializeField] private TextMeshProUGUI ItemVolumeLabel;

    private IStorable thisStorable;
    private PlanetStorageManager thisPlanetStorageManager;

    private StoragePanelComponent storagePanelRef;
    
    public void InitStorableListItem(IStorable storable, PlanetStorageManager planetStorageManager, StoragePanelComponent storagePanelComponent)
    {
        thisStorable = storable;
        thisPlanetStorageManager = planetStorageManager;
        storagePanelRef = storagePanelComponent;
        
        UpdateLabels();
    }

    public void UpdateLabels()
    {
        ItemNameLabel.SetText(thisStorable.StorableName);
        ItemUnitsLabel.SetText("{0:0}", thisPlanetStorageManager.GetUnitsStored(thisStorable.StorableType));
        ItemVolumeLabel.SetText("{0:2} m3", thisPlanetStorageManager.GetVolumeStored(thisStorable.StorableType));
    }
    
    public void OnPointerClick(PointerEventData eventData)
    {
        storagePanelRef.PopulateStorableDetails(thisStorable);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ListItemBackground.color = DataValues.RecipeListItemBackgroundHoverColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ListItemBackground.color = DataValues.RecipeListItemBackgroundRegularColor;
    }
}