﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlanetStorageItemDetailsComponent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI StorableNameLabel;
    [SerializeField] private TextMeshProUGUI StorableItemInfoLabel;
    
    private void OnEnable()
    {
        StorableNameLabel.SetText("");
        StorableItemInfoLabel.SetText("");
    }

    public void PopulateStorableDetails(IStorable storable)
    {
        StorableNameLabel.SetText(storable.StorableName);
        StorableItemInfoLabel.SetText(storable.StorableInfoString);
    }
}