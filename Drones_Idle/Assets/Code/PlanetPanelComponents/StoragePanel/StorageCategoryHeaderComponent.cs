﻿using System.Collections;
using System.Collections.Generic;
using Code.Utility;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StorageCategoryHeaderComponent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI CategoryNameLabel;
    
    public void InitStorageHeader(StorableCategory storableCategory)
    {
        CategoryNameLabel.SetText(storableCategory.ToString());//TODO: remove allocation
    }
}