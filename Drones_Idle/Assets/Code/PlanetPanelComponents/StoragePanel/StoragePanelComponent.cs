﻿using System;
using System.Collections;
using System.Collections.Generic;
using Code.Utility;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class StoragePanelComponent : MonoBehaviour
{
    [SerializeField] private StorageCategoryHeaderComponent StorageCategoryHeaderPrefab;
    [SerializeField] private StorableCategoryItemComponent StorageCategoryItemPrefab;
    
    [SerializeField] private GameObject StorageContentParent;
    
    [SerializeField] private TextMeshProUGUI StorageContentsVolume;

    [SerializeField] private PlanetStorageItemDetailsComponent DetailsComponent;
    
    private PlanetStorageManager planetStorageManager;

    private Dictionary<StorableCategory, StorageCategoryHeaderComponent> CategoryHeaderItems;
    private Dictionary<StorableType, StorableCategoryItemComponent> CategoryListItems;

    private void Awake()
    {
        planetStorageManager = GlobalGameStateController.Instance.GetPlanetById(1).PlanetStorageManager;
        
        CategoryHeaderItems = new Dictionary<StorableCategory, StorageCategoryHeaderComponent>();
        CategoryListItems = new Dictionary<StorableType, StorableCategoryItemComponent>();

        planetStorageManager.OnPlanetStorageContentChanged += UpdateStorageLabels;
    }

    private void OnEnable()
    {
        RenderStorablesByCategory();
    }

    public void PopulateStorableDetails(IStorable storable)
    {
        DetailsComponent.PopulateStorableDetails(storable);
    }

    private void UpdateStorageLabels(object sender, PlanetStorageManager.OnPlanetStorageContentChangedEventArgs e)
    {
        UpdateOverviewLabel();
        
        if (CategoryListItems.ContainsKey(e.Storable.StorableType))
        {
            CategoryListItems[e.Storable.StorableType].UpdateLabels();
        }
    }

    private void RenderStorablesByCategory()
    {
        foreach (var header in CategoryHeaderItems)
        {
            Destroy(header.Value.gameObject);
        }

        foreach (var listItem in CategoryListItems)
        {
            Destroy(listItem.Value.gameObject);
        }
        CategoryHeaderItems.Clear();
        CategoryListItems.Clear();
        
        foreach (var category in (StorableCategory[])Enum.GetValues(typeof(StorableCategory)))
        {
            var categoryHeader = Instantiate(StorageCategoryHeaderPrefab, StorageContentParent.transform);
            categoryHeader.InitStorageHeader(category);

            int thisCategoryItemsCount = 0;
            foreach (var storable in DataInstanceFactory.GetStorableInstancesByCategory(category))
            {
                if (Math.Abs(planetStorageManager.GetUnitsStored(storable.StorableType)) > 0)
                {
                    var storableCategoryItem = Instantiate(StorageCategoryItemPrefab, StorageContentParent.transform);
                    storableCategoryItem.InitStorableListItem(storable, planetStorageManager, this);
                    thisCategoryItemsCount++;
                    
                    CategoryListItems.Add(storable.StorableType, storableCategoryItem);
                }
            }

            if (thisCategoryItemsCount == 0)
            {
                Destroy(categoryHeader.gameObject);
            }
            else
            {
                CategoryHeaderItems.Add(category, categoryHeader);
            }
        }
    }

    private void UpdateOverviewLabel()
    {
        StorageContentsVolume.SetText("{0:0} m3", planetStorageManager.GetTotalVolumeStored());
    }
}