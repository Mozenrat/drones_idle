﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ConstructionQueueListItemComponent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI ConstructionQueueListItemNameLabel;
    [SerializeField] private TextMeshProUGUI ConstructionQueueListItemTimeLeftLabel;
    [SerializeField] private Image ConstructionQueueListItemProgressBar;
    [SerializeField] private Button ConstructionQueueListItemCancelButton;

    public ConstructionCalculation PeekCalculation => thisCalc;
    private ConstructionCalculation thisCalc;

    private PlanetContents thisPlanet;
    
    private float labelUpdateInterval;
    private float labelUpdateIntervalMax = .02f;

    private void Update()
    {
        labelUpdateInterval -= Time.deltaTime;
        if (labelUpdateInterval < 0)
        {
            labelUpdateInterval = labelUpdateIntervalMax;
            UpdateListItem();
        }
    }
    
    public void InitConstructionQueueListItem(ConstructionCalculation calc, PlanetContents planet)
    {
        thisCalc = calc;
        thisPlanet = planet;
        
        ConstructionQueueListItemNameLabel.SetText(thisCalc.PeekConstructable.BuildingName);

        ConstructionQueueListItemCancelButton.onClick.AddListener(CancelLastConstructionTaskOfSameType);
    }

    private void CancelLastConstructionTaskOfSameType()
    {
        var allCalcs = thisPlanet.PlanetCalculationsManager.GetConstructionCalculations;
        var lastOfSameBuildingType = allCalcs.Last(item => item.PeekConstructable.BuildingType == thisCalc.PeekConstructable.BuildingType);
        lastOfSameBuildingType.CancelThisConstructionTask();
    }

    private void UpdateListItem()
    {
        ConstructionQueueListItemTimeLeftLabel.SetText("{0} s", thisCalc.TimeToConstructionCompletion); //TODO: proper timefunc needed
        ConstructionQueueListItemProgressBar.fillAmount = Mathf.InverseLerp(0, thisCalc.ConstructionDurationTotal,
            thisCalc.ConstructionDurationCurrent);
    }
}