﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstructionQueueComponent : MonoBehaviour
{
    [SerializeField] private GameObject ConstructionQueueContent;
    [SerializeField] private ConstructionQueueListItemComponent ConstructionQueueListItemPrefab;

    private List<ConstructionQueueListItemComponent> CurrentQueueListItems;
    
    private PlanetContents thisPlanet;
    
    private void Awake()
    {
        thisPlanet = GlobalGameStateController.Instance.GetPlanetById(1);
        CurrentQueueListItems = new List<ConstructionQueueListItemComponent>();

        thisPlanet.PlanetCalculationsManager.OnConstructionQueueChanged += RenderConstructionQueueChange;
    }

    private void OnEnable()
    {
        RenderConstructionQueue();
    }

    private void RenderConstructionQueue()
    {
        foreach (var listItem in CurrentQueueListItems)
        {
            Destroy(listItem.gameObject);
        }
        CurrentQueueListItems.Clear();

        foreach (var calcItem in thisPlanet.PlanetCalculationsManager.GetConstructionCalculations)
        {
            var listItem = Instantiate(ConstructionQueueListItemPrefab, ConstructionQueueContent.transform);
            listItem.InitConstructionQueueListItem(calcItem, thisPlanet);
            CurrentQueueListItems.Add(listItem);
        }
    }

    private void RenderConstructionQueueChange(object sender, PlanetCalculationsManager.OnConstructionQueueChangedEventArgs e)
    {
        if (e.IsCalculationBeingAdded)
        {
            var listItem = Instantiate(ConstructionQueueListItemPrefab, ConstructionQueueContent.transform);
            listItem.InitConstructionQueueListItem(e.CalcRef, thisPlanet);
            CurrentQueueListItems.Add(listItem);
        }
        else
        {
            var listItem = CurrentQueueListItems.Find(item => item.PeekCalculation == e.CalcRef);
            Destroy(listItem.gameObject);
            CurrentQueueListItems.Remove(listItem);
        }
    }
}