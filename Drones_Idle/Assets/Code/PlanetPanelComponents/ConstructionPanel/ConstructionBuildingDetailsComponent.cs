﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using Code.Utility;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ConstructionBuildingDetailsComponent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI BuildingNameLabel;
    [SerializeField] private TextMeshProUGUI BuildingInfoLabel;
    [SerializeField] private TextMeshProUGUI BuildingStatsLabel;
    [SerializeField] private Button AddToConstructionButton;

    private BonusController bonusController;
    private PlanetContents thisPlanet;
    private IConstructable selectedConstructable;

    private readonly StringBuilder stringBuilder = new StringBuilder();

    public void PopulateBuildingDetails(BuildingType buildingType)
    {
        bonusController = GlobalGameStateController.Instance.BonusController;
        thisPlanet = GlobalGameStateController.Instance.GetPlanetById(1);
        selectedConstructable = DataInstanceFactory.GetConstructableInstance(buildingType);
        
        BuildingNameLabel.SetText(selectedConstructable.BuildingName);
        BuildingInfoLabel.SetText(selectedConstructable.BuildingInfo);

        PopulateLabels();
        
        AddToConstructionButton.onClick.RemoveAllListeners();
        AddToConstructionButton.onClick.AddListener(AddToConstructionQueue);
    }

    private void PopulateLabels()
    {
        stringBuilder.Clear();
        var buildingCount = thisPlanet.PlanetBuildingManager.GetBuildingAmount(selectedConstructable.BuildingType);
        var nextDuration = bonusController.GetBuildingConstructionDuration(selectedConstructable.BuildingType, thisPlanet);
        var nextCost = bonusController.GetBuildingConstructionCost(selectedConstructable.BuildingType, thisPlanet);
        IStorable storableData;
        
        stringBuilder.Append("Constructed on this planet: ").Append(buildingCount).AppendLine();
        stringBuilder.Append("Construction duration: ").AppendLine();
        stringBuilder.Append("    ").Append(nextDuration.ToTwoDigitFloat()).Append(" s").AppendLine();
        stringBuilder.AppendLine("Cost of next building: ");
        foreach (var costItem in nextCost)
        {
            storableData = DataInstanceFactory.GetStorableInstance(costItem.Key);
            stringBuilder.Append("    ").Append(costItem.Value.ToTwoDigitFloat()).Append(" ").Append(storableData.StorableName).AppendLine();
        }
        
        BuildingStatsLabel.SetText(stringBuilder.ToString());
    }

    private void AddToConstructionQueue()
    {
        var nextCost = bonusController.GetBuildingConstructionCost(selectedConstructable.BuildingType, thisPlanet);
        //check if enough resources to start
        foreach (var costItem in nextCost)
        {
            var inStorage = thisPlanet.PlanetStorageManager.GetUnitsStored(costItem.Key);
            if (inStorage < costItem.Value)
            {
                NotificationController.ShowNotification_Static("Not enough resources to start construction", true);
                return;
            }
        }
        //if enough deduct resources from planet storage
        foreach (var costItem in nextCost)
        {
            thisPlanet.PlanetStorageManager.SubtractUnitsFromStorage(costItem.Key, costItem.Value);
        }
        
        thisPlanet.PlanetCalculationsManager.InitConstructionCalculation(thisPlanet, selectedConstructable.BuildingType, out _);
        
        PopulateLabels();
    }
}