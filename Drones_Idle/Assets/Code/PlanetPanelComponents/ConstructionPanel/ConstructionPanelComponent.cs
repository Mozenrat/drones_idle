﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConstructionPanelComponent : MonoBehaviour
{
    [SerializeField] private GameObject ConstructionPanelContent;
    [SerializeField] private ConstructionListItemComponent ConstructionListItemPrefab;
    [SerializeField] private ConstructionBuildingDetailsComponent BuildingDetailsComponent;
    [SerializeField] private ConstructionQueueComponent ConstructionQueueComponent;

    [SerializeField] private Button ConstructionQueueButton;

    private List<ConstructionListItemComponent> CurrentListItems;
    
    private PlanetContents selectedPlanet;
    private TechnologyController technologyController;

    private void Awake()
    {
        selectedPlanet = GlobalGameStateController.Instance.GetPlanetById(1);
        technologyController = GlobalGameStateController.Instance.MothershipEntity.TechnologyController;
        
        CurrentListItems = new List<ConstructionListItemComponent>();
        
        ConstructionQueueButton.onClick.AddListener(ViewConstructionQueue);
    }

    private void OnEnable()
    {
        RenderConstructionListItems();
        
        ConstructionQueueComponent.gameObject.SetActive(true);
        BuildingDetailsComponent.gameObject.SetActive(false);
    }
    
    public void PopulateBuildingDetails(BuildingType buildingType)
    {
        ConstructionQueueComponent.gameObject.SetActive(false);
        BuildingDetailsComponent.gameObject.SetActive(true);
        
        BuildingDetailsComponent.PopulateBuildingDetails(buildingType);
    }

    private void ViewConstructionQueue()
    {
        ConstructionQueueComponent.gameObject.SetActive(true);
        BuildingDetailsComponent.gameObject.SetActive(false);
    }

    private void RenderConstructionListItems()
    {
        foreach (var item in CurrentListItems)
        {
            Destroy(item.gameObject);
        }
        CurrentListItems.Clear();
        
        foreach (var buildingType in technologyController.KnownBuildingTypes)
        {
            var listItem = Instantiate(ConstructionListItemPrefab, ConstructionPanelContent.transform);
            listItem.InitConstructionListItem(buildingType, selectedPlanet, this);
            CurrentListItems.Add(listItem);
        }
    }
}