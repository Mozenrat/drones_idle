﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ConstructionListItemComponent : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private TextMeshProUGUI BuildingNameLabel;
    [SerializeField] private TextMeshProUGUI BuildingQuantityLabel;
    [SerializeField] private Image BuildingListItemBackground;

    private ConstructionPanelComponent constructionPanelRef;
    private PlanetContents thisPlanet;
    private IConstructable thisConstructable;

    public void InitConstructionListItem(BuildingType buildingType, PlanetContents planet, ConstructionPanelComponent constructionPanelReference)
    {
        constructionPanelRef = constructionPanelReference;
        thisPlanet = planet;
        thisConstructable = DataInstanceFactory.GetConstructableInstance(buildingType);
        
        BuildingNameLabel.SetText(thisConstructable.BuildingName);
        BuildingQuantityLabel.SetText("{0}",thisPlanet.PlanetBuildingManager.GetBuildingAmount(thisConstructable.BuildingType));
    }
    
    public void OnPointerClick(PointerEventData eventData)
    {
        constructionPanelRef.PopulateBuildingDetails(thisConstructable.BuildingType);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        BuildingListItemBackground.color = DataValues.RecipeListItemBackgroundHoverColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        BuildingListItemBackground.color = DataValues.RecipeListItemBackgroundRegularColor;
    }
}