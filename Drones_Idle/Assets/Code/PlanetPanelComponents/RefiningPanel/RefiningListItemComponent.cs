﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using Code.Utility;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RefiningListItemComponent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI OreNameLabel;
    [SerializeField] private TextMeshProUGUI RefiningProgressLabel;
    [SerializeField] private TMP_InputField DronesAssignedInput;
    [SerializeField] private Button DronePlusButton;
    [SerializeField] private Button DroneMinusButton;
    [SerializeField] private Image RefiningProgressBar;
    
    private IOre thisOre;
    private RefiningProcess thisRefiningProcess;
    private PlanetContents thisPlanet;
    private RefiningCalculation thisRefiningCalc;
    
    private float labelUpdateInterval;
    private float labelUpdateIntervalMax = .02f;
    
    private readonly StringBuilder stringBuilder = new StringBuilder(100);
    
    public void InitRefiningListItem(IOre oreInstance, PlanetContents planet)
    {
        thisOre = oreInstance;
        thisRefiningProcess = RefiningProcess.CrushProcessing;
        thisPlanet = planet;
        
        OreNameLabel.SetText(thisOre.Name);
        
        thisPlanet.PlanetCalculationsManager.InitRefiningCalculation(thisPlanet, thisOre.OreType, thisRefiningProcess, out thisRefiningCalc);
        
        var dronesCountThisOre = thisPlanet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Refining, thisOre.OreType, ProductionRecipeType.None);
        DronesAssignedInput.text = dronesCountThisOre.ToString();
        
        var nameTooltip = OreNameLabel.gameObject.AddComponent<TooltipProvider>();
        nameTooltip.InitTooltip(GetTooltipString);
        var progressTooltip = RefiningProgressBar.gameObject.AddComponent<TooltipProvider>();
        progressTooltip.InitTooltip(GetTooltipString);
        
        DronePlusButton.onClick.AddListener(AssignDroneToRefiningTask);
        DroneMinusButton.onClick.AddListener(UnassignDroneFromRefiningTask);
        UpdateListItem();
    }

    private void Update()
    {
        labelUpdateInterval -= Time.deltaTime;
        if (labelUpdateInterval < 0)
        {
            labelUpdateInterval = labelUpdateIntervalMax;
            UpdateListItem();
        }
    }
    
    private void UpdateListItem()
    {
        if (thisRefiningCalc == null) return;
        
        RefiningProgressLabel.SetText("{0:1} s", thisRefiningCalc.RefiningCycleDuration - thisRefiningCalc.RefiningCycleCurrent);
        RefiningProgressBar.fillAmount = Mathf.InverseLerp(0f, thisRefiningCalc.RefiningCycleDuration,
            thisRefiningCalc.RefiningCycleCurrent);
    }
    
    private string GetTooltipString()
    {
        stringBuilder.Clear();
        if (thisRefiningCalc == null || !thisRefiningCalc.IsActive)
        {
            stringBuilder.Append("Currently not refining this ore");
            return stringBuilder.ToString();
        }

        var oreInStorage = thisPlanet.PlanetStorageManager.GetUnitsStored(thisOre.StorableType).ToTwoDigitFloat();
        stringBuilder.Append(oreInStorage).AppendLine($" units of {thisOre.Name} in storage");
        stringBuilder.AppendLine();

        stringBuilder.AppendLine("Refines into: ");
        foreach (var mineral in thisRefiningCalc.MineralBatchToGet)
        {
            stringBuilder.Append($"    {DataInstanceFactory.GetStorableInstance(mineral.Key).StorableName}: ").Append(mineral.Value).AppendLine(" units");
        }
        stringBuilder.AppendLine();

        if (!thisRefiningCalc.IsEnoughResourcesInStorage)
        {
            stringBuilder.AppendLine("Not enough ore in storage");
            stringBuilder.Append(thisRefiningCalc.RefineBatchWithBusyDrones).Append($" units of {thisOre.Name} required in storage to start refining");
            return stringBuilder.ToString();
        }

        stringBuilder.Append(thisRefiningCalc.RefineBatchWithBusyDrones).Append($" units of {thisOre.Name} is refined per operation");
        return stringBuilder.ToString();
    }

    private void AssignDroneToRefiningTask()
    {
        thisPlanet.PlanetDroneManager.AssignDronesToRole(DroneRole.Refining, thisOre.OreType, ProductionRecipeType.None, 1);
        
        var currentlyAssignedDrones = thisPlanet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Refining, thisOre.OreType, ProductionRecipeType.None);
        if (currentlyAssignedDrones >= 1)
        {
            thisPlanet.PlanetCalculationsManager.InitRefiningCalculation(thisPlanet, thisOre.OreType, thisRefiningProcess, out thisRefiningCalc);
            DronesAssignedInput.text = currentlyAssignedDrones.ToString();
        }
    }

    private void UnassignDroneFromRefiningTask()
    {
        thisPlanet.PlanetDroneManager.UnassignDronesFromRole(DroneRole.Refining, thisOre.OreType, ProductionRecipeType.None, 1);
        var currentlyAssignedDrones = thisPlanet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Refining, thisOre.OreType, ProductionRecipeType.None);
        DronesAssignedInput.text = currentlyAssignedDrones.ToString();
    }
}
