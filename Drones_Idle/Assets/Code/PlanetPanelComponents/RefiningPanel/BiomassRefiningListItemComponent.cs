﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BiomassRefiningListItemComponent : MonoBehaviour
{
    [SerializeField] private TMP_InputField BiomassDronesAssignedInput;
    [SerializeField] private TextMeshProUGUI PlanetBiomassAbundance;
    [SerializeField] private TextMeshProUGUI RefiningProgressLabel;
    [SerializeField] private Button DronePlusButton;
    [SerializeField] private Button DroneMinusButton;
    [SerializeField] private Image RefiningProgressBar;

    private IOre thisOre;
    private RefiningProcess thisRefiningProcess;
    private PlanetContents thisPlanet;
    private RefiningCalculation thisRefiningCalc;
    
    private float labelUpdateInterval;
    private float labelUpdateIntervalMax = .02f;
    
    public void InitBiomassListItem(PlanetContents planet)
    {
        thisOre = DataInstanceFactory.GetOreInstance(OreType.BiomassOre);
        thisRefiningProcess = RefiningProcess.BiomassRefining;
        thisPlanet = planet;
        
        thisPlanet.PlanetCalculationsManager.InitRefiningCalculation(thisPlanet, thisOre.OreType, thisRefiningProcess, out thisRefiningCalc);
        
        var dronesCountThisOre = thisPlanet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Refining, thisOre.OreType, ProductionRecipeType.None);
        BiomassDronesAssignedInput.text = dronesCountThisOre.ToString();
        
        PlanetBiomassAbundance.SetText("{0}%", thisPlanet.BiomassAbundance);
        DronePlusButton.onClick.AddListener(AssignDroneToBiomassRefiningTask);
        DroneMinusButton.onClick.AddListener(UnassignDroneFromBiomassRefiningTask);
        UpdateListItem();
    }
    
    private void Update()
    {
        labelUpdateInterval -= Time.deltaTime;
        if (labelUpdateInterval < 0)
        {
            labelUpdateInterval = labelUpdateIntervalMax;
            UpdateListItem();
        }
    }
    
    private void UpdateListItem()
    {
        if (thisRefiningCalc == null) return;
        
        RefiningProgressLabel.SetText("{0:1} s", thisRefiningCalc.RefiningCycleDuration - thisRefiningCalc.RefiningCycleCurrent);
        RefiningProgressBar.fillAmount = Mathf.InverseLerp(0f, thisRefiningCalc.RefiningCycleDuration,
            thisRefiningCalc.RefiningCycleCurrent);
    }

    private void AssignDroneToBiomassRefiningTask()
    {
        thisPlanet.PlanetDroneManager.AssignDronesToRole(DroneRole.Refining, thisOre.OreType, ProductionRecipeType.None, 1);
        
        var currentlyAssignedDrones = thisPlanet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Refining, thisOre.OreType, ProductionRecipeType.None);
        if (currentlyAssignedDrones >= 1)
        {
            thisPlanet.PlanetCalculationsManager.InitRefiningCalculation(thisPlanet, thisOre.OreType, thisRefiningProcess, out thisRefiningCalc);
            BiomassDronesAssignedInput.text = currentlyAssignedDrones.ToString();
        }
    }

    private void UnassignDroneFromBiomassRefiningTask()
    {
        thisPlanet.PlanetDroneManager.UnassignDronesFromRole(DroneRole.Refining, thisOre.OreType, ProductionRecipeType.None, 1);
        var currentlyAssignedDrones = thisPlanet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Refining, thisOre.OreType, ProductionRecipeType.None);
        BiomassDronesAssignedInput.text = currentlyAssignedDrones.ToString();
    }
}