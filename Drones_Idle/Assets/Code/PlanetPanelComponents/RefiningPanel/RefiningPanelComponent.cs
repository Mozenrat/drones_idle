﻿using System.Linq;
using Code.Utility;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RefiningPanelComponent : MonoBehaviour
{
    [SerializeField] private RefiningListItemComponent RefiningListItemPrefab;
    [SerializeField] private BiomassRefiningListItemComponent BiomassRefiningListItemPrefab;
    
    [SerializeField] private GameObject RefiningListItemsContainerParent;

    private PlanetContents selectedPlanet;

    private void Awake()
    {
        selectedPlanet = GlobalGameStateController.Instance.GetPlanetById(1);
        
        foreach (var oreBucket in selectedPlanet.OreBuckets)
        {
            var listItem = Instantiate(RefiningListItemPrefab, RefiningListItemsContainerParent.transform);
            listItem.InitRefiningListItem(DataInstanceFactory.GetOreInstance(oreBucket.BucketOreType), selectedPlanet);
        }

        var biomassItem = Instantiate(BiomassRefiningListItemPrefab, RefiningListItemsContainerParent.transform);
        biomassItem.InitBiomassListItem(selectedPlanet);
        biomassItem.transform.SetAsLastSibling();
    }
}