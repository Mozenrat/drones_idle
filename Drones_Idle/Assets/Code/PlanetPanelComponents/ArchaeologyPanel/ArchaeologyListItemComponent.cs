﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ArchaeologyListItemComponent : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private TextMeshProUGUI ListItemNameLabel;
    [SerializeField] private TextMeshProUGUI ListItemLevelLabel;
    [SerializeField] private TextMeshProUGUI ListItemTimeToCompletionLabel;
    [SerializeField] private Image ListItemProgressBar;
    [SerializeField] private GameObject ListItemIsActiveGameObject;

    private ArchaeologyMilestoneObject thisMilestone;
    private ArchaeologyPanelComponent panelReference;

    public void InitArchaeologyListItem(ArchaeologyMilestoneObject milestoneObject, ArchaeologyPanelComponent archaeologyPanel)
    {
        thisMilestone = milestoneObject;
        panelReference = archaeologyPanel;
        
        ListItemIsActiveGameObject.SetActive(false);
        
        ListItemNameLabel.SetText(milestoneObject.MilestoneDisplayName);
        ListItemLevelLabel.SetText("Level: {0}", milestoneObject.MilestoneLevel);
        UpdateTimeToNextLevelLabel();
        ListItemProgressBar.fillAmount = Mathf.InverseLerp(0f, milestoneObject.MilestoneNextLevelCost, milestoneObject.MilestoneCurrentPointsProgress);
    }
    
    public void UpdateMilestoneItem()
    {
        ListItemLevelLabel.SetText("Level: {0}", thisMilestone.MilestoneLevel);
        UpdateTimeToNextLevelLabel();
        ListItemProgressBar.fillAmount = Mathf.InverseLerp(0f, thisMilestone.MilestoneNextLevelCost, thisMilestone.MilestoneCurrentPointsProgress);
    }

    public void SetMilestoneListItemActive(bool active)
    {
        ListItemIsActiveGameObject.SetActive(active);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        panelReference.SetArchaeologyMilestoneActive(thisMilestone.MilestoneType);
    }
    
    private void UpdateTimeToNextLevelLabel()
    {
        if (thisMilestone.MilestoneTimeToNextLevel <= -1f)
        {
            ListItemTimeToCompletionLabel.SetText("Finishes in: never");
        }
        else
        {
            ListItemTimeToCompletionLabel.SetText("Finishes in: {0:1}s", thisMilestone.MilestoneTimeToNextLevel);
        }
    }
}