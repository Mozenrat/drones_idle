﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ArchaeologyDigItemComponent : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private TextMeshProUGUI CurrentDepthLabel;
    [SerializeField] private TextMeshProUGUI TimeToNextDepthLabel;
    [SerializeField] private Image DigProgressBar;
    [SerializeField] private GameObject DigItemIsActiveGameObject;
    
    private ArchaeologyMilestoneObject digMilestone;
    private ArchaeologyPanelComponent panelReference;
    
    public void InitArchaeologyDigItem(ArchaeologyMilestoneObject milestoneObject, ArchaeologyPanelComponent archaeologyPanel)
    {
        digMilestone = milestoneObject;
        panelReference = archaeologyPanel;
        
        DigItemIsActiveGameObject.SetActive(false);
        
        CurrentDepthLabel.SetText("Current depth: {0}m", digMilestone.MilestoneLevel);
        UpdateTimeToNextLevelLabel();
        DigProgressBar.fillAmount = Mathf.InverseLerp(0f, digMilestone.MilestoneNextLevelCost, digMilestone.MilestoneCurrentPointsProgress);
    }

    public void UpdateDigItem()
    {
        CurrentDepthLabel.SetText("Current depth: {0}m", digMilestone.MilestoneLevel);
        UpdateTimeToNextLevelLabel();
        DigProgressBar.fillAmount = Mathf.InverseLerp(0f, digMilestone.MilestoneNextLevelCost, digMilestone.MilestoneCurrentPointsProgress);
    }

    public void SetDigItemActive(bool active)
    {
        DigItemIsActiveGameObject.SetActive(active);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        digMilestone.AddProgress(1); //TODO: move hardcoded
        panelReference.SetArchaeologyMilestoneActive(digMilestone.MilestoneType);
    }
    
    private void UpdateTimeToNextLevelLabel()
    {
        if (digMilestone.MilestoneTimeToNextLevel <= -1f)
        {
            TimeToNextDepthLabel.SetText("");
        }
        else
        {
            TimeToNextDepthLabel.SetText("Time to next depth: {0:1}s", digMilestone.MilestoneTimeToNextLevel);
        }
    }
}