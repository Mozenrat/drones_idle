﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ArchaeologyPanelComponent : MonoBehaviour
{
    [SerializeField] private Button DronesAssignedMinusButton;
    [SerializeField] private Button DronesAssignedPlusButton;
    [SerializeField] private TMP_InputField DronesAssignedInput;
    
    [SerializeField] private ArchaeologyDigItemComponent ArchaeologyDigItemComponent;
    
    [SerializeField] private ArchaeologyListItemComponent ArchaeologyListItemPrefab;
    [SerializeField] private GameObject MilestonesParentContent; 
    
    private PlanetContents selectedPlanet;

    private Dictionary<ArchaeologyMilestoneType, ArchaeologyListItemComponent> existingMilestoneListItems;
    
    private float labelUpdateInterval;
    private float labelUpdateIntervalMax = .02f;
    
    private void Awake()
    {
        selectedPlanet = GlobalGameStateController.Instance.GetPlanetById(1);
        existingMilestoneListItems = new Dictionary<ArchaeologyMilestoneType, ArchaeologyListItemComponent>();

        ArchaeologyDigItemComponent.InitArchaeologyDigItem(
            selectedPlanet.PlanetArchaeologyManager.GetMilestoneByType(ArchaeologyMilestoneType.InitialDig), this);
        ArchaeologyDigItemComponent.SetDigItemActive(true);
        
        var dronesCount = selectedPlanet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Archaeology, 
            OreType.None,ProductionRecipeType.None);
        DronesAssignedInput.text = dronesCount.ToString();
        DronesAssignedPlusButton.onClick.AddListener(AssignDronesToArchaeology);
        DronesAssignedMinusButton.onClick.AddListener(UnassignDronesFromArchaeology);
    }

    private void OnEnable()
    {
        RenderArchaeologyMilestones();
        SetArchaeologyMilestoneActive(selectedPlanet.PlanetArchaeologyManager.ActiveArchaeologyObject.MilestoneType);
        selectedPlanet.PlanetArchaeologyManager.OnNewMilestoneUnlocked += OnPlanetNewMilestoneUnlocked;
    }

    private void OnDisable()
    {
        selectedPlanet.PlanetArchaeologyManager.OnNewMilestoneUnlocked -= OnPlanetNewMilestoneUnlocked;
    }

    private void Update()
    {
        labelUpdateInterval -= Time.deltaTime;
        if (labelUpdateInterval < 0)
        {
            labelUpdateInterval = labelUpdateIntervalMax;
            
            var dronesCount = selectedPlanet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Archaeology, 
                OreType.None,ProductionRecipeType.None);
            DronesAssignedInput.text = dronesCount.ToString();
            ArchaeologyDigItemComponent.UpdateDigItem();

            foreach (var listItem in existingMilestoneListItems.Values)
            {
                listItem.UpdateMilestoneItem();
            }
        }
    }

    public void SetArchaeologyMilestoneActive(ArchaeologyMilestoneType milestoneType)
    {
        if (milestoneType == ArchaeologyMilestoneType.InitialDig)
        {
            ArchaeologyDigItemComponent.SetDigItemActive(true);
            selectedPlanet.PlanetArchaeologyManager.SetActiveMilestone(milestoneType);
        }
        else
        {
            ArchaeologyDigItemComponent.SetDigItemActive(false);
        }
        
        foreach (var listItem in existingMilestoneListItems)
        {
            if (listItem.Key == milestoneType)
            {
                listItem.Value.SetMilestoneListItemActive(true);
                selectedPlanet.PlanetArchaeologyManager.SetActiveMilestone(milestoneType);
                continue;
            }
            listItem.Value.SetMilestoneListItemActive(false);
        }
    }

    private void AssignDronesToArchaeology()
    {
        selectedPlanet.PlanetDroneManager.AssignDronesToRole(DroneRole.Archaeology, OreType.None,ProductionRecipeType.None, 1);
    }

    private void UnassignDronesFromArchaeology()
    {
        selectedPlanet.PlanetDroneManager.UnassignDronesFromRole(DroneRole.Archaeology, OreType.None, ProductionRecipeType.None, 1);
    }
    
    private void OnPlanetNewMilestoneUnlocked(object sender, PlanetArchaeologyManager.OnNewMilestoneUnlockedEventArgs e)
    {
        if (existingMilestoneListItems.ContainsKey(e.MilestoneObject.MilestoneType)) return;
        
        var listItem = Instantiate(ArchaeologyListItemPrefab, MilestonesParentContent.transform);
        listItem.InitArchaeologyListItem(e.MilestoneObject, this);
        existingMilestoneListItems.Add(e.MilestoneObject.MilestoneType, listItem);
    }

    private void RenderArchaeologyMilestones()
    {
        foreach (var milestoneListItem in existingMilestoneListItems)
        {
            Destroy(milestoneListItem.Value.gameObject);
        }
        existingMilestoneListItems.Clear();
        
        foreach (var milestone in selectedPlanet.PlanetArchaeologyManager.GetAllMilestones())
        {
            if (milestone.Key == ArchaeologyMilestoneType.InitialDig || !milestone.Value.IsUnlocked) continue;
            
            var listItem = Instantiate(ArchaeologyListItemPrefab, MilestonesParentContent.transform);
            listItem.InitArchaeologyListItem(milestone.Value, this);
            existingMilestoneListItems.Add(milestone.Key, listItem);
        }
    }
}