﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AddToRocketListManager : MonoBehaviour
{
    public float RocketFreeCapacity => DataValues.RocketCargoholdCapacity - TotalRocketCargoVolume;
    public float TotalRocketCargoVolume => existingCargoListItems.Sum(item => item.ListItemCargoVolume);
    public int TotalRocketCargoBaysUsed => existingCargoListItems.Count;
    public float CurrentCargoLaunchFuelCost => TotalRocketCargoVolume * DataValues.RocketFuelCostPerM3Cargo;

    [SerializeField] private GameObject AddCargoContainerParent;
    [SerializeField] private GameObject HelpLabel;
    
    [SerializeField] private Button AddCargoButton;

    [SerializeField] private NewCargoListItemComponent NewCargoListItemPrefab;
    [SerializeField] private RocketCargoListItemComponent ExistingRocketCargoListItemPrefab;

    private PlanetContents thisPlanet;
    private List<NewCargoListItemComponent> newCargoListItems;
    private List<RocketCargoListItemComponent> existingCargoListItems;

    private Stack<RocketCargoListItemComponent> cargoListItemComponentsPool;
    
    private void Awake()
    {
        thisPlanet = GlobalGameStateController.Instance.GetPlanetById(1);
        cargoListItemComponentsPool = FillCargoListItemsPool();
        existingCargoListItems = new List<RocketCargoListItemComponent>();
        newCargoListItems = new List<NewCargoListItemComponent>();
        
        AddCargoButton.onClick.AddListener(ExpandNewItemSelectionContainer);
    }
    
    public void AddNewItemToRocketCargo(IStorable storable)
    {
        var cargoListItem = cargoListItemComponentsPool.Pop();
        cargoListItem.gameObject.SetActive(true);
        cargoListItem.InitAddedCargoListItem(storable, this, thisPlanet);
        cargoListItem.transform.SetAsLastSibling();
        existingCargoListItems.Add(cargoListItem);

        ClearNewCargoItemsSelector();

        AddCargoContainerParent.transform.SetAsLastSibling();
    }

    public void ExpandNewItemSelectionContainer()
    {
        if (existingCargoListItems.Count == DataValues.RocketCargoBaysAmount)
        {
            NotificationController.ShowNotification_Static(DataValues.LaunchPadAllRocketCargoBaysFull, true);
            return;
        }
        if (thisPlanet.PlanetStorageManager.GetStoredTypes().Length == 0)
        {
            NotificationController.ShowNotification_Static(DataValues.LaunchPadNoResourceToAddToRocket, true);
            return;
        }
        
        foreach (var storableType in thisPlanet.PlanetStorageManager.GetStoredTypes())
        {
            if (existingCargoListItems.Find(item => item.PeekRocketCargoListItemStorable == storableType) != null ||
                thisPlanet.PlanetStorageManager.GetUnitsStored(storableType) < 1f)
            {
                continue;
            }

            var listItem = Instantiate(NewCargoListItemPrefab, AddCargoContainerParent.transform);
            listItem.InitNewCargoListItem(DataInstanceFactory.GetStorableInstance(storableType), this);
            newCargoListItems.Add(listItem);
        }

        if (newCargoListItems.Count == 0) {
            NotificationController.ShowNotification_Static(DataValues.LaunchPadAllAvailableResourcesAlreadyAdded, false);
            return;
        }
        
        HelpLabel.SetActive(true);
        HelpLabel.transform.SetAsFirstSibling();
        AddCargoButton.gameObject.SetActive(false);
    }

    public void RemoveExistingItemFromRocketCargo(RocketCargoListItemComponent item)
    {
        existingCargoListItems.Remove(item);
        item.gameObject.SetActive(false);
        cargoListItemComponentsPool.Push(item);
        
        if (existingCargoListItems.Find(i => i == item) != null) 
            Debug.Log("Rocket cargo item still present in lookup list!");
    }

    public void LaunchAndClearCargoContents()
    {
        thisPlanet.PlanetStorageManager.SubtractUnitsFromStorage(StorableType.RocketFuel, CurrentCargoLaunchFuelCost);
        
        Dictionary<StorableType, float> cargoDictionary = new Dictionary<StorableType, float>();
        for (int i = existingCargoListItems.Count - 1; i >= 0; i--)
        {
            var cargoItem = existingCargoListItems[i];
            
            cargoDictionary.Add(cargoItem.PeekRocketCargoListItemStorable, cargoItem.ThisCargoUnits);
            thisPlanet.PlanetStorageManager.SubtractUnitsFromStorage(cargoItem.PeekRocketCargoListItemStorable, cargoItem.ThisCargoUnits);
            
            RemoveExistingItemFromRocketCargo(existingCargoListItems[i]);
        }
        
        thisPlanet.PlanetCalculationsManager.InitRocketLaunchCalculation(cargoDictionary, thisPlanet, out _);
    }
    
    private void ClearNewCargoItemsSelector()
    {
        foreach (var newCargoItem in newCargoListItems)
        {
            Destroy(newCargoItem.gameObject);
        }

        newCargoListItems.Clear();
        HelpLabel.SetActive(false);
        AddCargoButton.gameObject.SetActive(true);
    }

    private Stack<RocketCargoListItemComponent> FillCargoListItemsPool()
    {
        var cargoItemsPool = new Stack<RocketCargoListItemComponent>();
        
        //TODO: concerned that only 10 active list items is possible
        for (int i = 0; i < 10; i++)
        {
            var cargoListItem = Instantiate(ExistingRocketCargoListItemPrefab, transform);
            cargoItemsPool.Push(cargoListItem.GetComponent<RocketCargoListItemComponent>());
            cargoListItem.gameObject.SetActive(false);
        }
        
        AddCargoContainerParent.transform.SetAsLastSibling();

        return cargoItemsPool;
    }
}