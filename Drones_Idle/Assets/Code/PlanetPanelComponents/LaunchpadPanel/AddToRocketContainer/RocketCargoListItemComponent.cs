﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RocketCargoListItemComponent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI CargoNameLabel;
    [SerializeField] private TextMeshProUGUI CargoUnitsLabel;
    [SerializeField] private TextMeshProUGUI StorageUnitsLabel;
    [SerializeField] private Slider QuantitySlider;
    [SerializeField] private Button RemoveFromRocketButton;

    [HideInInspector] public float ThisCargoUnits;
    public StorableType PeekRocketCargoListItemStorable => thisStorable.StorableType;
    public float ListItemCargoVolume => ThisCargoUnits * thisStorable.UnitVolume;
    
    private PlanetContents thisPlanet;
    private IStorable thisStorable;
    private AddToRocketListManager rocketListManager;
    
    public void InitAddedCargoListItem(IStorable storable, AddToRocketListManager listManager, PlanetContents planet)
    {
        thisPlanet = planet;
        thisStorable = storable;
        rocketListManager = listManager;
        ThisCargoUnits = 1f;
        
        CargoNameLabel.SetText(thisStorable.StorableName);

        
        var planetStorageCount = thisPlanet.PlanetStorageManager.GetUnitsStored(thisStorable.StorableType);
        var planetVolume = planetStorageCount * thisStorable.UnitVolume;
        if (planetVolume > listManager.RocketFreeCapacity)
        {
            QuantitySlider.maxValue = Mathf.FloorToInt(listManager.RocketFreeCapacity / thisStorable.UnitVolume);
        }
        else
        {
            QuantitySlider.maxValue = planetStorageCount;
        }
        QuantitySlider.minValue = 0;
        QuantitySlider.value = ThisCargoUnits;
        UpdateItemLabels();

        QuantitySlider.onValueChanged.AddListener(SliderUpdate);
        RemoveFromRocketButton.onClick.AddListener(() => rocketListManager.RemoveExistingItemFromRocketCargo(this));
    }

    private void SliderUpdate(float sliderValue)
    {
        ThisCargoUnits = sliderValue;
        UpdateItemLabels();
    }

    private void UpdateItemLabels()
    {
        CargoUnitsLabel.SetText("{0} ({1} m3)", ThisCargoUnits, ThisCargoUnits * thisStorable.UnitVolume);
        StorageUnitsLabel.SetText("{0} units", thisPlanet.PlanetStorageManager.GetUnitsStored(thisStorable.StorableType) - ThisCargoUnits);
    }
}