﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NewCargoListItemComponent : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    [SerializeField] private Image ListItemBackground;
    [SerializeField] private TextMeshProUGUI StorableNameLabel;
    
    private IStorable thisStorable;
    private AddToRocketListManager listManager;
    
    public void InitNewCargoListItem(IStorable storable, AddToRocketListManager rocketListManager)
    {
        thisStorable = storable;
        listManager = rocketListManager;
        StorableNameLabel.SetText(storable.StorableName);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ListItemBackground.color = DataValues.RecipeListItemBackgroundHoverColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ListItemBackground.color = DataValues.RecipeListItemBackgroundRegularColor;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (listManager.RocketFreeCapacity > thisStorable.UnitVolume)
        {
            listManager.AddNewItemToRocketCargo(thisStorable);
        }
        else
        {
            NotificationController.ShowNotification_Static(DataValues.LaunchPadRocketFull, true);
        }
    }
}
