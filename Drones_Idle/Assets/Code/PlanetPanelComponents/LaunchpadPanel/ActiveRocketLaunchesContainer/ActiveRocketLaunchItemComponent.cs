﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using Code.Utility;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ActiveRocketLaunchItemComponent : MonoBehaviour
{
    [SerializeField] private Image RocketLaunchProgressBar;
    [SerializeField] private TextMeshProUGUI RocketOriginLabel;
    [SerializeField] private TextMeshProUGUI RocketArrivalLabel;
    [SerializeField] private Button CancelRocketLaunchButton;

    public RocketLaunchCalculation PeekLaunchCalculation => launchCalculation;
    
    private RocketLaunchCalculation launchCalculation;
    
    private float labelUpdateInterval;
    private float labelUpdateIntervalMax = .02f;
    
    private readonly StringBuilder stringBuilder = new StringBuilder(100);

    public void InitActiveRocketLaunchItem(RocketLaunchCalculation calculation)
    {
        launchCalculation = calculation;

        var originTooltip = RocketOriginLabel.gameObject.AddComponent<TooltipProvider>();
        originTooltip.InitTooltip(GetTooltipString);
        var arrivalTooltip = RocketArrivalLabel.gameObject.AddComponent<TooltipProvider>();
        arrivalTooltip.InitTooltip(GetTooltipString);
        
        CancelRocketLaunchButton.onClick.RemoveAllListeners();
        CancelRocketLaunchButton.onClick.AddListener(AbortThisRocketLaunch);
        
        UpdateListItem();
    }

    private void Update()
    {
        labelUpdateInterval -= Time.deltaTime;
        if (labelUpdateInterval < 0)
        {
            labelUpdateInterval = labelUpdateIntervalMax;
            UpdateListItem();
        }
    }
    
    private void OnDisable()
    {
        if (MouseUtility.IsMouseOverUiGameObject(gameObject))
        {
            TooltipController.HideTooltip_Static();
        }
    }
    
    private string GetTooltipString()
    {
        stringBuilder.Clear();

        stringBuilder.AppendLine("Rocket cargo: ");
        foreach (var cargoItem in launchCalculation.GetRocketCargoholdContents)
        {
            var storable = DataInstanceFactory.GetStorableInstance(cargoItem.Key);
            stringBuilder.Append($"    {storable.StorableName}: ").Append(cargoItem.Value).AppendLine(" units");
        }
        return stringBuilder.ToString();
    }

    private void UpdateListItem()
    {
        RocketOriginLabel.SetText($"Origin planet: {launchCalculation.GetOriginPlanetName}");
        RocketArrivalLabel.SetText("Arrival in: {0:1}s", launchCalculation.GetFlightTimeLeft);
        RocketLaunchProgressBar.fillAmount = Mathf.InverseLerp(0, DataValues.RocketFlightTimeToMothership,
            launchCalculation.CurrentFlightDuration);
    }
    
    private void AbortThisRocketLaunch()
    {
        launchCalculation.AbortRocketLaunch();
        NotificationController.ShowNotification_Static($"Rocket launch from {launchCalculation.GetOriginPlanetName} aborted, 20% of cargo was lost", true);
    }
}