﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LaunchpadPanelComponent : MonoBehaviour
{
    [SerializeField] private Image CapacityProgressBar;
    [SerializeField] private TextMeshProUGUI RocketCargoholdCapacityLabel;
    [SerializeField] private TextMeshProUGUI CurrentRocketCargoLabel;
    [SerializeField] private TextMeshProUGUI RocketCargoBayUsageLabel;
    [SerializeField] private TextMeshProUGUI FuelRequiredLabel;
    [SerializeField] private TextMeshProUGUI FuelInStorageLabel;
    [SerializeField] private Button LaunchButton;

    [SerializeField] private ActiveRocketLaunchItemComponent ActiveRocketLaunchItemPrefab;
    [SerializeField] private GameObject ActiveRocketLaunchItemsContainer;
    
    private AddToRocketListManager rocketCurrentCargoManager;
    private PlanetContents thisPlanet;

    private Stack<ActiveRocketLaunchItemComponent> activeRocketItemsPool;
    private List<ActiveRocketLaunchItemComponent> activeListItems;
    
    private float labelUpdateInterval;
    private float labelUpdateIntervalMax = .09f;

    private void Awake()
    {
        thisPlanet = GlobalGameStateController.Instance.GetPlanetById(1);
        rocketCurrentCargoManager = GetComponentInChildren<AddToRocketListManager>();
        activeRocketItemsPool = FillActiveRocketLaunchItemsPool();
        
        activeListItems = new List<ActiveRocketLaunchItemComponent>();

        thisPlanet.PlanetCalculationsManager.OnRocketLaunchQuantityChanged += RenderActiveRocketItem;
        
        LaunchButton.onClick.AddListener(LaunchOnClick);
        
        RocketCargoholdCapacityLabel.SetText("Rocket maximum capacity: {0} m3", DataValues.RocketCargoholdCapacity);
    }

    private void Start()
    {
        activeListItems.Clear();
        thisPlanet.PlanetCalculationsManager.OnLaunchpadPageFirstRender();
    }

    private void Update()
    {
        labelUpdateInterval -= Time.deltaTime;
        if (labelUpdateInterval < 0)
        {
            labelUpdateInterval = labelUpdateIntervalMax;
            UpdateLabels();
            UpdateLaunchButtonState();
        }
    }

    private void RenderActiveRocketItem(object sender, PlanetCalculationsManager.OnRocketLaunchEventArgs onRocketLaunchEventArgs)
    {
        if (onRocketLaunchEventArgs.IsCalculationBeingAdded)
        {
            var listItem = activeRocketItemsPool.Pop();
            listItem.gameObject.SetActive(true);
            listItem.InitActiveRocketLaunchItem(onRocketLaunchEventArgs.CalcRef);
            listItem.transform.SetAsLastSibling();
            activeListItems.Add(listItem);
        }
        else
        {
            var listItem = activeListItems.Find(item => item.PeekLaunchCalculation == onRocketLaunchEventArgs.CalcRef);
            if (listItem == null) 
                return;
            
            activeListItems.Remove(listItem);
            listItem.gameObject.SetActive(false);
            activeRocketItemsPool.Push(listItem);
        }
    }

    private void UpdateLabels()
    {
        CurrentRocketCargoLabel.SetText("Current Cargo: {0} m3", rocketCurrentCargoManager.TotalRocketCargoVolume);
        RocketCargoBayUsageLabel.SetText("Rocket Cargo Bay Usage: {0}/{1}", rocketCurrentCargoManager.TotalRocketCargoBaysUsed, DataValues.RocketCargoBaysAmount);
        FuelRequiredLabel.SetText("Fuel Required: {0}", rocketCurrentCargoManager.CurrentCargoLaunchFuelCost);
        FuelInStorageLabel.SetText("Fuel In Storage: {0}", thisPlanet.PlanetStorageManager.GetUnitsStored(StorableType.RocketFuel));

        CapacityProgressBar.fillAmount =
            Mathf.InverseLerp(0f, DataValues.RocketCargoholdCapacity, rocketCurrentCargoManager.TotalRocketCargoVolume);
    }

    private void UpdateLaunchButtonState()
    {
        if (rocketCurrentCargoManager.TotalRocketCargoBaysUsed != 0 &&
            Math.Abs(rocketCurrentCargoManager.TotalRocketCargoVolume) > 0.0001f &&
            rocketCurrentCargoManager.TotalRocketCargoVolume <= DataValues.RocketCargoholdCapacity &&
            rocketCurrentCargoManager.TotalRocketCargoBaysUsed <= DataValues.RocketCargoBaysAmount &&
            rocketCurrentCargoManager.CurrentCargoLaunchFuelCost <= thisPlanet.PlanetStorageManager.GetUnitsStored(StorableType.RocketFuel))
        {
            LaunchButton.interactable = true;
        }
        else
        {
            LaunchButton.interactable = false;
        }
    }

    private void LaunchOnClick()
    {
        rocketCurrentCargoManager.LaunchAndClearCargoContents();
    }
    
    private Stack<ActiveRocketLaunchItemComponent> FillActiveRocketLaunchItemsPool()
    {
        var itemsPool = new Stack<ActiveRocketLaunchItemComponent>();

        //TODO: concerned that only 10 active list items is possible
        for (int i = 0; i < 10; i++)
        {
            var activeListItem = Instantiate(ActiveRocketLaunchItemPrefab, ActiveRocketLaunchItemsContainer.transform);
            itemsPool.Push(activeListItem.GetComponent<ActiveRocketLaunchItemComponent>());
            activeListItem.gameObject.SetActive(false);
        }

        return itemsPool;
    }
}