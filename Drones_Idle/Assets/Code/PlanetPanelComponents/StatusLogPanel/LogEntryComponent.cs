﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LogEntryComponent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI LogEntryLabel;

    public void InitEntry(string logString)
    {
        LogEntryLabel.SetText(logString);
    }
}