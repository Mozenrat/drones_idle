﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StatusLogPanelComponent : MonoBehaviour
{
    [SerializeField] private GameObject StatusLogPanel;
    [SerializeField] private GameObject CombatLogPanel;
    [SerializeField] private RectTransform StatusLogPanelContentContainer;
    [SerializeField] private RectTransform CombatLogPanelContentContainer;
    
    [SerializeField] private LogEntryComponent LogEntryPrefab;

    [SerializeField] private Button StatusLogSwitchButton;
    [SerializeField] private Button CombatLogSwitchButton;

    private Queue<LogEntryComponent> statusLogEntryObjectsQueue;
    private Queue<LogEntryComponent> combatLogEntryObjectsQueue;

    private void Awake()
    {
        //Pool all UI Prefab Instances at Awake time
        statusLogEntryObjectsQueue = new Queue<LogEntryComponent>(DataValues.StatusLogLength);
        for (int i = 0; i < DataValues.StatusLogLength; i++)
        {
            var statusLogEntry = Instantiate(LogEntryPrefab, StatusLogPanelContentContainer);
            statusLogEntry.gameObject.SetActive(false);
            statusLogEntryObjectsQueue.Enqueue(statusLogEntry);
        }
        
        combatLogEntryObjectsQueue = new Queue<LogEntryComponent>(DataValues.CombatLogLength);
        for (int i = 0; i < DataValues.CombatLogLength; i++)
        {
            var combatLogEntry = Instantiate(LogEntryPrefab, CombatLogPanelContentContainer);
            combatLogEntry.gameObject.SetActive(false);
            combatLogEntryObjectsQueue.Enqueue(combatLogEntry);
        }
        
        StatusLogSwitchButton.onClick.AddListener(SwitchToStatusLog);
        CombatLogSwitchButton.onClick.AddListener(SwitchToCombatLog);
    }

    private void OnEnable()
    {
        foreach (var logString in GlobalGameStateController.Instance.LogMessageList)
        {
            LogNewStatusMessage(logString);
        }
    }

    public void LogNewStatusMessage(string message)
    {
        //Dequeue a first UI instance from the pool suitable for next log line
        var statusLogEntry = statusLogEntryObjectsQueue.Dequeue();
        statusLogEntry.gameObject.SetActive(true);
        
        statusLogEntry.InitEntry(message);
        statusLogEntry.transform.SetAsLastSibling();
        
        //Enqueue the same UI instance at the end of the pool
        statusLogEntryObjectsQueue.Enqueue(statusLogEntry);
    }

    public void LogNewCombatMessage(string message)
    {
        var combatLogEntry = combatLogEntryObjectsQueue.Dequeue();
        combatLogEntry.gameObject.SetActive(true);
        
        combatLogEntry.InitEntry(message);
        combatLogEntry.transform.SetAsLastSibling();
        
        combatLogEntryObjectsQueue.Enqueue(combatLogEntry);
    }

    private void SwitchToStatusLog()
    {
        if (StatusLogPanel.activeSelf) return;
        
        CombatLogPanel.SetActive(false);
        StatusLogPanel.SetActive(true);
    }

    private void SwitchToCombatLog()
    {
        if (CombatLogPanel.activeSelf) return;
        
        StatusLogPanel.SetActive(false);
        CombatLogPanel.SetActive(true);
    }
}