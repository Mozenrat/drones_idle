﻿using System;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MiningListItemComponent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI OreNameLabel;
    [SerializeField] private TextMeshProUGUI OreStoredLabel;
    [SerializeField] private TMP_InputField DronesAssignedInput;
    [SerializeField] private TextMeshProUGUI MiningProgressLabel;
    [SerializeField] private Button DronePlusButton;
    [SerializeField] private Button DroneMinusButton;
    [SerializeField] private Image MiningProgressBar;

    private IOre thisOre;
    private PlanetContents thisPlanet;
    private MiningCalculation thisMiningCalc;

    private string MinedPerOperationLabel => $" {thisOre.Name} mined per operation";
    private string MinedPerSecondLabel => $" {thisOre.Name} mined per second"; 
    
    private float labelUpdateInterval;
    private float labelUpdateIntervalMax = .02f;
    
    private readonly StringBuilder stringBuilder = new StringBuilder(100);

    public void InitMiningListItem(IOre oreInstance, PlanetContents planet)
    {
        thisOre = oreInstance;
        thisPlanet = planet;
        
        OreNameLabel.SetText(thisOre.Name);
        
        thisPlanet.PlanetCalculationsManager.InitMiningCalculation(thisPlanet, thisOre.OreType, out thisMiningCalc);
        
        var droneCountThisOre = thisPlanet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Mining, thisOre.OreType, ProductionRecipeType.None);
        DronesAssignedInput.text = droneCountThisOre.ToString();
        
        var nameTooltip = OreNameLabel.gameObject.AddComponent<TooltipProvider>();
        nameTooltip.InitTooltip(GetTooltipString);
        var progressTooltip = MiningProgressBar.gameObject.AddComponent<TooltipProvider>();
        progressTooltip.InitTooltip(GetTooltipString);
        
        DronePlusButton.onClick.AddListener(AssignDroneToMiningTask);
        DroneMinusButton.onClick.AddListener(UnassignDroneFromMiningTask);
        UpdateListItem();
    }

    private void Update()
    {
        labelUpdateInterval -= Time.deltaTime;
        if (labelUpdateInterval < 0)
        {
            labelUpdateInterval = labelUpdateIntervalMax;
            UpdateListItem();
        }
    }

    private void UpdateListItem()
    {
        OreStoredLabel.SetText("{0:1}",thisPlanet.PlanetStorageManager.GetUnitsStored(thisOre.StorableType));

        if (thisMiningCalc == null) return;
        
        MiningProgressLabel.SetText("{0:1} s", thisMiningCalc.CurrentMiningProgress);
        MiningProgressBar.fillAmount = Mathf.InverseLerp(0f, thisMiningCalc.MiningCycleDuration,
            thisMiningCalc.MiningCycleCurrent);
    }

    private string GetTooltipString()
    {
        stringBuilder.Clear();
        
        if (thisMiningCalc == null || Math.Abs(thisMiningCalc.MinedPerOperation) < 0.0001f)
        {
            stringBuilder.Append("This ore is not being mined currently");
            return stringBuilder.ToString();
        }

        stringBuilder.Append(thisMiningCalc.MinedPerOperation).AppendLine(MinedPerOperationLabel);
        stringBuilder.Append(thisMiningCalc.MinedPerSecond).Append(MinedPerSecondLabel);
        return stringBuilder.ToString();
    }

    private void AssignDroneToMiningTask()
    {
        thisPlanet.PlanetDroneManager.AssignDronesToRole(DroneRole.Mining, thisOre.OreType, ProductionRecipeType.None, 1);

        var currentlyAssignedDrones = thisPlanet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Mining, thisOre.OreType, ProductionRecipeType.None);
        if (currentlyAssignedDrones >= 1)
        {
            thisPlanet.PlanetCalculationsManager.InitMiningCalculation(thisPlanet, thisOre.OreType, out thisMiningCalc);
            DronesAssignedInput.text = currentlyAssignedDrones.ToString();
        }
    }

    private void UnassignDroneFromMiningTask()
    {
        thisPlanet.PlanetDroneManager.UnassignDronesFromRole(DroneRole.Mining, thisOre.OreType, ProductionRecipeType.None, 1);
        var currentlyAssignedDrones = thisPlanet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Mining, thisOre.OreType, ProductionRecipeType.None);
        DronesAssignedInput.text = currentlyAssignedDrones.ToString();
    }
}