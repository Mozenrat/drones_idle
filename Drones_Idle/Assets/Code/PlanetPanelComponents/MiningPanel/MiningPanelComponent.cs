﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Code.Utility;
using TMPro;
using UnityEngine;

public class MiningPanelComponent : MonoBehaviour
{
    [SerializeField] private MiningListItemComponent MiningListItemPrefab;
    [SerializeField] private GameObject MiningListItemsContainerParent;

    private PlanetContents selectedPlanet;

    private void Awake()
    {
        selectedPlanet = GlobalGameStateController.Instance.GetPlanetById(1);

        foreach (var oreBucket in selectedPlanet.OreBuckets)
        {
            var listItem = Instantiate(MiningListItemPrefab, MiningListItemsContainerParent.transform);
            listItem.InitMiningListItem(DataInstanceFactory.GetOreInstance(oreBucket.BucketOreType), selectedPlanet);
            listItem.transform.SetAsLastSibling();
        }
    }
}