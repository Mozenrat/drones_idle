﻿using System.Collections;
using System.Collections.Generic;
using Code.Utility;
using UnityEngine;
using TMPro;

/// <summary>
/// Class rendering duration of task currently in Setup Production page > selected recipe panel
/// </summary>
public class SelectedRecipeDurationComponent : MonoBehaviour
{
    public TextMeshProUGUI DurationWithDronesLabel;

    public void SetLabels(float duration)
    {
        DurationWithDronesLabel.SetText("{0} s", duration);
    }
}