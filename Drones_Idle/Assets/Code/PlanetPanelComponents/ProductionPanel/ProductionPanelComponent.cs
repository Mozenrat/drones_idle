﻿using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;
using UnityEngine.UI;

public class ProductionPanelComponent : MonoBehaviour
{
    [SerializeField] private Button SetupNewProductionButton;
    [SerializeField] private Button ActiveProductionButton;
    [SerializeField] private GameObject SetupNewProductionPanel;
    [SerializeField] private GameObject ActiveProductionPanel;
    
    [SerializeField] private GameObject RecipesSelectorContent;
    [SerializeField] private ProductionRecipeListHeader RecipeSelectorHeaderPrefab;
    [SerializeField] private ProductionRecipeListItem RecipeSelectorListItemPrefab;
    
    [SerializeField] private SelectedRecipeComponent SelectedRecipeComponent;

    [SerializeField] private TextMeshProUGUI SelectedRecipeLabel;
    [SerializeField] private TMP_InputField ProductionQuantityInput;
    [SerializeField] private Button AddDroneToSelectedRecipeButton;
    [SerializeField] private Button RemoveDroneFromSelectedRecipeButton;
    [SerializeField] private TMP_InputField DronesAssignedToRecipeLabel;
    [SerializeField] private Button AddToProductionQueueButton;

    private IProductionRecipe selectedRecipe;
    private int selectedRecipeAssignedDronesAmount = 0;
    private PlanetContents thisPlanet;

    private float activeProductionCheckInterval;
    private float activeProductionCheckIntervalMax = .1f;

    private void Awake()
    {
        thisPlanet = GlobalGameStateController.Instance.GetPlanetById(1);
        
        AddDroneToSelectedRecipeButton.onClick.AddListener(AssignDroneToProductionTask);
        RemoveDroneFromSelectedRecipeButton.onClick.AddListener(UnassignDroneFromProductionTask);
        AddToProductionQueueButton.onClick.AddListener(SendSelectedRecipeInProductionQueue);
        
        SetupNewProductionButton.onClick.AddListener(GoToSetupProduction);
        ActiveProductionButton.onClick.AddListener(GoToActiveProduction);
    }

    private void OnEnable()
    {
        RenderKnownRecipes();
    }

    private void Update()
    {
        activeProductionCheckInterval -= Time.deltaTime;
        if (activeProductionCheckInterval < 0)
        {
            activeProductionCheckInterval = activeProductionCheckIntervalMax;
            DronesAssignedToRecipeLabel.text = selectedRecipeAssignedDronesAmount.ToString();
        }
    }
    
    public void SelectRecipe(IProductionRecipe productionRecipe)
    {
        selectedRecipe = productionRecipe;
        SelectedRecipeLabel.SetText(productionRecipe.RecipeName);
    }

    private void SendSelectedRecipeInProductionQueue()
    {
        if (selectedRecipe == null)
        {
            NotificationController.ShowNotification_Static(DataValues.NoProductionRecipeSelected, true);
            return;
        }

        if (!int.TryParse(ProductionQuantityInput.text, out int parseResult) ||
            parseResult == 0)
        {
            NotificationController.ShowNotification_Static(DataValues.NoValidProductionAmount, true);
            return;
        }

        if (thisPlanet.PlanetCalculationsManager.GetProductionCalculations.Exists(item =>
            item.Recipe.ProductionRecipeType == selectedRecipe.ProductionRecipeType))
        {
            NotificationController.ShowNotification_Static(DataValues.SelectedRecipeAlreadyInProduction, true);
            return;
        }
        
        thisPlanet.PlanetDroneManager.AssignDronesToRole(DroneRole.Production, OreType.None,
            selectedRecipe.ProductionRecipeType, selectedRecipeAssignedDronesAmount);

        thisPlanet.PlanetCalculationsManager.InitProductionCalculation(
            selectedRecipe,
            thisPlanet,
            parseResult,
            out _);
        
        selectedRecipe = null;
        SelectedRecipeLabel.SetText(DataValues.ProductionPageUiSelectRecipeText);
        ProductionQuantityInput.text = "";
        selectedRecipeAssignedDronesAmount = 0;
        
        NotificationController.ShowNotification_Static(DataValues.ProductionTaskCreationSuccessful, false);
    }

    private void GoToSetupProduction()
    {
        ActiveProductionPanel.SetActive(false);
        SetupNewProductionPanel.SetActive(true);
    }

    private void GoToActiveProduction()
    {
        SetupNewProductionPanel.SetActive(false);
        ActiveProductionPanel.SetActive(true);
    }

    private void AssignDroneToProductionTask()
    {
        if (thisPlanet.PlanetDroneManager.GetUnassignedDronesCount() > selectedRecipeAssignedDronesAmount)
        {
            selectedRecipeAssignedDronesAmount++;
        }
    }

    private void UnassignDroneFromProductionTask()
    {
        if (selectedRecipeAssignedDronesAmount > 0)
        {
            selectedRecipeAssignedDronesAmount--;
        }
    }

    private void RenderKnownRecipes()
    {
        for (int i = 0; i < RecipesSelectorContent.transform.childCount; i++)
        {
            Destroy(RecipesSelectorContent.transform.GetChild(i).gameObject);
        }
        
        //Create Header for each recipe category
        foreach (var recipeCategory in GlobalGameStateController.Instance.MothershipEntity.TechnologyController.KnownPlanetProductionRecipes)
        {
            var recipeCategoryHeader = Instantiate(RecipeSelectorHeaderPrefab, RecipesSelectorContent.transform);
            recipeCategoryHeader.SetHeaderLabel(recipeCategory.RecipeCategoryName);

            //populate category with recipes
            foreach (var item in recipeCategory.KnownProductionRecipesInCategory)
            {
                var recipeGameObject = Instantiate(RecipeSelectorListItemPrefab, RecipesSelectorContent.transform);
                recipeGameObject.SetLabels(item, SelectedRecipeComponent);
            }
        }
    }
}