﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// class populating all of the selected recipe panel in Setup Production subpage
/// </summary>
public class SelectedRecipeComponent : MonoBehaviour
{
    [SerializeField] private SelectedRecipeListItem RecipeListItemPrefab;
    [SerializeField] private GameObject SelectedRecipeSpacerPrefab;
    [SerializeField] private SelectedRecipeDurationComponent SelectedRecipeDurationPrefab;

    [SerializeField] private GameObject ContainerParent;

    [SerializeField] private ProductionPanelComponent ProductionPanelRef;

    public void PopulateSelectedRecipe(IProductionRecipe productionRecipe)
    {
        ProductionPanelRef.SelectRecipe(productionRecipe);

        ClearContainer();

        int nestedMarginInitial = 10;
        var firstLevel = Instantiate(RecipeListItemPrefab, ContainerParent.transform);
        firstLevel.SetLabelsFirstItem(productionRecipe.RecipeName, nestedMarginInitial);

        RenderSelectedRecipeListItemRecursive(productionRecipe, nestedMarginInitial, 1);
        RenderSelectedRecipeDurationAndComplexity(productionRecipe);
    }

    private void RenderSelectedRecipeDurationAndComplexity(IProductionRecipe productionRecipe)
    {
        Instantiate(SelectedRecipeSpacerPrefab, ContainerParent.transform);
        
        var durationLabel = Instantiate(SelectedRecipeDurationPrefab, ContainerParent.transform);
        durationLabel.GetComponent<SelectedRecipeDurationComponent>().SetLabels(productionRecipe.ProductionDurationBase);
    }

    private void RenderSelectedRecipeListItemRecursive(IProductionRecipe productionRecipe, int nestedMargin, int quantityMultiplier)
    {
        //loop over all the materials
        foreach (var item in productionRecipe.BaseProductionCost)
        {
            //for each material instantiate the listItemPrefab
            var listItem = Instantiate(RecipeListItemPrefab, ContainerParent.transform);
            var storableInstance = DataInstanceFactory.GetStorableInstance(item.Key);

            //if material is a compound material e.g. is a ProductionRecipe itself => recurse
            if (storableInstance is IProductionRecipe asRecipeInstance)
            {
                var newMargin = nestedMargin + 20;
                listItem.SetLabels(asRecipeInstance, this, newMargin, item.Value);
                //recurse
                RenderSelectedRecipeListItemRecursive(asRecipeInstance, newMargin, (int) item.Value);
            }
            //if material is a simple Storable just render it without recursion
            else
            {
                listItem.SetLabels(storableInstance, nestedMargin + 20, item.Value * quantityMultiplier);
            }
        }
    }

    private void ClearContainer()
    {
        for (int i = 0; i < ContainerParent.transform.childCount; i++)
        {
            Destroy(ContainerParent.transform.GetChild(i).gameObject);
        }
    }
}