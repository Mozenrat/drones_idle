﻿using System;
using System.Collections;
using System.Collections.Generic;
using Code.Utility;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Class defining every recipe line in Setup Production page > selected recipe panel
/// </summary>
public class SelectedRecipeListItem : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public TextMeshProUGUI RecipeItemName;
    public TextMeshProUGUI Quantity;
    
    [SerializeField] private Image ListItemBackground;

    private IProductionRecipe thisItemRecipe;
    private SelectedRecipeComponent selectedRecipeReference;

    private float ThisQuantityInStorage => GlobalGameStateController.Instance.GetPlanetById(1).PlanetStorageManager
        .GetUnitsStored(thisStorable.StorableType);//TODO: fix dependency on certain planet 
    
    private IStorable thisStorable;
    private float thisQuantityRequired;
    
    private float updateLabelsPeriodCurrent = 0f;
    private float updateLabelsPeriodMax = 0.25f;

    public void SetLabels(IProductionRecipe productionRecipe, SelectedRecipeComponent selectedRecipeComponent, int nestedLevel, float quantityRequired)
    {
        thisStorable = productionRecipe;
        thisItemRecipe = productionRecipe;
        selectedRecipeReference = selectedRecipeComponent;
        thisQuantityRequired = quantityRequired;

        RecipeItemName.SetText(productionRecipe.RecipeName);
        RecipeItemName.margin = new Vector4(nestedLevel, 4, 4, 4);

        UpdateListItemLabels();
    }

    public void SetLabels(IStorable storable, int nestedLevel, float quantityRequired)
    {
        thisStorable = storable;
        thisQuantityRequired = quantityRequired;
        
        RecipeItemName.SetText(storable.StorableName);
        RecipeItemName.margin = new Vector4(nestedLevel, 4, 4, 4);

        UpdateListItemLabels();
    }

    public void SetLabelsFirstItem(string recipeName, int nestedLevel)
    {
        RecipeItemName.SetText(recipeName);
        RecipeItemName.margin = new Vector4(nestedLevel, 4, 4, 4);
        Quantity.SetText("x1"); // TODO: need to make ingredient quantities reactive at some point
        Quantity.color = DataValues.SelectedRecipeIngredientEnoughInStorageColor;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (thisItemRecipe != null && selectedRecipeReference != null)
        {
            selectedRecipeReference.PopulateSelectedRecipe(thisItemRecipe);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (thisItemRecipe != null && selectedRecipeReference != null)
        {
            ListItemBackground.color = DataValues.RecipeListItemBackgroundHoverColor;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (thisItemRecipe != null && selectedRecipeReference != null)
        {
            ListItemBackground.color = DataValues.RecipeListItemBackgroundRegularColor;
        }
    }
    
    private void Update()
    {
        if (thisStorable == null) return; // TODO: remove hacky return point if first item in selected recipe list 
        
        updateLabelsPeriodCurrent += Time.deltaTime;
        if (updateLabelsPeriodCurrent >= updateLabelsPeriodMax)
        {
            updateLabelsPeriodCurrent = 0f;
            UpdateListItemLabels();
        }
    }

    private void UpdateListItemLabels()
    {
        Quantity.SetText("{0} / {1:1}",thisQuantityRequired.ToTwoDigitFloat(), ThisQuantityInStorage.ToTwoDigitFloat());

        bool reddenColor = ThisQuantityInStorage < thisQuantityRequired;
        if (reddenColor)
        {
            RecipeItemName.color = DataValues.SelectedRecipeIngredientNotEnoughInStorageColor;
            Quantity.color = DataValues.SelectedRecipeIngredientNotEnoughInStorageColor;
        }
        else
        {
            RecipeItemName.color = DataValues.SelectedRecipeIngredientEnoughInStorageColor;
            Quantity.color = DataValues.SelectedRecipeIngredientEnoughInStorageColor;
        }
    }
}