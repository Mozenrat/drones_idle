﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using System.Text;
using Code.Utility;
using UnityEngine.EventSystems;

/// <summary>
/// class rendering each individual production queue list item in Active Production subpage
/// </summary>
public class ActiveProductionListItem : MonoBehaviour
{
    [SerializeField] private Image ProductionProgress;
    [SerializeField] private TextMeshProUGUI RecipeNameLabel;
    [SerializeField] private TextMeshProUGUI RemainingTextLabel;
    [SerializeField] private TextMeshProUGUI InProductionQuantityLabel;
    [SerializeField] private TMP_InputField DronesOnThisTaskLabel;
    [SerializeField] private Button AddDroneToTaskButton;
    [SerializeField] private Button RemoveDroneFromTaskButton;
    [SerializeField] private Button CancelThisProductionTaskButton;

    public ProductionCalculation PeekProductionCalculation => calc;
    public IProductionRecipe PeekRecipe => calc.Recipe;

    private PlanetContents thisPlanet;
    private ProductionCalculation calc;

    private int thisRecipeAssignedDronesAmount;

    private float labelUpdateInterval;
    private float labelUpdateIntervalMax = .02f;
    
    private readonly StringBuilder stringBuilder = new StringBuilder(100);

    public void InitActiveProductionListItem(PlanetContents planet, ProductionCalculation productionCalculation)
    {
        thisPlanet = planet;
        calc = productionCalculation;
        thisRecipeAssignedDronesAmount =
            thisPlanet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Production, OreType.None, calc.Recipe.ProductionRecipeType);
        
        AddDroneToTaskButton.onClick.RemoveAllListeners();
        RemoveDroneFromTaskButton.onClick.RemoveAllListeners();
        CancelThisProductionTaskButton.onClick.RemoveAllListeners();
        
        AddDroneToTaskButton.onClick.AddListener(AddDroneToProductionTask);
        RemoveDroneFromTaskButton.onClick.AddListener(RemoveDroneFromProductionTask);
        CancelThisProductionTaskButton.onClick.AddListener(CancelThisProductionTask);
        
        var nameTooltip = RecipeNameLabel.gameObject.AddComponent<TooltipProvider>();
        nameTooltip.InitTooltip(GetTooltipString);
        var progressTooltip = RemainingTextLabel.gameObject.AddComponent<TooltipProvider>();
        progressTooltip.InitTooltip(GetTooltipString);
    }

    private void Update()
    {
        labelUpdateInterval -= Time.deltaTime;
        if (labelUpdateInterval < 0)
        {
            labelUpdateInterval = labelUpdateIntervalMax;
            UpdateListItem();
        }
    }

    private void OnDisable()
    {
        if (MouseUtility.IsMouseOverUiGameObject(gameObject))
        {
            TooltipController.HideTooltip_Static();
        }
    }

    private string GetTooltipString()
    {
        stringBuilder.Clear();
        
        if (thisRecipeAssignedDronesAmount < 1)
        {
            stringBuilder.AppendLine("This production task is currently on hold");
            stringBuilder.AppendLine("Assign more drones to continue production");
            return stringBuilder.ToString();
        }

        if (!calc.IsActive && !calc.IsEnoughResourcesToStart)
        {
            stringBuilder.AppendLine("Not enough resources in storage to start production");
            stringBuilder.AppendLine();
            foreach (var item in calc.Recipe.BaseProductionCost)
            {
                var storable = DataInstanceFactory.GetStorableInstance(item.Key);
                stringBuilder.Append($"    {storable.StorableName} required: ").Append(item.Value)
                    .Append(" / ")
                    .Append(thisPlanet.PlanetStorageManager.GetUnitsStored(item.Key).ToTwoDigitFloat()).AppendLine();
            }

            return stringBuilder.ToString();
        }

        stringBuilder.Append("Time to produce one unit: ").Append(calc.TimeForOneCycle).AppendLine(" sec");
        stringBuilder.Append("Remaining: ").Append(calc.TimeToCompletion).AppendLine(" sec");
        return stringBuilder.ToString();
    }
    
    private void AddDroneToProductionTask()
    {
        thisPlanet.PlanetDroneManager.AssignDronesToRole(DroneRole.Production, OreType.None, calc.Recipe.ProductionRecipeType, 1);
        thisRecipeAssignedDronesAmount =
            thisPlanet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Production, OreType.None, calc.Recipe.ProductionRecipeType);
    }

    private void RemoveDroneFromProductionTask()
    {
        if (thisRecipeAssignedDronesAmount <= 0) return;
        
        thisPlanet.PlanetDroneManager.UnassignDronesFromRole(DroneRole.Production, OreType.None, calc.Recipe.ProductionRecipeType, 1);
        
        thisRecipeAssignedDronesAmount =
            thisPlanet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Production, OreType.None, calc.Recipe.ProductionRecipeType);
    }

    private void CancelThisProductionTask()
    {
        StatusLogController.LogStatusMessage_Static($"Production of {calc.UnitsToProduce} {PeekRecipe.RecipeName} cancelled");
        calc.CancelThisProductionTask();
    }

    private void UpdateListItem()
    {
        RecipeNameLabel.SetText(calc.Recipe.RecipeName);
        ProductionProgress.fillAmount = Mathf.InverseLerp(0, calc.ProductionCycleDuration, calc.ProductionCycleCurrent);
        DronesOnThisTaskLabel.text = thisRecipeAssignedDronesAmount.ToString();
        InProductionQuantityLabel.SetText("{0}", calc.UnitsToProduce);
    }
}