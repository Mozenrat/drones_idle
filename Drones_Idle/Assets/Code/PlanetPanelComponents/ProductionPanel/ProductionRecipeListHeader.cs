﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

/// <summary>
/// class defining each recipe category header in Setup Production subpage
/// </summary>
public class ProductionRecipeListHeader : MonoBehaviour
{
    public TextMeshProUGUI HeaderLabel;
    
    public void SetHeaderLabel(string headerLabel)
    {
        HeaderLabel.SetText(headerLabel);
    }
}