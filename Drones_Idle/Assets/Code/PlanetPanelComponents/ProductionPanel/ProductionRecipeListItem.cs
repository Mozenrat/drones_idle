﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using Code.Utility;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// class defining each available recipe in Setup Production subpage
/// </summary>
public class ProductionRecipeListItem : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public TextMeshProUGUI RecipeName;
    public TextMeshProUGUI QuantityInStorage;
    public TextMeshProUGUI ProductionDurationBase;

    private IProductionRecipe thisItemRecipe;
    private SelectedRecipeComponent selectedRecipeReference;

    private Image itemBackgroundImage;

    public void SetLabels(IProductionRecipe recipeItem, SelectedRecipeComponent selectedRecipeComponent)
    {
        thisItemRecipe = recipeItem;
        selectedRecipeReference = selectedRecipeComponent;

        itemBackgroundImage = GetComponent<Image>();

        var itemStored = GlobalGameStateController.Instance.GetPlanetById(1).PlanetStorageManager.GetUnitsStored(recipeItem.StorableType);
        TimeSpan timespan = TimeSpan.FromSeconds(recipeItem.ProductionDurationBase);
        string formattedDuration = timespan.ToString(recipeItem.ProductionDurationBase > 3600f ? @"h\:m\:ss" : @"m\:ss");

        RecipeName.SetText(recipeItem.RecipeName);
        QuantityInStorage.SetText("{0}",itemStored.ToTwoDigitFloat());
        ProductionDurationBase.SetText(formattedDuration);
    }
    
    public void OnPointerClick(PointerEventData eventData)
    {
        selectedRecipeReference.PopulateSelectedRecipe(thisItemRecipe);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        itemBackgroundImage.color = DataValues.RecipeListItemBackgroundHoverColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        itemBackgroundImage.color = DataValues.RecipeListItemBackgroundRegularColor;
    }
}