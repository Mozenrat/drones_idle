﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveProductionPanelComponent : MonoBehaviour
{
    [SerializeField] private GameObject ActiveProductionContent;
    [SerializeField] private ActiveProductionListItem ActiveProductionListItemPrefab;
    
    private List<ActiveProductionListItem> activeListItems;
    private PlanetContents thisPlanet;
    
    private void Awake()
    {
        thisPlanet = GlobalGameStateController.Instance.GetPlanetById(1);
        activeListItems = new List<ActiveProductionListItem>();

        thisPlanet.PlanetCalculationsManager.OnProductionTasksChanged += RenderActiveProductionItem;
    }

    private void OnEnable()
    {
        RenderAllActiveProductionItems();
    }

    private void RenderActiveProductionItem(object sender, PlanetCalculationsManager.OnProductionChangeEventArgs e)
    {
        if (e.IsCalculationBeingAdded)
        {
            var listItem = Instantiate(ActiveProductionListItemPrefab, ActiveProductionContent.transform);
            listItem.InitActiveProductionListItem(thisPlanet, e.CalcRef);
            activeListItems.Add(listItem);
        }
        else
        {
            var listItem = activeListItems.Find(item => item.PeekProductionCalculation == e.CalcRef);
            if (listItem == null) 
                return;
            
            activeListItems.Remove(listItem);
            Destroy(listItem.gameObject);
        }
    }

    private void RenderAllActiveProductionItems()
    {
        foreach (var item in activeListItems)
        {
            Destroy(item.gameObject);
        }
        activeListItems.Clear();
        
        foreach (var calculation in thisPlanet.PlanetCalculationsManager.GetProductionCalculations)
        {
            var listItem = Instantiate(ActiveProductionListItemPrefab, ActiveProductionContent.transform);
            listItem.InitActiveProductionListItem(thisPlanet, calculation);
            activeListItems.Add(listItem);
        }
    }
}