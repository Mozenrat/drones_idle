using System;
using System.Globalization;
using System.Runtime.Serialization.Json;
using System.Text;
using OdinSerializer.Utilities;
using UnityEngine;
using UnityEngine.EventSystems;

public class TooltipProvider : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private Func<string> getTooltipFunc;

    public void InitTooltip(Func<string> getTooltip)
    {
        getTooltipFunc = getTooltip;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        TooltipController.ShowTooltip_Static(getTooltipFunc, this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        TooltipController.HideTooltip_Static();
    }
}