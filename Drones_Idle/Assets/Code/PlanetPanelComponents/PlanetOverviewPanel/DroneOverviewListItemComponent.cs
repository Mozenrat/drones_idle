﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DroneOverviewListItemComponent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI DroneNameLabel;
    [SerializeField] private TextMeshProUGUI DronesOccupiedLabel;
    [SerializeField] private TextMeshProUGUI DronesOperationalLabel;
    [SerializeField] private TextMeshProUGUI DronesInStorageLabel;
    [SerializeField] private TextMeshProUGUI DronesBandwidthUsageLabel;

    [SerializeField] private Button PutOnlineButton;
    [SerializeField] private Button PutOfflineButton;

    private bool initialized;
    private PlanetContents thisPlanet;
    private WorkerDrone thisDrone;
    
    private float labelUpdateInterval;
    private float labelUpdateIntervalMax = .09f;

    public void InitDroneOverviewListItem(PlanetContents planet, WorkerDrone droneType)
    {
        thisPlanet = planet;
        thisDrone = droneType;
        initialized = true;
    }

    private void Start()
    {
        if (initialized)
        {
            PopulateDroneListItem();
        }
        else
        {
            Debug.Log("Failed to Initialize DroneOverviewListItem!");
        }
    }

    private void Update()
    {
        labelUpdateInterval -= Time.deltaTime;
        if (labelUpdateInterval < 0)
        {
            labelUpdateInterval = labelUpdateIntervalMax;
            UpdateLabels();
        }
    }

    private void UpdateLabels()
    {
        //(int totalCount, float totalBandwidth) = thisPlanet.PlanetDroneManager.GetTotalDroneCountAndBandwidthUsage(thisDrone.Name);
        //DronesOccupiedLabel.SetText("Occupied: {0}", thisPlanet.PlanetDroneManager.GetOccupiedDroneCount(thisDrone.Name));
        //DronesOperationalLabel.SetText("Operational: {0}", totalCount);
        DronesInStorageLabel.SetText("In Storage: {0}", thisPlanet.PlanetStorageManager.GetUnitsStored(thisDrone.StorableType));
        //DronesBandwidthUsageLabel.SetText("{0} MBit/s", totalBandwidth);
    }

    private void PopulateDroneListItem()
    {
        DroneNameLabel.SetText(thisDrone.Name);
        UpdateLabels();
        
        PutOnlineButton.onClick.AddListener(PutDroneOnline);
        PutOfflineButton.onClick.AddListener(PutDroneOffline);
    }

    private void PutDroneOnline()
    {
        if (!GlobalGameStateController.Instance.IsEnoughBandwidthForExtraDrone(thisDrone.BandwidthRequired) 
            || thisPlanet.PlanetStorageManager.GetUnitsStored(thisDrone.StorableType) < 1f) return; //TODO: notification here
        
        //thisPlanet.PlanetDroneManager.PutDroneOnline(DataInstanceFactory.GetDroneInstanceByStorableType(thisDrone.StorableType));
        //thisPlanet.PlanetStorageManager.SubtractUnitsFromStorage(thisDrone.StorableType, 1f);
    }

    private void PutDroneOffline()
    {
        //if (thisPlanet.PlanetDroneManager.GetUnoccupiedDroneCount() == 0) return;
        
        //thisPlanet.PlanetDroneManager.PutDroneOffline(thisDrone.Name);
        //thisPlanet.PlanetStorageManager.AddUnitsToStorage(thisDrone.StorableType, 1f);
    }
}