﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class PlanetOverviewComponent : MonoBehaviour
{
    [SerializeField] private GameObject DroneOverviewListItemPrefab;
    [SerializeField] private GameObject DroneOverviewContainerParent;
    [SerializeField] private GameObject DroneOverviewContainerSpacerReference;

    [SerializeField] private TextMeshProUGUI TotalPlanetBandwidthUsageLabel;

    private PlanetContents thisPlanet;
    //private List<IDroneBase> droneTypes;

    private float labelUpdateInterval;
    private float labelUpdateIntervalMax = .09f;

    private void Awake()
    {
        thisPlanet = GlobalGameStateController.Instance.GetPlanetById(1);
        //droneTypes = GetAllDroneTypesWithReflection();
    }

    private void Start()
    {
        PopulateDroneOverviewSection();
    }


    private void Update()
    {
        labelUpdateInterval -= Time.deltaTime;
        if (labelUpdateInterval < 0)
        {
            labelUpdateInterval = labelUpdateIntervalMax;
            UpdateLabels();
        }
    }

    private void UpdateLabels()
    {
        TotalPlanetBandwidthUsageLabel.SetText("Planet bandwidth usage: {0}/{1} MBit/s",
            thisPlanet.PlanetDroneManager.TotalPlanetBandwidthUsage,
            GlobalGameStateController.Instance.MothershipEntity.GlobalBandwidthLimit);
    }

    private void PopulateDroneOverviewSection()
    {
//        foreach (var drone in droneTypes)
//        {
            var listItem = Instantiate(DroneOverviewListItemPrefab, DroneOverviewContainerParent.transform);
            listItem.GetComponent<DroneOverviewListItemComponent>().InitDroneOverviewListItem(thisPlanet, new WorkerDrone());
//        }

        DroneOverviewContainerSpacerReference.transform.SetAsLastSibling();
    }

//    private List<IDroneBase> GetAllDroneTypesWithReflection()
//    {
//        var interfaceType = typeof(IDroneBase);
//        var all = AppDomain.CurrentDomain.GetAssemblies()
//            .SelectMany(x => x.GetTypes())
//            .Where(x => interfaceType.IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract)
//            .Select(x => Activator.CreateInstance(x)).ToList();
//        return all.ConvertAll(e => e as IDroneBase);
//    }
}