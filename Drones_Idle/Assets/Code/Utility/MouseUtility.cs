using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Code.Utility
{
    public static class MouseUtility
    {
        public static bool IsMouseOverUiGameObject(GameObject go)
        {
            var rectTransform = go.GetComponent<RectTransform>();
            return RectTransformUtility.RectangleContainsScreenPoint(rectTransform, Input.mousePosition, Camera.main);
        }
    }
}