using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Code.Utility.Debug
{
    public class DebugPanelComponent : MonoBehaviour
    {
        [SerializeField] private Button OpenDebugPanelButton;
        [SerializeField] private Button CloseDebugPanelButton;
        [SerializeField] private GameObject DebugMainPanel;
        [SerializeField] private TMP_Dropdown StorableDropdownPlanet;
        [SerializeField] private TMP_Dropdown StorableDropdownMothership;
        [SerializeField] private TMP_InputField StorableQuantityInputPlanet;
        [SerializeField] private TMP_InputField StorableQuantityInputMothership;
        
        private readonly Dictionary<int, StorableType> storablesIndex = new Dictionary<int, StorableType>();
        private readonly Vector2 openedPosition = Vector2.zero;
        private readonly Vector2 closedPosition = new Vector2(0f, -243f);
        
        private float slidingSpeed = 9f;
        private bool opening;
        private bool closing;
        private PlanetContents planet;
        private MothershipEntity mothershipEntity;
        
        private RectTransform PanelRectTransform => DebugMainPanel.GetComponent<RectTransform>();

        public void OpenDebugPanel()
        {
            opening = true;
            OpenDebugPanelButton.gameObject.SetActive(false);
            CloseDebugPanelButton.gameObject.SetActive(true);
        }

        public void CloseDebugPanel()
        {
            closing = true;
            OpenDebugPanelButton.gameObject.SetActive(true);
            CloseDebugPanelButton.gameObject.SetActive(false);
        }

        public void AddSelectedStorableToPlanet()
        {
            var amount = 1f;
            if (int.TryParse(StorableQuantityInputPlanet.text, out int value))
            {
                amount = value;
            }
            
            var storableToAdd = storablesIndex[StorableDropdownPlanet.value];
            planet.PlanetStorageManager.AddUnitsToStorage(storableToAdd, amount);
        }
        
        public void AddSelectedStorableToMothership()
        {
            var amount = 1f;
            if (int.TryParse(StorableQuantityInputMothership.text, out int value))
            {
                amount = value;
            }
            
            var storableToAdd = storablesIndex[StorableDropdownMothership.value];
            mothershipEntity.MothershipStorageManager.AddUnitsToStorage(storableToAdd, amount);
        }

        private void Start()
        {
            mothershipEntity = GlobalGameStateController.Instance.MothershipEntity;
            planet = GlobalGameStateController.Instance.GetPlanetById(1);
            PopulateStorableTypeDropdown(StorableDropdownPlanet);
            PopulateStorableTypeDropdown(StorableDropdownMothership);
        }

        private void Update()
        {
            AnimatePanelSliding();
        }

        private void PopulateStorableTypeDropdown(TMP_Dropdown dropdownInstance)
        {
            var index = 0;
            var options = new List<TMP_Dropdown.OptionData>();
            foreach (var item in (StorableType[])Enum.GetValues(typeof(StorableType)))
            {
                if (item == StorableType.None) continue;
                var storable = DataInstanceFactory.GetStorableInstance(item);
                options.Add(new TMP_Dropdown.OptionData(storable.StorableName));
                if (!storablesIndex.ContainsKey(index))
                {
                    storablesIndex.Add(index, item);
                }
                index++;
            }
            
            dropdownInstance.options = options;
        }

        private void AnimatePanelSliding()
        {
            if (opening)
            {
                var yCoord = Mathf.Lerp(PanelRectTransform.anchoredPosition.y, openedPosition.y, Time.deltaTime * slidingSpeed);
                PanelRectTransform.anchoredPosition = new Vector2(0f, yCoord);
                if (Mathf.Abs(PanelRectTransform.anchoredPosition.y - openedPosition.y) < 1f)
                {
                    opening = false;
                    PanelRectTransform.anchoredPosition = openedPosition;
                }
            }
            if (closing)
            {
                var yCoord = Mathf.Lerp(PanelRectTransform.anchoredPosition.y, closedPosition.y, Time.deltaTime * slidingSpeed);
                PanelRectTransform.anchoredPosition = new Vector2(0f, yCoord);
                if (Mathf.Abs(PanelRectTransform.anchoredPosition.y - closedPosition.y) < 3f)
                {
                    closing = false;
                    PanelRectTransform.anchoredPosition = closedPosition;
                }
            }
        }
    }
}