using System;

namespace Code.Utility
{
    public static class FloatUtility
    {
        public static float ToTwoDigitFloat (this float floatNum)
        {
            return (float) Math.Round(floatNum * 100f) / 100f;
        }
    }
}