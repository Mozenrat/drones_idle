using System;
using System.Globalization;
using System.Text;

namespace Code.Utility
{
    public static class StringUtility
    {
        private static readonly StringBuilder StringBuilder = new StringBuilder();
        
        public static string ToPrettyString(this float numFloat)
        {
            if (Math.Abs(numFloat % 1) > float.Epsilon)
            {
                return numFloat.ToString("0.0#", CultureInfo.InvariantCulture);                
            }

            return numFloat.ToString(CultureInfo.InvariantCulture);
        }

        public static int GetIntStringHash(int inputInt, string inputString)
        {
            StringBuilder.Clear();
            StringBuilder.Append(inputInt).Append(inputString);
            return StringBuilder.ToString().GetDeterministicHashCode();
        }

        public static int GetStringIntIntHash(string inputString, int inputInt1, int inputInt2)
        {
            StringBuilder.Clear();
            StringBuilder.Append(inputString).Append(inputInt1).Append(inputInt2);
            return StringBuilder.ToString().GetDeterministicHashCode();
        }
        
        private static int GetDeterministicHashCode(this string str)
        {
            unchecked
            {
                int hash1 = (5381 << 16) + 5381;
                int hash2 = hash1;

                for (int i = 0; i < str.Length; i += 2)
                {
                    hash1 = ((hash1 << 5) + hash1) ^ str[i];
                    if (i == str.Length - 1)
                        break;
                    hash2 = ((hash2 << 5) + hash2) ^ str[i + 1];
                }

                return hash1 + (hash2 * 1566083941);
            }
        }
    }
}