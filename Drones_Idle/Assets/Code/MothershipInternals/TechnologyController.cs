﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public class TechnologyController
{
    public class OnResearchTaskAddedArgs : EventArgs
    {
        public ResearchableType ResearchableType;
        public ResearchCalculation ResearchCalculation;
    }

    public class OnResearchTaskRemovedArgs : EventArgs
    {
        public ResearchableType ResearchableType;
    }

    public event EventHandler<OnResearchTaskAddedArgs> OnResearchTaskAdded;
    public event EventHandler<OnResearchTaskRemovedArgs> OnResearchTaskRemoved;

    public readonly List<KeyValuePair<ResearchableType, ResearchCalculation>> ActiveResearch;

    public readonly List<BuildingType> KnownBuildingTypes;
    public readonly List<ProductionCategory> KnownPlanetProductionRecipes;
    public readonly List<ResearchableType> CompletedResearch;

    public float TotalAssignedResearchDuration
    {
        get
        {
            float total = 0f;
            foreach (var activeResearchObject in ActiveResearch)
            {
                if (activeResearchObject.Value.ResearchCount > 1)
                {
                    //TODO: handle next research level duration increase to get proper calculation here
                    total += activeResearchObject.Value.ResearchTaskCycleTotalProgress * (activeResearchObject.Value.ResearchCount-1);
                }
                total += activeResearchObject.Value.TimeToCycleCompletion;
            }

            return total;
        }
    }

    public TechnologyPersistenceData GetPersistenceData
    {
        get
        {
            var data = new TechnologyPersistenceData
            {
                ActiveResearchData = new Dictionary<ResearchableType, int>(),
                CompletedResearchData = new List<ResearchableType>(),
                ResearchLevelsData = new Dictionary<ResearchableType, int>(),
                PartialResearchProgressData = new Dictionary<ResearchableType, float>()
            };

            //Fill partial progress data
            foreach (var partialProgressObject in PartialResearchProgress)
            {
                data.PartialResearchProgressData.Add(partialProgressObject.Key, partialProgressObject.Value);
            }
            var activeCalc = GlobalGameStateController.Instance.MothershipEntity
                .MothershipCalculationsManager.GetActiveResearchCalculation();
            data.PartialResearchProgressData.Add(activeCalc.PeekType, activeCalc.ResearchTaskCycleCurrentProgress);
            
            //Fill completed research data
            foreach (var researchableType in CompletedResearch)
            {
                data.CompletedResearchData.Add(researchableType);
            }
            
            //Fill completed research levels data
            foreach (var researchLevelsObject in ResearchLevels)
            {
                data.ResearchLevelsData.Add(researchLevelsObject.Key, researchLevelsObject.Value);
            }
            
            //Fill active research list
            foreach (var activeResearchObject in ActiveResearch)
            {
                data.ActiveResearchData.Add(activeResearchObject.Key, activeResearchObject.Value.ResearchCount);
            }
            
            return data;
        }
    }

    private readonly List<ResearchableType> AvailableResearch;
    private readonly Dictionary<ResearchableType, int> ResearchLevels;
    private readonly Dictionary<ResearchableType, float> PartialResearchProgress;

    public TechnologyController()
    {
        ActiveResearch = new List<KeyValuePair<ResearchableType, ResearchCalculation>>();
        KnownBuildingTypes = new List<BuildingType>();
        KnownPlanetProductionRecipes = new List<ProductionCategory>();
        AvailableResearch = new List<ResearchableType>();
        CompletedResearch = new List<ResearchableType>();
        ResearchLevels = new Dictionary<ResearchableType, int>();
        PartialResearchProgress = new Dictionary<ResearchableType, float>();
        
        InitAvailableResearch();
        InitKnownPlanetTechnology();
        InitKnownPlanetBuildings();
    }

    public void RemoveFromActiveResearch(ResearchableType researchableType, bool cancelled)
    {
        if (!ActiveResearch.Exists(item => item.Key == researchableType))
            throw new ArgumentException("Attempted to remove non-existing active research item");

        var calcManager = GlobalGameStateController.Instance.MothershipEntity.MothershipCalculationsManager;
        var dataInstance = DataInstanceFactory.GetResearchableInstance(researchableType);
        
        var activeCalcObject = ActiveResearch.Find(item => item.Key == researchableType);
        if (cancelled)
        {
            PartialResearchProgress.Add(activeCalcObject.Value.PeekType, activeCalcObject.Value.ResearchTaskCycleCurrentProgress);
            
            NotificationController.ShowNotification_Static($"Research was cancelled: {dataInstance.ResearchableName}", true);
        }
        else
        {
            NotificationController.ShowNotification_Static($"Research completed: {dataInstance.ResearchableName}", false);
        }
        
        calcManager.DisposeResearchCalculation(activeCalcObject.Value);
        ActiveResearch.Remove(activeCalcObject);
        
        OnResearchTaskRemoved?.Invoke(null, new OnResearchTaskRemovedArgs {ResearchableType = researchableType});
    }

    public void AddToActiveResearch(ResearchableType researchableType)
    {
        var dataInstance = DataInstanceFactory.GetResearchableInstance(researchableType);

        if (ActiveResearch.Exists(item => item.Key == researchableType))
        {
            ActiveResearch.Find(item => item.Key == researchableType).Value.AddResearchCount(1);
        }
        else
        {
            GlobalGameStateController.Instance.MothershipEntity.MothershipCalculationsManager.InitResearchCalculation(dataInstance, out var calculation);
            ActiveResearch.Add(new KeyValuePair<ResearchableType, ResearchCalculation>(researchableType, calculation));

            if (PartialResearchProgress.ContainsKey(researchableType))
            {
                calculation.ResearchTaskCycleCurrentProgress = PartialResearchProgress[researchableType];
                PartialResearchProgress.Remove(researchableType);
            }

            OnResearchTaskAdded?.Invoke(null, new OnResearchTaskAddedArgs
            {
                ResearchableType = researchableType,
                ResearchCalculation = calculation
            });
        }
    }

    public void AddResearchableToDone(ResearchableType researchableType)
    {
        if (!CompletedResearch.Contains(researchableType)) CompletedResearch.Add(researchableType);
        
        var data = DataInstanceFactory.GetResearchableInstance(researchableType);
        if (data is IResearchArtifact || data is IUpgradableTechnology)
        {
            if (ResearchLevels.ContainsKey(researchableType))
            {
                ResearchLevels[researchableType]++;
            }
            else
            {
                ResearchLevels.Add(researchableType, 1);
            }
        }
        else
        {
            AvailableResearch.Remove(researchableType);
        }
    }

    public int GetTechnologyLevel(ResearchableType researchableType)
    {
        var data = DataInstanceFactory.GetResearchableInstance(researchableType);

        if (!(data is IResearchArtifact) && !(data is IUpgradableTechnology))
        {
            throw new ArgumentException("provided researchable is not an artifact or upgradable tech");
        }

        return ResearchLevels.ContainsKey(researchableType) ? ResearchLevels[researchableType] : 0;
    }

    public void InjectPartialResearchProgress(Dictionary<ResearchableType, float> persistencePartialResearchData)
    {
        foreach (var partialDataObject in persistencePartialResearchData)
        {
            PartialResearchProgress.Add(partialDataObject.Key, partialDataObject.Value);
        }
    }

    public List<ResearchableType> GetAvailableResearch()
    {
        InitAvailableResearch();
        return AvailableResearch;
    }

    private void InitAvailableResearch()
    {
        AvailableResearch.Clear();
        foreach (var researchableType in (ResearchableType[]) Enum.GetValues(typeof(ResearchableType)))
        {
            var researchableInstance = DataInstanceFactory.GetResearchableInstance(researchableType);
            
            if (CompletedResearch.Contains(researchableType) && !(researchableInstance is IUpgradableTechnology)) continue;

            if (researchableInstance is IUpgradableTechnology)
            {
                AvailableResearch.Add(researchableType);
            }
        }
    }

    private void InitKnownPlanetTechnology()
    {
        //loop through all Production Categories in enum
        foreach (var recipeCategory in (ProductionRecipeCategory[])Enum.GetValues(typeof(ProductionRecipeCategory)))
        {
            //create category out of enum member and add it to global recipe list
            var prodCategory = new ProductionCategory
            {
                RecipeCategory = recipeCategory,
                RecipeCategoryName = string.Join(" ", Regex.Split(recipeCategory.ToString(), "(?<!^)(?=[A-Z])")),
                KnownProductionRecipesInCategory = new List<IProductionRecipe>()
            };
            KnownPlanetProductionRecipes.Add(prodCategory);

            //for each category, loop thorough all of recipe types belonging to this category
            foreach (var recipeType in (ProductionRecipeType[])Enum.GetValues(typeof(ProductionRecipeType)))
            {
                //skip NONE recipe type
                if (recipeType == ProductionRecipeType.None) continue;

                //if recipe type is of wrong category skip it
                var recipeInstance = DataInstanceFactory.GetPlanetProductionRecipeInstance(recipeType);
                if (recipeInstance.ProductionRecipeCategory != recipeCategory) continue;

                //add recipe type to relevant category
                KnownPlanetProductionRecipes.Last().KnownProductionRecipesInCategory.Add(recipeInstance);
            }
        }
    }

    private void InitKnownPlanetBuildings()
    {
        foreach (var buildingType in (BuildingType[]) Enum.GetValues(typeof(BuildingType)))
        {
            KnownBuildingTypes.Add(buildingType);
        }
    }
}

public class ProductionCategory
{
    public ProductionRecipeCategory RecipeCategory;
    public string RecipeCategoryName;
    public List<IProductionRecipe> KnownProductionRecipesInCategory;
}