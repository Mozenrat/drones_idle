﻿using System;
using System.Collections.Generic;

public class MothershipEntity
{
    public class PlayerFleetChangedEventArgs : EventArgs
    {
        public readonly PlayerVesselFleet PlayerFleet;

        public PlayerFleetChangedEventArgs(PlayerVesselFleet playerFleet)
        {
            PlayerFleet = playerFleet;
        }
    }

    public EventHandler<PlayerFleetChangedEventArgs> OnPlayerFleetDataChanged;
    
    public float GlobalBandwidthLimit = 200f;
    public TechnologyController TechnologyController;
    public List<IShipyardRecipe> ShipyardVesselRecipes;
    public MothershipStorageManager MothershipStorageManager;
    public MothershipCalculationsManager MothershipCalculationsManager;

    public PlayerVesselFleet MothershipFleetStaging;

    public List<PlayerVesselFleet> ActivePlayerFleets;
}