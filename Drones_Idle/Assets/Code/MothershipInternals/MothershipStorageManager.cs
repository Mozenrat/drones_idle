using System;
using System.Collections.Generic;
using System.Linq;

public class MothershipStorageManager
{
    private readonly Dictionary<StorableType, float> StorageContents;
    
    public class OnMothershipStorageContentChangedEventArgs : EventArgs
    {
        public IStorable Storable { get; private set; }
        public float Quantity { get; private set; }

        public void ChangeEventArgsData(IStorable newStorable, float newQuantity)
        {
            Storable = newStorable;
            Quantity = newQuantity;
        }
    }

    public EventHandler<OnMothershipStorageContentChangedEventArgs> OnMothershipStorageContentChanged;

    private readonly OnMothershipStorageContentChangedEventArgs contentChangedEventArgs;

    public MothershipStorageManager()
    {
        StorageContents = new Dictionary<StorableType, float>();
        
        contentChangedEventArgs = new OnMothershipStorageContentChangedEventArgs();
    }
    
    public void AddUnitsToStorage(StorableType storableType, float unitAmount)
    {
        var storableInstance = DataInstanceFactory.GetStorableInstance(storableType);
        
        if (StorageContents.ContainsKey(storableType))
        {
            StorageContents[storableType] += unitAmount;
            contentChangedEventArgs.ChangeEventArgsData(storableInstance, GetUnitsStored(storableType));
            OnMothershipStorageContentChanged?.Invoke(null, contentChangedEventArgs);
        }
        else
        {
            StorageContents.Add(storableType, unitAmount);
            contentChangedEventArgs.ChangeEventArgsData(storableInstance, GetUnitsStored(storableType));
            OnMothershipStorageContentChanged?.Invoke(null, contentChangedEventArgs);
        }
    }
    
    public void SubtractUnitsFromStorage(StorableType storableType, float unitAmount)
    {
        if (!StorageContents.ContainsKey(storableType) || StorageContents[storableType] < unitAmount)
        {
            throw new InvalidOperationException("Attempted to remove nonexisting StorableType from MotherShip Storage!");
        }
        
        var storableInstance = DataInstanceFactory.GetStorableInstance(storableType);
        
        StorageContents[storableType] -= unitAmount;
        
        contentChangedEventArgs.ChangeEventArgsData(storableInstance, GetUnitsStored(storableType));
        OnMothershipStorageContentChanged?.Invoke(null, contentChangedEventArgs);
    }
    
    public float GetUnitsStored(StorableType storableType)
    {
        return !StorageContents.ContainsKey(storableType) ? 0f : StorageContents[storableType];
    }
    
    public StorableType[] GetStoredTypes()
    {
        return StorageContents.Keys.ToArray();
    }
}