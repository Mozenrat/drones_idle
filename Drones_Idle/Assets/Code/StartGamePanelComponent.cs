﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartGamePanelComponent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI StartGamePanelTitle;
    [SerializeField] private Button NewGameButton;
    [SerializeField] private Button LoadGameButton;

    public void InitDefaultPanel()
    {
        StartGamePanelTitle.SetText("Do you want to load a game or start new?");
    }

    private void Start()
    {
        LoadGameButton.onClick.AddListener(GlobalPersistenceController.PerformLoad_Static);
        NewGameButton.onClick.AddListener(GlobalPersistenceController.StartNewGame_Static);
    }
}