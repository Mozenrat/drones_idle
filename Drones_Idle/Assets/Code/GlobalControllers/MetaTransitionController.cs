﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Analytics;

public delegate void OnMetaPanelChanged();

public class MetaTransitionController : MonoBehaviour
{
    [SerializeField] private GameObject PlanetFeaturesPanel;
    [SerializeField] private GameObject MothershipFeaturesPanel;
    [SerializeField] private GameObject WarfareFeaturesPanel;
    [SerializeField] private TextMeshProUGUI ActivePanelTitleTextMesh;

    public Action OnTransitionToMothershipPanel;
    public Action OnTransitionToPlanetPanel;
    public Action OnTransitionToWarfarePanel;
    
    private List<GameObject> metaPanels;
    
    private void Awake()
    {
        metaPanels = new List<GameObject>
        {
            PlanetFeaturesPanel, 
            MothershipFeaturesPanel,
            WarfareFeaturesPanel
        };

        GoToPlanetScreen();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void GoToPlanetScreen()
    {
        OnTransitionToPlanetPanel?.Invoke();
        HideAllPanels(PlanetFeaturesPanel);
        metaPanels.Single(item => item == PlanetFeaturesPanel).SetActive(true);
    }

    public void GoToMothershipScreen()
    {
        OnTransitionToMothershipPanel?.Invoke();
        HideAllPanels(MothershipFeaturesPanel);
        metaPanels.Single(item => item == MothershipFeaturesPanel).SetActive(true);
    }

    public void GoToWarfareScreen()
    {
        OnTransitionToWarfarePanel?.Invoke();
        HideAllPanels(WarfareFeaturesPanel);
        metaPanels.Single(item => item == WarfareFeaturesPanel).SetActive(true);
    }
    
    private void HideAllPanels(GameObject except)
    {
        foreach (var item in metaPanels)
        {
            if (except != null && item == except) continue;

            if (item.activeSelf) item.SetActive(false);
        }
    }
}
