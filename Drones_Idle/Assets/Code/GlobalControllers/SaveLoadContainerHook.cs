﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SaveLoadContainerHook : MonoBehaviour
{
    [SerializeField] private Button SaveButton;
    [SerializeField] private Button LoadButton;
    
    private void Start()
    {
        SaveButton.onClick.RemoveAllListeners();
        LoadButton.onClick.RemoveAllListeners();
        SaveButton.onClick.AddListener(GlobalPersistenceController.PerformSave_Static);
        LoadButton.onClick.AddListener(GlobalPersistenceController.PerformLoad_Static);
    }
}