﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GlobalGameStateController : MonoBehaviour
{
    public static GlobalGameStateController Instance;

    [SerializeField] private StartGamePanelComponent StartGamePanelComponent;

    public List<PlanetContents> ActivePlanetList;
    public MothershipEntity MothershipEntity;
    public WarfareController WarfareController;
    public BonusController BonusController;

    public List<string> LogMessageList = new List<string>(DataValues.StatusLogSavedHistoryLength);
    public List<string> CombatMessageList = new List<string>(DataValues.CombatLogSavedHistoryLength);

    private float updatePeriodCurrent = 0f;

    public PlanetContents GetPlanetById(int Id)
    {
        return ActivePlanetList.Single(planet => planet.PlanetID == Id);
    }

    public bool IsEnoughBandwidthForExtraDrone(float droneBandwidth)
    {
        //TODO: dispatch some kind of event and show that all available bandwidth is used by drones
        return ActivePlanetList.Sum(item => item.PlanetDroneManager.TotalPlanetBandwidthUsage) + droneBandwidth <=
               MothershipEntity.GlobalBandwidthLimit;
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }

        DontDestroyOnLoad(gameObject);

        InitBlankGameState();
    }

    private void Start()
    {
        StartGamePanelComponent.InitDefaultPanel();
        
    }

    public void InitBlankGameState()
    {
        ActivePlanetList = new List<PlanetContents>();

        InitMothershipEntity();
        InitWarfare();

        AddPlanetToDomain(1);
    }

    public void InitWarfare()
    {
        WarfareController = new WarfareController
        {
            WarfareCalculationsManager = new WarfareCalculationsManager()
        };

        WarfareController.Init();
    }

    public void InitMothershipEntity()
    {
        MothershipEntity = new MothershipEntity
        {
            TechnologyController = new TechnologyController(),
            ShipyardVesselRecipes = new List<IShipyardRecipe>
                {DataInstanceFactory.GetCombatVesselInstance(VesselType.StarterFighter)},
            MothershipStorageManager = new MothershipStorageManager(),
            MothershipCalculationsManager = new MothershipCalculationsManager(),
            MothershipFleetStaging = new PlayerVesselFleet
            {
                FleetName = "Staging fleet",
                ActiveExpeditionZone = WarfareData.GetStagingZone()
            },
            ActivePlayerFleets = new List<PlayerVesselFleet>()
        };

        BonusController = new BonusController(MothershipEntity.TechnologyController);
    }

    private void Update()
    {
        updatePeriodCurrent += Time.deltaTime;
        if (updatePeriodCurrent >= DataValues.CalculationsUpdatePeriod)
        {
            MothershipEntity.MothershipCalculationsManager.UpdateCalculations(updatePeriodCurrent);
            WarfareController.WarfareCalculationsManager.UpdateCalculations(updatePeriodCurrent);

            foreach (var planet in ActivePlanetList)
            {
                planet.PlanetCalculationsManager.UpdateCalculations(DataValues.CalculationsUpdatePeriod);
            }

            updatePeriodCurrent = 0f;
        }
    }

    private void AddPlanetToDomain(int id)
    {
        var planet = new PlanetContents();
        planet.PlanetID = id;
        planet.PlanetName = "Default Planet";
        planet.PlanetTier = 1;
        planet.BiomassAbundance = 90;
        planet.OreBuckets.Add(new OreBucket
        {
            BucketOreType = OreType.Ferrium,
            OreTier = DataInstanceFactory.GetOreInstance(OreType.Ferrium).Tier,
            OrePlanetTotal = 456000,
            OrePlanetSurveyed = 1200
        });
        planet.OreBuckets.Add(new OreBucket
        {
            BucketOreType = OreType.Cuprite,
            OreTier = DataInstanceFactory.GetOreInstance(OreType.Cuprite).Tier,
            OrePlanetTotal = 221000,
            OrePlanetSurveyed = 670
        });
        planet.PlanetDroneManager = new PlanetDroneManager(BonusController, planet);
        planet.PlanetStorageManager = new PlanetStorageManager(); //TODO: remove hardcoded value
        planet.PlanetBuildingManager = new PlanetBuildingManager();
        planet.PlanetCalculationsManager = new PlanetCalculationsManager();
        planet.PlanetArchaeologyManager = new PlanetArchaeologyManager(planet);

        planet.PlanetStorageManager.AddUnitsToStorage(StorableType.WorkerDrone, 10);

        ActivePlanetList.Add(planet);
    }
}