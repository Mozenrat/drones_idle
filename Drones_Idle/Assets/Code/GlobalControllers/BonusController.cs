using System;
using System.Collections.Generic;
using UnityEngine;

public class BonusController
{
    private readonly TechnologyController techController;
    
    public BonusController(TechnologyController technologyController)
    {
        techController = technologyController;
    }

    public float GetDroneWorkSpeed()
    {
        var data = DataInstanceFactory.GetDroneInstanceByStorableType(StorableType.WorkerDrone);
        var techInfluence = techController.GetTechnologyLevel(ResearchableType.TechDroneWorkSpeed);
        var value = data.WorkSpeedBase * Mathf.Sqrt(techInfluence) + 1f;
        return value;
    }

    public float GetBuildingConstructionDuration(BuildingType buildingType, PlanetContents planet)
    {
        var data = DataInstanceFactory.GetConstructableInstance(buildingType);
        var existingCount = planet.PlanetBuildingManager.GetBuildingAmount(buildingType);
        
        int queueCount = 0;
        foreach (var calc in planet.PlanetCalculationsManager.GetConstructionCalculations)
        {
            if (calc.PeekConstructable.BuildingType == buildingType)
            {
                queueCount++;
            }
        }
        existingCount += queueCount;
        
        var existingCountCoeff = existingCount == 0 ? 1 : existingCount + 1;
        var value = data.BuildingConstructionDurationBase * Mathf.Pow(existingCountCoeff, data.BuildingNextCostCoefficient);
        return value;
    }

    public Dictionary<StorableType, float> GetBuildingConstructionCost(BuildingType buildingType, PlanetContents planet)
    {
        var data = DataInstanceFactory.GetConstructableInstance(buildingType);
        var existingCount = planet.PlanetBuildingManager.GetBuildingAmount(buildingType);
        
        int queueCount = 0;
        foreach (var calc in planet.PlanetCalculationsManager.GetConstructionCalculations)
        {
            if (calc.PeekConstructable.BuildingType == buildingType)
            {
                queueCount++;
            }
        }
        existingCount += queueCount;
        
        Dictionary<StorableType, float> value = new Dictionary<StorableType, float>();
        foreach (var costItem in data.BuildingBaseCost)
        {
            var existingCountCoeff = existingCount == 0 ? 1 : existingCount + 1 / 1.5f;
            var costComputed = Mathf.Pow(costItem.Value * (existingCountCoeff), data.BuildingNextCostCoefficient);
            value.Add(costItem.Key, costComputed);
        }
        return value;
    }

    public float GetShipyardRecipeSpeed(IShipyardRecipe shipyardRecipe)
    {
        var value = Mathf.Clamp(shipyardRecipe.ProductionDurationBase, 1f, float.MaxValue); //TODO: what influences ship building speed?
        return value;
    }

    public Dictionary<StorableType, float> GetShipyardRecipeProductionCost(IShipyardRecipe shipyardRecipe)
    {
        var value = new Dictionary<StorableType, float>(); //TODO: what influences ship building cost?
        foreach (var baseCostObj in shipyardRecipe.BaseProductionCost)
        {
            value.Add(baseCostObj.Key, baseCostObj.Value);
        }
        return value;
    }

    public float GetResearchDuration(ResearchableType researchableType)
    {
        var data = DataInstanceFactory.GetResearchableInstance(researchableType);
        var value = data.ResearchDurationBase * Mathf.Pow(1.09f, techController.GetTechnologyLevel(researchableType));
        return value;
    }

    public float GetArchaeologyMilestoneNextLevelCost(ArchaeologyMilestoneType milestoneType, int currentLevel)
    {
        var data = DataInstanceFactory.GetArchaeologyMilestoneInstance(milestoneType);
        var value = (data.MilestoneUnlockDepth / 2f + currentLevel) * data.MilestoneLevelUpCost;
        
        if (Math.Abs(value) < 0.001f)
            return data.MilestoneLevelUpCost;
        return value;
    }
}