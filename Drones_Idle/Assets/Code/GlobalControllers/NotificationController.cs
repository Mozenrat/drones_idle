﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NotificationController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI NotificationLabel;
    
    private static NotificationController instance;
    
    private CanvasGroup notificationGraphicCanvasGroup;
    
    private bool fadingOut;
    private float fadeOutCheckInterval;
    private float fadeOutCheckIntervalMax = DataValues.NotificationVisibilityDuration;
    
    public static void ShowNotification_Static(string text, bool redden)
    {
        instance.ShowNotification(text, redden);
    }
    
    private void Awake()
    {
        if (instance == null) instance = this;
        
        notificationGraphicCanvasGroup = GetComponent<CanvasGroup>();
        notificationGraphicCanvasGroup.alpha = 0f;
    }

    private void Update()
    {
        if (!fadingOut) return;
        
        fadeOutCheckInterval += Time.deltaTime;
        if (fadeOutCheckInterval >= fadeOutCheckIntervalMax)
        {
            FadeOutNotificationGraphic();
        }
    }

    private void ShowNotification(string notificationText, bool redden)
    {
        notificationGraphicCanvasGroup.alpha = 1f;
        fadeOutCheckInterval = 0;
        NotificationLabel.SetText(notificationText);
        fadingOut = true;
        NotificationLabel.color = redden ? DataValues.NotificationNegativeColor : DataValues.NotificationPositiveColor;
    }

    private void FadeOutNotificationGraphic()
    {
        var currentAlpha = notificationGraphicCanvasGroup.alpha;
        currentAlpha -= 1.2f * Time.deltaTime;
        if (currentAlpha <= 0)
        {
            fadeOutCheckInterval = 0;
            fadingOut = false;
        }
        notificationGraphicCanvasGroup.alpha = currentAlpha;
    }
}