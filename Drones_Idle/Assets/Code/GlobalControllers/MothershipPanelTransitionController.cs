﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

public class MothershipPanelTransitionController : MonoBehaviour
{
    [SerializeField] private CargoholdPanelComponent CargoholdPanelComponent;
    [SerializeField] private ResearchPanelComponent ResearchPanelComponent;
    [SerializeField] private ShipyardPanelComponent ShipyardPanelComponent;
    [SerializeField] private MolecularForgePanelComponent MolecularForgePanelComponent;
    
    private MetaTransitionController metaController;

    private List<MonoBehaviour> mothershipPanelsList;

    public void NavigateToCargoholdPanel()
    {
        HideAllPanels(CargoholdPanelComponent);
        mothershipPanelsList.Single(item => item.GetType() == typeof(CargoholdPanelComponent)).gameObject.SetActive(true);
    }
    
    public void NavigateToResearchPanel()
    {
        HideAllPanels(ResearchPanelComponent);
        mothershipPanelsList.Single(item => item.GetType() == typeof(ResearchPanelComponent)).gameObject.SetActive(true);
    }
    
    public void NavigateToShipyardPanel()
    {
        HideAllPanels(ShipyardPanelComponent);
        mothershipPanelsList.Single(item => item.GetType() == typeof(ShipyardPanelComponent)).gameObject.SetActive(true);
    }
    
    public void NavigateToMolecularForgePanel()
    {
        HideAllPanels(MolecularForgePanelComponent);
        mothershipPanelsList.Single(item => item.GetType() == typeof(MolecularForgePanelComponent)).gameObject.SetActive(true);
    }

    private void Awake()
    {
        metaController = FindObjectOfType<MetaTransitionController>();
        
        mothershipPanelsList = new List<MonoBehaviour>
        {
            CargoholdPanelComponent,
            ResearchPanelComponent,
            ShipyardPanelComponent,
            MolecularForgePanelComponent
        };
        
        HideAllPanels(null);

        metaController.OnTransitionToPlanetPanel += () => HideAllPanels(null);
        metaController.OnTransitionToWarfarePanel += () => HideAllPanels(null);
    }
    
    private void HideAllPanels(MonoBehaviour except)
    {
        foreach (var item in mothershipPanelsList)
        {
            if (except != null && item == except) continue;

            if (item.gameObject.activeSelf) item.gameObject.SetActive(false);
        }
    }
}
