﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlanetPanelTransitionController : MonoBehaviour
{
    [SerializeField] private MiningPanelComponent miningPanelComponent;
    [SerializeField] private StoragePanelComponent storagePanelComponent;
    [SerializeField] private ArchaeologyPanelComponent ArchaeologyPanelComponent;
    [SerializeField] private RefiningPanelComponent refiningPanelComponent;
    [SerializeField] private ConstructionPanelComponent constructionPanelComponent;
    [SerializeField] private ProductionPanelComponent productionPanelComponent;
    [SerializeField] private PlanetOverviewComponent planetOverviewComponent;
    [SerializeField] private LaunchpadPanelComponent launchpadPanelComponent;

    private MetaTransitionController metaController;

    private List<MonoBehaviour> planetPanelsList;

    public void NavigateToMiningPanel()
    {
        HideAllPanels(miningPanelComponent);
        planetPanelsList.Single(item => item.GetType() == typeof(MiningPanelComponent)).gameObject.SetActive(true);
    }

    public void NavigateToStoragePanel()
    {
        HideAllPanels(storagePanelComponent);
        planetPanelsList.Single(item => item.GetType() == typeof(StoragePanelComponent)).gameObject.SetActive(true);
    }

    public void NavigateToArchaeologyPanel()
    {
        HideAllPanels(ArchaeologyPanelComponent);
        planetPanelsList.Single(item => item.GetType() == typeof(ArchaeologyPanelComponent)).gameObject.SetActive(true);
    }

    public void NavigateToRefiningPanel()
    {
        HideAllPanels(refiningPanelComponent);
        planetPanelsList.Single(item => item.GetType() == typeof(RefiningPanelComponent)).gameObject.SetActive(true);
    }

    public void NavigateToConstructionPanel()
    {
        HideAllPanels(constructionPanelComponent);
        planetPanelsList.Single(item => item.GetType() == typeof(ConstructionPanelComponent)).gameObject.SetActive(true);
    }

    public void NavigateToProductionPanel()
    {
        HideAllPanels(productionPanelComponent);
        planetPanelsList.Single(item => item.GetType() == typeof(ProductionPanelComponent)).gameObject.SetActive(true);
    }
    
    public void NavigateToPlanetOverviewPanel()
    {
        HideAllPanels(planetOverviewComponent);
        planetPanelsList.Single(item => item.GetType() == typeof(PlanetOverviewComponent)).gameObject.SetActive(true);
    }
    
    public void NavigateToLaunchpadPanel()
    {
        HideAllPanels(launchpadPanelComponent);
        planetPanelsList.Single(item => item.GetType() == typeof(LaunchpadPanelComponent)).gameObject.SetActive(true);
    }

    private void Awake()
    {
        metaController = FindObjectOfType<MetaTransitionController>();

        planetPanelsList = new List<MonoBehaviour>
        {
            miningPanelComponent,
            storagePanelComponent,
            ArchaeologyPanelComponent,
            refiningPanelComponent,
            constructionPanelComponent,
            productionPanelComponent,
            planetOverviewComponent,
            launchpadPanelComponent
        };

        HideAllPanels(null);

        metaController.OnTransitionToMothershipPanel += () => HideAllPanels(null);
        metaController.OnTransitionToWarfarePanel += () => HideAllPanels(null);
    }

    private void HideAllPanels(MonoBehaviour except)
    {
        foreach (var item in planetPanelsList)
        {
            if (except != null && item == except) continue;

            if (item.gameObject.activeSelf) item.gameObject.SetActive(false);
        }
    }
}