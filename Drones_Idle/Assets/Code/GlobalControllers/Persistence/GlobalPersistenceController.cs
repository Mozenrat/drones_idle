using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using OdinSerializer;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalPersistenceController : MonoBehaviour
{
    public static bool IsNewGameStarting;
    public static Exception GameLoadingException;
    
    private static GlobalPersistenceController instance;
    
    private PersistenceAggregator dataAggregator;
    private PersistenceInjector dataInjector;
    private SerializableGameState saveInstance;
    private string savePath;

    private float autoSaveInterval = 0f;
    private float autoSaveIntervalPeriod = 10f;

    public static void StartNewGame_Static()
    {
        IsNewGameStarting = true;
        GlobalGameStateController.Instance.InitBlankGameState();
        SceneManager.LoadScene("MainScene");

        SceneManager.sceneLoaded += (scene, mode) =>
        {
            IsNewGameStarting = false;
        };
    }

    public static void PerformSave_Static()
    {
        instance.PerformGameSave();
        NotificationController.ShowNotification_Static(DataValues.GameSavedMessage, false);
    }

    public static void PerformLoad_Static()
    {
        GameLoadingException = null;
        
        try
        {
            instance.PerformGameLoad();
        }
        catch (Exception e)
        {
            GameLoadingException = e;
        }
        
        SceneManager.LoadScene("MainScene");
        
        SceneManager.sceneLoaded += (scene, mode) =>
        {
            IsNewGameStarting = false;
        };
    }

    private void PerformGameSave()
    {
        InitSaveInstance();
        dataAggregator.GatherStateData();

        var bytesArray = SerializationUtility.SerializeValue(saveInstance, DataFormat.Binary);
        var strValue = Convert.ToBase64String(bytesArray);

        using (var fs = new StreamWriter(savePath, false, Encoding.Unicode))
        {
            fs.Write(strValue);
        }
    }

    private void PerformGameLoad()
    {
        InitSaveInstance();
        string strValue;
        using (var fs = new StreamReader(savePath, Encoding.Unicode))
        {
            strValue = fs.ReadToEnd();
        }

        var bytes = Convert.FromBase64String(strValue);
        var saveDeserialized = SerializationUtility.DeserializeValue<SerializableGameState>(bytes, DataFormat.Binary);
        
        dataInjector.InjectDataAfterGameLoad(saveDeserialized);
    }

    private void Awake()
    {
        savePath = Application.persistentDataPath + "/save.txt";
        
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }
        
        DontDestroyOnLoad(gameObject);
    }

    private void InitSaveInstance()
    {
        saveInstance = new SerializableGameState();
        
        dataInjector = new PersistenceInjector();
        dataAggregator = new PersistenceAggregator(saveInstance);
    }

    private void Update()
    {
        autoSaveInterval += Time.deltaTime;
        if (autoSaveInterval >= autoSaveIntervalPeriod)
        {
            autoSaveInterval = 0f;
            //PerformAutoSave();
        }
    }

    private void PerformAutoSave()
    {
        PerformSave_Static();
    }
}