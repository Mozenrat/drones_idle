using System;
using System.Collections.Generic;

[Serializable]
public class SerializableGameState
{
    public List<PlanetActiveDronesPersistenceData> PlanetActiveDronesData;
    public List<PlanetOreBucketsPersistenceData> PlanetOreBucketsData;
    public List<PlanetStoragePersistenceData> PlanetStorageData;
    public List<PlanetBuildingsPersistenceData> PlanetBuildingsData;
    public List<PlanetArchaeologyPersistenceData> PlanetArchaeologyData;

    public List<MiningCalculationsPersistenceData> MiningCalculationsData;
    public List<RefiningCalculationPersistenceData> RefiningCalculationsData;
    public List<ProductionCalculationPersistenceData> ProductionCalculationsData;
    public List<PlanetRocketLaunchPersistenceData> RocketLaunchCalculationsData;
    public List<ConstructionCalculationPersistenceData> ConstructionCalculationData;

    public MothershipStagingFleetPersistenceData MothershipVesselFleetPersistenceData;
    public MothershipStoragePersistenceData MothershipStoragePersistenceData;
    public List<ShipyardCalculationPersistenceData> ShipyardCalculationsData;

    public List<ActivePlayerFleetsPersistenceData> ActivePlayerFleetsPersistenceData;

    public TechnologyPersistenceData TechnologyPersistenceData;

    public List<string> StatusLogsData;

    public SerializableGameState()
    {
        PlanetActiveDronesData = new List<PlanetActiveDronesPersistenceData>();
        PlanetOreBucketsData = new List<PlanetOreBucketsPersistenceData>();
        PlanetStorageData = new List<PlanetStoragePersistenceData>();
        PlanetBuildingsData = new List<PlanetBuildingsPersistenceData>();
        PlanetArchaeologyData = new List<PlanetArchaeologyPersistenceData>();
        MiningCalculationsData = new List<MiningCalculationsPersistenceData>();
        RefiningCalculationsData = new List<RefiningCalculationPersistenceData>();
        ProductionCalculationsData = new List<ProductionCalculationPersistenceData>();
        RocketLaunchCalculationsData = new List<PlanetRocketLaunchPersistenceData>();
        ConstructionCalculationData = new List<ConstructionCalculationPersistenceData>();
        MothershipVesselFleetPersistenceData = new MothershipStagingFleetPersistenceData();
        MothershipStoragePersistenceData = new MothershipStoragePersistenceData();
        ShipyardCalculationsData = new List<ShipyardCalculationPersistenceData>();
        ActivePlayerFleetsPersistenceData = new List<ActivePlayerFleetsPersistenceData>();
        TechnologyPersistenceData = new TechnologyPersistenceData();
        StatusLogsData = new List<string>();
    }
}

public class PlanetArchaeologyPersistenceData
{
    public class PlanetArchaeologyMilestoneDataObject
    {
        public ArchaeologyMilestoneType MilestoneType;
        public bool IsUnlocked;
        public int MilestoneLevel;
        public float CurrentMilestoneProgress;
    }

    public int PlanetId;
    public ArchaeologyMilestoneType PlanetActiveMilestone;
    public List<PlanetArchaeologyMilestoneDataObject> PlanetArchaeologyDataObjects;
}

public class PlanetRocketLaunchPersistenceData
{
    public class PlanetRocketLaunchDataObject
    {
        public Dictionary<StorableType, float> RocketCargo;
        public float CurrentFlightTime;
    }

    public int PlanetId;
    public List<PlanetRocketLaunchDataObject> PlanetRocketLaunchDataObjects;
}

public class PlanetActiveDronesPersistenceData
{
    public class PlanetDronesDataObject
    {
        public string DroneRoleHash;
        public int DroneAmount;
    }

    public int PlanetId;
    public List<PlanetDronesDataObject> PlanetDronesDataObjects;
}

public class PlanetOreBucketsPersistenceData
{
    public int PlanetId;
    public List<OreBucket> PlanetOreBucketObjects;
}

public class PlanetStoragePersistenceData
{
    public int PlanetId;
    public Dictionary<StorableType, float> PlanetStorageData;
}

public class PlanetBuildingsPersistenceData
{
    public int PlanetId;
    public Dictionary<BuildingType, int> PlanetBuildingsData;
}

public class MiningCalculationsPersistenceData
{
    public class MiningCalcDataObject
    {
        public int OreType;
        public float MiningCycleCurrent;
    }

    public int PlanetId;
    public List<MiningCalcDataObject> PlanetMiningDataObjects;
}

public class RefiningCalculationPersistenceData
{
    public class RefiningCalcDataObject
    {
        public int OreType;
        public float RefineCycleCurrent;
        public int RefiningProcess;
    }

    public int PlanetId;
    public List<RefiningCalcDataObject> PlanetRefiningDataObjects;
}

public class ProductionCalculationPersistenceData
{
    public class ProductionCalcDataObject
    {
        public int ProductionRecipe;
        public float ProductionCycleCurrent;
        public int UnitsToProduce;
        public bool IsActive;
    }

    public int PlanetId;
    public List<ProductionCalcDataObject> PlanetProductionDataObjects;
}

public class ConstructionCalculationPersistenceData
{
    public class ConstructionCalcDataObject
    {
        public int BuildingType;
        public float ConstructionDurationCurrent;
    }

    public int PlanetId;
    public List<ConstructionCalcDataObject> PlanetConstructionDataObjects;
}

public class ShipyardCalculationPersistenceData
{
    public int ShipyardRecipe;
    public float ShipyardCycleCurrent;
    public int UnitsToProduce;
    public bool IsActive;
}

public class MothershipStagingFleetPersistenceData
{
    public string FleetName;
    public Dictionary<VesselType, int> VesselFleetData;
}

public class MothershipStoragePersistenceData
{
    public Dictionary<StorableType, float> MothershipStorageData;
}

public class ActivePlayerFleetsPersistenceData
{
    public string FleetName;
    public int ActiveWarzoneTier;
    public Dictionary<VesselType, int> VesselFleetData;
}

public class TechnologyPersistenceData
{
    public Dictionary<ResearchableType, int> ActiveResearchData;
    public List<ResearchableType> CompletedResearchData;
    public Dictionary<ResearchableType, int> ResearchLevelsData;
    public Dictionary<ResearchableType, float> PartialResearchProgressData;
}