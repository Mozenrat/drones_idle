using System.Collections.Generic;
using System.Linq;

public class PersistenceInjector
{
    private readonly GlobalGameStateController globalGameState;

    public PersistenceInjector()
    {
        globalGameState = GlobalGameStateController.Instance;
    }
    
    public void InjectDataAfterGameLoad(SerializableGameState savedState)
    {
        ValidateSaveData(savedState);
        
        ResetMothership();
        ResetWarfare();
        ResetActivePlanets();

        InjectPlanetsOreBucketData(savedState);
        InjectPlanetsStorageData(savedState);
        InjectPlanetsBuildingsData(savedState);
        InjectPlanetsDronesData(savedState);
        InjectPlanetsArchaeologyData(savedState);
        
        InjectMiningCalcData(savedState);
        InjectRefiningCalcData(savedState);
        InjectProductionCalcData(savedState);
        InjectRocketLaunchCalcData(savedState);
        InjectConstructionCalcData(savedState);

        InjectMothershipStagingFleetData(savedState);
        InjectMothershipStorageData(savedState);

        InjectShipyardCalcData(savedState);

        InjectActivePlayerFleetsData(savedState);
        
        InjectTechnologyData(savedState);
        
        InjectStatusLogData(savedState);
    }

    private void ResetActivePlanets()
    {
        foreach (var planet in globalGameState.ActivePlanetList)
        {
            planet.PlanetCalculationsManager = new PlanetCalculationsManager();
            planet.PlanetDroneManager = new PlanetDroneManager(globalGameState.BonusController, planet);
            planet.PlanetStorageManager = new PlanetStorageManager();
            planet.PlanetBuildingManager = new PlanetBuildingManager();
            planet.PlanetArchaeologyManager = new PlanetArchaeologyManager(planet);
            planet.OreBuckets = new List<OreBucket>();
        }
    }

    private void ResetMothership()
    {
        globalGameState.InitMothershipEntity();
    }

    private void ResetWarfare()
    {
        globalGameState.InitWarfare();
    }

    private void InjectRocketLaunchCalcData(SerializableGameState savedState)
    {
        if (savedState.RocketLaunchCalculationsData == null) return;
        
        foreach (var calcPersistenceObject in savedState.RocketLaunchCalculationsData)
        {
            var planet = globalGameState.GetPlanetById(calcPersistenceObject.PlanetId);

            foreach (var calcDataObject in calcPersistenceObject.PlanetRocketLaunchDataObjects)
            {
                planet.PlanetCalculationsManager.InitRocketLaunchCalculation(calcDataObject.RocketCargo, planet, out var calcRef);
                calcRef.CurrentFlightDuration = calcDataObject.CurrentFlightTime;
            }
        }
    }

    private void InjectShipyardCalcData(SerializableGameState savedState)
    {
        if (savedState.ShipyardCalculationsData == null) return;
        
        foreach (var dataObject in savedState.ShipyardCalculationsData)
        {
            var recipeInstance = DataInstanceFactory.GetCombatVesselInstance((VesselType) dataObject.ShipyardRecipe);
            
            globalGameState.MothershipEntity.MothershipCalculationsManager.InitShipyardCalculation(
                recipeInstance,
                dataObject.UnitsToProduce,
                out var calcRef);

            calcRef.ShipyardTaskCycleCurrent = dataObject.ShipyardCycleCurrent;
            calcRef.IsActive = dataObject.IsActive;
        }
    }

    private void InjectMothershipStagingFleetData(SerializableGameState savedState)
    {
        if (savedState.MothershipVesselFleetPersistenceData?.VesselFleetData == null) return;
        
        var mothershipEntity = GlobalGameStateController.Instance.MothershipEntity;
        mothershipEntity.MothershipFleetStaging = new PlayerVesselFleet(
            savedState.MothershipVesselFleetPersistenceData.VesselFleetData,
            savedState.MothershipVesselFleetPersistenceData.FleetName);
    }

    private void InjectMothershipStorageData(SerializableGameState savedState)
    {
        if (savedState.MothershipStoragePersistenceData?.MothershipStorageData == null) return;
        
        var mothershipEntity = GlobalGameStateController.Instance.MothershipEntity;
        foreach (var storageData in savedState.MothershipStoragePersistenceData.MothershipStorageData)
        {
            mothershipEntity.MothershipStorageManager.AddUnitsToStorage(storageData.Key, storageData.Value);
        }
    }

    private void InjectActivePlayerFleetsData(SerializableGameState savedState)
    {
        if (savedState.ActivePlayerFleetsPersistenceData == null) return;

        var mothershipEntity = GlobalGameStateController.Instance.MothershipEntity;
        foreach (var fleetDataObject in savedState.ActivePlayerFleetsPersistenceData)
        {
            //Recreate player fleet object
            var fleet = new PlayerVesselFleet();
            fleet.FleetName = fleetDataObject.FleetName;
            fleet.ActiveExpeditionZone = WarfareData.GetZoneByTier(fleetDataObject.ActiveWarzoneTier);

            //add ships to fleet from savedState
            foreach (var vesselDataObject in fleetDataObject.VesselFleetData)
            {
                fleet.AddShipToFleet(vesselDataObject.Key, vesselDataObject.Value);
            }
            
            fleet.ResetFleetHp();
            //Init warfare calculation for this fleet if not in staging zone
            if (fleet.ActiveExpeditionZone.ZoneTier != 0)
            {
                GlobalGameStateController.Instance.WarfareController.WarfareCalculationsManager.InitNewWarfareCalculation(
                    zone: fleet.ActiveExpeditionZone,
                    playerFleet: fleet,
                    enemyFleet: fleet.ActiveExpeditionZone.GetRandomEnemyFleet());
            }
            
            mothershipEntity.ActivePlayerFleets.Add(fleet);
        }
    }

    private void InjectTechnologyData(SerializableGameState savedState)
    {
        if (savedState.TechnologyPersistenceData == null) return;

        var techController = GlobalGameStateController.Instance.MothershipEntity.TechnologyController;
        foreach (var researchLevelsData in savedState.TechnologyPersistenceData.ResearchLevelsData)
        {
            for (int i = 0; i < researchLevelsData.Value; i++)
            {
                techController.AddResearchableToDone(researchLevelsData.Key);
            }
        }

        foreach (var researchCompletedType in savedState.TechnologyPersistenceData.CompletedResearchData)
        {
            if (techController.CompletedResearch.Contains(researchCompletedType)) continue;
            techController.AddResearchableToDone(researchCompletedType);
        }

        techController.InjectPartialResearchProgress(savedState.TechnologyPersistenceData.PartialResearchProgressData);

        foreach (var activeResearchType in savedState.TechnologyPersistenceData.ActiveResearchData)
        {
            for (int i = 0; i < activeResearchType.Value; i++)
            {
                techController.AddToActiveResearch(activeResearchType.Key);
            }
        }
    }

    private void InjectPlanetsArchaeologyData(SerializableGameState savedState)
    {
        if (savedState.PlanetArchaeologyData == null) return;
        
        foreach (var archaeologyDataObject in savedState.PlanetArchaeologyData)
        {
            var planet = globalGameState.GetPlanetById(archaeologyDataObject.PlanetId);
            foreach (var milestoneData in archaeologyDataObject.PlanetArchaeologyDataObjects)
            {
                var milestone = planet.PlanetArchaeologyManager.GetMilestoneByType(milestoneData.MilestoneType);
                milestone.IsUnlocked = milestoneData.IsUnlocked;
                milestone.InjectData(milestoneData.MilestoneLevel, milestoneData.CurrentMilestoneProgress);
            }
            planet.PlanetArchaeologyManager.SetActiveMilestone(archaeologyDataObject.PlanetActiveMilestone);
        }
    }

    private void InjectPlanetsOreBucketData(SerializableGameState savedState)
    {
        foreach (var oreBucketDataObject in savedState.PlanetOreBucketsData)
        {
            var planet = globalGameState.GetPlanetById(oreBucketDataObject.PlanetId);
            foreach (var bucketData in oreBucketDataObject.PlanetOreBucketObjects)
            {
                planet.OreBuckets.Add(bucketData);
            }
        }
    }

    private void InjectPlanetsDronesData(SerializableGameState savedState)
    {
        foreach (var dronesPersistenceObject in savedState.PlanetActiveDronesData)
        {
            var planet = globalGameState.GetPlanetById(dronesPersistenceObject.PlanetId);
            var totalDrones = dronesPersistenceObject.PlanetDronesDataObjects.Sum(item => item.DroneAmount);
            planet.PlanetStorageManager.AddUnitsToStorage(StorableType.WorkerDrone, totalDrones);
            
            foreach (var droneDataObject in dronesPersistenceObject.PlanetDronesDataObjects)
            {
                planet.PlanetDroneManager.ParseDroneHashToEnums(droneDataObject.DroneRoleHash, out var droneRole, out var oreType, out var productionRecipeType);
                planet.PlanetDroneManager.AssignDronesToRole(droneRole, oreType, productionRecipeType, droneDataObject.DroneAmount);
            }
        }
    }

    private void InjectPlanetsStorageData(SerializableGameState savedState)
    {
        foreach (var planetStorageData in savedState.PlanetStorageData)
        {
            var planet = globalGameState.GetPlanetById(planetStorageData.PlanetId);
            foreach (var storable in planetStorageData.PlanetStorageData)
            {
                planet.PlanetStorageManager.AddUnitsToStorage(storable.Key, storable.Value);
            }
        }
    }
    
    private void InjectPlanetsBuildingsData(SerializableGameState savedState)
    {
        if (savedState.PlanetBuildingsData == null) return;
        
        foreach (var buildingsData in savedState.PlanetBuildingsData)
        {
            var planet = globalGameState.GetPlanetById(buildingsData.PlanetId);
            foreach (var buildingType in buildingsData.PlanetBuildingsData)
            {
                planet.PlanetBuildingManager.ConstructBuilding(buildingType.Key, buildingType.Value);
            }
        }
    }

    private void InjectMiningCalcData(SerializableGameState savedState)
    {
        if (savedState.MiningCalculationsData == null) return;
        
        foreach (var calculationData in savedState.MiningCalculationsData)
        {
            var planet = globalGameState.GetPlanetById(calculationData.PlanetId);

            foreach (var dataObject in calculationData.PlanetMiningDataObjects)
            {
                planet.PlanetCalculationsManager.InitMiningCalculation(planet, (OreType) dataObject.OreType, out var calcRef);
                calcRef.MiningCycleCurrent = dataObject.MiningCycleCurrent;
            }
        }
    }

    private void InjectRefiningCalcData(SerializableGameState savedState)
    {
        if (savedState.RefiningCalculationsData == null) return;
        
        foreach (var calculationData in savedState.RefiningCalculationsData)
        {
            var planet = globalGameState.GetPlanetById(calculationData.PlanetId);

            foreach (var dataObject in calculationData.PlanetRefiningDataObjects)
            {
                planet.PlanetCalculationsManager.InitRefiningCalculation(
                    planet,
                    (OreType) dataObject.OreType,
                    (RefiningProcess) dataObject.RefiningProcess,
                    out var refCalc);
                refCalc.RefiningCycleCurrent = dataObject.RefineCycleCurrent;
            }
        }
    }
    
    private void InjectProductionCalcData(SerializableGameState savedState)
    {
        if (savedState.ProductionCalculationsData == null) return;
        
        foreach (var calculationData in savedState.ProductionCalculationsData)
        {
            var planet = globalGameState.GetPlanetById(calculationData.PlanetId);

            foreach (var dataObject in calculationData.PlanetProductionDataObjects)
            {
                var recipe = DataInstanceFactory
                    .GetPlanetProductionRecipeInstance((ProductionRecipeType) dataObject.ProductionRecipe);
                
                planet.PlanetCalculationsManager.InitProductionCalculation(
                    recipe,
                    planet,
                    dataObject.UnitsToProduce,
                    out var calcRef
                    );
                
                calcRef.ProductionCycleCurrent = dataObject.ProductionCycleCurrent;
                calcRef.IsActive = dataObject.IsActive;
            }
        }
    }

    private void InjectConstructionCalcData(SerializableGameState savedState)
    {
        if (savedState.ConstructionCalculationData == null) return;

        foreach (var calculationData in savedState.ConstructionCalculationData)
        {
            var planet = globalGameState.GetPlanetById(calculationData.PlanetId);

            foreach (var dataObject in calculationData.PlanetConstructionDataObjects)
            {
                planet.PlanetCalculationsManager.InitConstructionCalculation(planet, (BuildingType) dataObject.BuildingType, out var calcRef);
                calcRef.ConstructionDurationCurrent = dataObject.ConstructionDurationCurrent;
            }
        }
    }

    private void InjectStatusLogData(SerializableGameState savedState)
    {
        if (savedState.StatusLogsData == null) return;
        
        StatusLogController.InjectStatusLogData_Static(savedState.StatusLogsData);
    }

    private void ValidateSaveData(SerializableGameState savedState)
    {
        //TODO:
    }
}