using System.Collections.Generic;

public class PersistenceAggregator
{
    private readonly SerializableGameState saveInstance;
    
    public PersistenceAggregator(SerializableGameState saveInstance)
    {
        this.saveInstance = saveInstance;
    }

    public void GatherStateData()
    {
        GatherPlanetsDroneData();
        GatherPlanetsOreBucketData();
        GatherPlanetsStorageData();
        GatherPlanetsBuildingsData();
        GatherPlanetArchaeologyData();
        
        GatherMiningCalculationsData();
        GatherRefiningCalculationData();
        GatherProductionCalculationData();
        GatherRocketLaunchCalculationData();
        GatherConstructionCalculationData();

        GatherMothershipStagingFleetData();
        GatherMothershipStorageData();
        GatherShipyardCalculationData();

        GatherActivePlayerFleetData();
        
        GatherTechnologyData();

        GatherStatusLogsData();
    }

    private void GatherPlanetArchaeologyData()
    {
        foreach (var planet in GlobalGameStateController.Instance.ActivePlanetList)
        {
            PlanetArchaeologyPersistenceData data = new PlanetArchaeologyPersistenceData
            {
                PlanetId = planet.PlanetID,
                PlanetActiveMilestone = planet.PlanetArchaeologyManager.ActiveArchaeologyObject.MilestoneType,
                PlanetArchaeologyDataObjects = planet.PlanetArchaeologyManager.AggregatePlanetArchaeologyData()
            };
            
            saveInstance.PlanetArchaeologyData.Add(data);
        }
    }

    private void GatherConstructionCalculationData()
    {
        foreach (var planet in GlobalGameStateController.Instance.ActivePlanetList)
        {
            ConstructionCalculationPersistenceData data = new ConstructionCalculationPersistenceData
            {
                PlanetId = planet.PlanetID,
                PlanetConstructionDataObjects = planet.PlanetCalculationsManager.AggregateConstructionCalculationData()
            };
            
            saveInstance.ConstructionCalculationData.Add(data);
        }
    }

    private void GatherRocketLaunchCalculationData()
    {
        foreach (var planet in GlobalGameStateController.Instance.ActivePlanetList)
        {
            PlanetRocketLaunchPersistenceData data = new PlanetRocketLaunchPersistenceData
            {
                PlanetId = planet.PlanetID,
                PlanetRocketLaunchDataObjects = planet.PlanetCalculationsManager.AggregateRocketLaunchCalculationData()
            }; 
            
            saveInstance.RocketLaunchCalculationsData.Add(data);
        }
    }

    private void GatherShipyardCalculationData()
    {
        saveInstance.ShipyardCalculationsData = GlobalGameStateController.Instance.MothershipEntity
            .MothershipCalculationsManager.AggregateShipyardCalculationData();
    }

    private void GatherMothershipStagingFleetData()
    {
        var existingVesselData = GlobalGameStateController.Instance.MothershipEntity.MothershipFleetStaging;
        saveInstance.MothershipVesselFleetPersistenceData.FleetName = existingVesselData.FleetName;
        saveInstance.MothershipVesselFleetPersistenceData.VesselFleetData = existingVesselData.FleetComposition;
    }

    private void GatherMothershipStorageData()
    {
        var mothershipStorage = new Dictionary<StorableType, float>();

        var mothershipStorageManager = GlobalGameStateController.Instance.MothershipEntity.MothershipStorageManager;
        foreach (var storableType in mothershipStorageManager.GetStoredTypes())
        {
            mothershipStorage.Add(storableType, mothershipStorageManager.GetUnitsStored(storableType));
        }
        
        saveInstance.MothershipStoragePersistenceData.MothershipStorageData = mothershipStorage;
    }

    private void GatherActivePlayerFleetData()
    {
        foreach (var activePlayerFleet in GlobalGameStateController.Instance.MothershipEntity.ActivePlayerFleets)
        {
            ActivePlayerFleetsPersistenceData fleetDataObject = new ActivePlayerFleetsPersistenceData
            {
                FleetName = activePlayerFleet.FleetName,
                ActiveWarzoneTier = activePlayerFleet.ActiveExpeditionZone.ZoneTier,
                VesselFleetData = activePlayerFleet.FleetComposition
            };
            
            saveInstance.ActivePlayerFleetsPersistenceData.Add(fleetDataObject);
        }
    }

    private void GatherTechnologyData()
    {
        saveInstance.TechnologyPersistenceData = GlobalGameStateController.Instance.MothershipEntity
            .TechnologyController.GetPersistenceData;
    }

    private void GatherStatusLogsData()
    {
        saveInstance.StatusLogsData = StatusLogController.GatherStatusLogData_Static();
    }

    private void GatherMiningCalculationsData()
    {
        foreach (var planet in GlobalGameStateController.Instance.ActivePlanetList)
        {
            MiningCalculationsPersistenceData data = new MiningCalculationsPersistenceData
            {
                PlanetId = planet.PlanetID,
                PlanetMiningDataObjects = planet.PlanetCalculationsManager.AggregateMiningCalculationData()
            };
            
            saveInstance.MiningCalculationsData.Add(data);
        }
    }
    
    private void GatherRefiningCalculationData()
    {
        foreach (var planet in GlobalGameStateController.Instance.ActivePlanetList)
        {
            RefiningCalculationPersistenceData data = new RefiningCalculationPersistenceData
            {
                PlanetId = planet.PlanetID,
                PlanetRefiningDataObjects = planet.PlanetCalculationsManager.AggregateRefiningCalculationData()
            };
            saveInstance.RefiningCalculationsData.Add(data);
        }
    }

    private void GatherProductionCalculationData()
    {
        foreach (var planet in GlobalGameStateController.Instance.ActivePlanetList)
        {
            ProductionCalculationPersistenceData data = new ProductionCalculationPersistenceData
            {
                PlanetId = planet.PlanetID,
                PlanetProductionDataObjects = planet.PlanetCalculationsManager.AggregateProductionCalculationData()
            };
            saveInstance.ProductionCalculationsData.Add(data);
        }
    }

    private void GatherPlanetsOreBucketData()
    {
        foreach (var planet in GlobalGameStateController.Instance.ActivePlanetList)
        {
            saveInstance.PlanetOreBucketsData.Add(new PlanetOreBucketsPersistenceData
            {
                PlanetId = planet.PlanetID,
                PlanetOreBucketObjects = planet.OreBuckets
            });
        }
    }

    private void GatherPlanetsStorageData()
    {
        foreach (var planet in GlobalGameStateController.Instance.ActivePlanetList)
        {
            var planetStorage = new Dictionary<StorableType, float>();
            foreach (var storable in planet.PlanetStorageManager.GetStoredTypes())
            {
                planetStorage.Add(storable, planet.PlanetStorageManager.GetUnitsStored(storable));
            }
            
            saveInstance.PlanetStorageData.Add(new PlanetStoragePersistenceData
            {
                PlanetId = planet.PlanetID,
                PlanetStorageData = planetStorage
            });
        }
    }

    private void GatherPlanetsDroneData()
    {
        foreach (var planet in GlobalGameStateController.Instance.ActivePlanetList)
        {
            var planetDroneObjects = new List<PlanetActiveDronesPersistenceData.PlanetDronesDataObject>();

            foreach (var droneInfo in planet.PlanetDroneManager.GetAllDronesData())
            {
                planetDroneObjects.Add(new PlanetActiveDronesPersistenceData.PlanetDronesDataObject
                {
                    DroneRoleHash = droneInfo.Key,
                    DroneAmount = droneInfo.Value
                });
            }

            saveInstance.PlanetActiveDronesData.Add(new PlanetActiveDronesPersistenceData
            {
                PlanetId = planet.PlanetID,
                PlanetDronesDataObjects = planetDroneObjects
            });
        }
    }

    private void GatherPlanetsBuildingsData()
    {
        foreach (var planet in GlobalGameStateController.Instance.ActivePlanetList)
        {
            saveInstance.PlanetBuildingsData.Add(new PlanetBuildingsPersistenceData
            {
                PlanetId = planet.PlanetID,
                PlanetBuildingsData = planet.PlanetBuildingManager.GetAllBuildings()
            });
        }
    }
}