﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StatusLogController : MonoBehaviour
{
    [SerializeField] private StatusLogPanelComponent StatusLogPanel;

    private static StatusLogController instance;

    public static void LogStatusMessage_Static(string msgString)
    {
        instance.LogNewStatusMessage($"<b>{DateTime.Now.ToShortTimeString()}</b> {msgString}");
    }

    public static void LogCombatMessage_Static(string msgString)
    {
        instance.LogNewCombatMessage($"<b>{DateTime.Now.ToShortTimeString()}</b> {msgString}");
    }

    public static List<string> GatherStatusLogData_Static()
    {
        return GlobalGameStateController.Instance.LogMessageList;
    }

    public static void InjectStatusLogData_Static(List<string> logData)
    {
        if (logData == null) return;
        GlobalGameStateController.Instance.LogMessageList = logData;
    }

    private void Awake()
    {
        if (instance == null) instance = this;
    }

    private void LogNewCombatMessage(string msg)
    {
        if (GlobalGameStateController.Instance.CombatMessageList.Count < DataValues.CombatLogSavedHistoryLength)
        {
            GlobalGameStateController.Instance.CombatMessageList.Add(msg);
        }
        else
        {
            GlobalGameStateController.Instance.CombatMessageList.RemoveRange(0, 25);
            GlobalGameStateController.Instance.CombatMessageList.Add(msg);
        }
        
        StatusLogPanel.LogNewCombatMessage(msg);
    }

    private void LogNewStatusMessage(string msg)
    {
        if (GlobalGameStateController.Instance.LogMessageList.Count < DataValues.StatusLogSavedHistoryLength)
        {
            GlobalGameStateController.Instance.LogMessageList.Add(msg);
        }
        else
        {
            GlobalGameStateController.Instance.LogMessageList.RemoveRange(0, 25);
            GlobalGameStateController.Instance.LogMessageList.Add(msg);
        }
        
        StatusLogPanel.LogNewStatusMessage(msg);
    }
}