﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WarfarePanelTransitionController : MonoBehaviour
{
    [SerializeField] private FleetsPanelComponent FleetsPanelComponent;
    [SerializeField] private ExpeditionsPanelComponent ExpeditionsPanelComponent;
    
    private MetaTransitionController metaController;

    private List<MonoBehaviour> warfarePanelsList;
    
    public void NavigateToFleetsPanel()
    {
        HideAllPanels(FleetsPanelComponent);
        warfarePanelsList.Single(item => item.GetType() == typeof(FleetsPanelComponent)).gameObject.SetActive(true);
    }
    
    public void NavigateToExpeditionsPanel()
    {
        if (GlobalGameStateController.Instance.MothershipEntity.ActivePlayerFleets.Count == 0) return; //TODO: remove hack, make proper solution
        
        HideAllPanels(ExpeditionsPanelComponent);
        warfarePanelsList.Single(item => item.GetType() == typeof(ExpeditionsPanelComponent)).gameObject.SetActive(true);
    }
    
    private void Awake()
    {
        metaController = FindObjectOfType<MetaTransitionController>();
        
        warfarePanelsList = new List<MonoBehaviour>
        {
            FleetsPanelComponent,
            ExpeditionsPanelComponent
        };
        
        HideAllPanels(null);

        metaController.OnTransitionToMothershipPanel += () => HideAllPanels(null);
        metaController.OnTransitionToPlanetPanel += () => HideAllPanels(null);
    }
    
    private void HideAllPanels(MonoBehaviour except)
    {
        foreach (var item in warfarePanelsList)
        {
            if (except != null && item == except) continue;

            if (item.gameObject.activeSelf) item.gameObject.SetActive(false);
        }
    }
}