﻿using System;
using OdinSerializer.Utilities;
using TMPro;
using UnityEngine;

public class TooltipController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI TooltipText;
    [SerializeField] private RectTransform CanvasRectTransform;
    [SerializeField] private RectTransform TooltipBackgroundTransform;
    [SerializeField] private RectTransform ParentTransform;
    [SerializeField] private Camera TooltipCamera;
    [SerializeField] private GameObject TooltipGraphic;

    private static TooltipController instance;

    private RectTransform graphicRectTransform;
    private Func<string> stringFunc;

    private TooltipProvider activeTooltipProvider;
    
    private float updateContentPeriodCurrent = 0f;
    private float updateContentPeriodMax = 0.25f;
    
    public static void ShowTooltip_Static(Func<string> text, TooltipProvider activeProvider)
    {
        instance.ShowTooltip(text, activeProvider);
    }

    public static void HideTooltip_Static()
    {
        instance.HideTooltip();
    }

    private void Awake()
    {
        if (instance == null) instance = this;
        TooltipText.autoSizeTextContainer = true;
        graphicRectTransform = TooltipGraphic.GetComponent<RectTransform>();
        TooltipGraphic.SetActive(false);
    }

    private void Update()
    {
        if (!TooltipGraphic.activeSelf) return;
        
        updateContentPeriodCurrent += Time.deltaTime;
        if (updateContentPeriodCurrent >= updateContentPeriodMax)
        {
            UpdateTooltipContent();
            updateContentPeriodCurrent = 0f;
        }
        CalculateTooltipPosition();
    }

    private void CalculateTooltipPosition()
    {
        if (activeTooltipProvider.SafeIsUnityNull() || !activeTooltipProvider.gameObject.activeInHierarchy)
        {
            HideTooltip();
            return;
        }
            
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            ParentTransform,
            Input.mousePosition,
            TooltipCamera,
            out var localPoint);

        TooltipGraphic.transform.localPosition = localPoint + new Vector2(20, -20 - TooltipText.preferredHeight);
        var anchoredPosition = graphicRectTransform.anchoredPosition;

        if (anchoredPosition.x + TooltipBackgroundTransform.rect.width > CanvasRectTransform.rect.width)
        {
            anchoredPosition.x = CanvasRectTransform.rect.width - TooltipBackgroundTransform.rect.width;
        }

        if (anchoredPosition.y + TooltipBackgroundTransform.rect.height < TooltipBackgroundTransform.rect.height)
        {
            anchoredPosition.y = 0;
        }

        graphicRectTransform.anchoredPosition = anchoredPosition;
    }

    private void UpdateTooltipContent()
    {
        TooltipText.SetText(stringFunc());
        Vector2 backgroundSize = new Vector2(TooltipText.preferredWidth, TooltipText.preferredHeight);
        TooltipBackgroundTransform.sizeDelta = backgroundSize;
    }
    
    private void ShowTooltip(Func<string> tooltipText, TooltipProvider provider)
    {
        stringFunc = tooltipText;
        activeTooltipProvider = provider;
        TooltipGraphic.SetActive(true);
        
        UpdateTooltipContent();
    }

    private void HideTooltip()
    {
        stringFunc = null;
        TooltipGraphic.SetActive(false);
    }
}