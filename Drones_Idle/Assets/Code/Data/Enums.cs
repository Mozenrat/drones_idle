﻿public enum DroneRole
{
    None,
    Mining,
    Refining,
    Production,
    Archaeology
}

public enum OreType
{
    None,
    Ferrium,
    Cuprite,
    BiomassOre = 10
}

public enum MineralType
{
    Iron,
    Copper,
    Silicates
}

public enum StorableType
{
    None,
    
    //Ores
    FerriumOre,
    CupriteOre,
    
    //Minerals
    IronMineral,
    CopperMineral,
    SilicatesMineral,
    
    //Production
    WorkerDrone,
    ConcreteSlab,
    Scaffolds,
    Cable,
    Electrolytes,
    RocketFuel,
    
    //Research Artifacts
    AlienShipWeaponT1,
    AlienShipShieldT1,
    AlienShipHullT1
}

public enum StorableCategory
{
    Drones,
    Ores,
    Minerals,
    Manufactured,
    ResearchMaterial
}

public enum RefiningProcess
{
    None,
    CrushProcessing,
    BiomassRefining = 10
}

public enum BuildingType
{
    BasicMiningFacility,
    Warehousing,
    Refinery
}

public enum ProductionRecipeType
{
    None,
    WorkerDrone,
    ConcreteSlab,
    Scaffolds,
    Cable,
    Electrolytes,
    RocketFuel
}

public enum ProductionRecipeCategory
{
    DroneModels,
    ConstructionComponents,
    TechComponents
}

public enum VesselType
{
    StarterFighter,
    AdvancedFighter,
    CybranInterceptor
}

public enum ResearchableType
{
    T1AlienWeapon,
    T1AlienShield,
    T1AlienHull,
    TechDroneWorkSpeed
}

public enum ArchaeologyMilestoneType
{
    InitialDig,
    FerriumVeins,
    CupriteVeins
}