﻿using System.Collections.Generic;

public interface IProductionRecipe : IStorable
{
    string RecipeName { get; }
    ProductionRecipeCategory ProductionRecipeCategory { get; }
    ProductionRecipeType ProductionRecipeType { get; }
    float ProductionDurationBase { get; }
    Dictionary<StorableType, float> BaseProductionCost { get; }
}

public class ElectrolytesRecipe : IProductionRecipe
{
    public string RecipeName { get; } = "Electrolytes";
    public string StorableName { get; } = "Electrolytes";
    public StorableType StorableType { get; } = StorableType.Electrolytes;
    public StorableCategory StorableCategory { get; } = StorableCategory.Manufactured;
    public string StorableInfoString { get; } = "Can be manufactured on planets from silicates and copper";
    public ProductionRecipeCategory ProductionRecipeCategory { get; } = ProductionRecipeCategory.TechComponents;
    public ProductionRecipeType ProductionRecipeType { get; } = ProductionRecipeType.Electrolytes;
    public float ProductionDurationBase { get; } = 114f;
    public float UnitVolume { get; } = 3f;
    public Dictionary<StorableType, float> BaseProductionCost { get; } = new Dictionary<StorableType, float>
    {
        [StorableType.SilicatesMineral] = 8f,
        [StorableType.CopperMineral] = 2f
    };
}

public class CableRecipe : IProductionRecipe
{
    public string RecipeName { get; } = "Cable";
    public string StorableName { get; } = "Cable";
    public StorableType StorableType { get; } = StorableType.Cable;
    public StorableCategory StorableCategory { get; } = StorableCategory.Manufactured;
    public string StorableInfoString { get; } = "Simple cable, manufactured on planets using copper";
    public ProductionRecipeCategory ProductionRecipeCategory { get; } = ProductionRecipeCategory.TechComponents;
    public ProductionRecipeType ProductionRecipeType { get; } = ProductionRecipeType.Cable;
    public float ProductionDurationBase { get; } = 14f;
    public float UnitVolume { get; } = 0.2f;
    public Dictionary<StorableType, float> BaseProductionCost { get; } = new Dictionary<StorableType, float>
    {
        [StorableType.CopperMineral] = 5f
    };
}

public class ScaffoldsRecipe : IProductionRecipe
{
    public string RecipeName { get; } = "Scaffolds";
    public string StorableName { get; } = "Scaffolds";
    public StorableType StorableType { get; } = StorableType.Scaffolds;
    public StorableCategory StorableCategory { get; } = StorableCategory.Manufactured;

    public string StorableInfoString { get; } =
        "Can be manufactured on planets from iron and cables, mostly required to construct big structures and buildings";
    public ProductionRecipeCategory ProductionRecipeCategory { get; } = ProductionRecipeCategory.ConstructionComponents;
    public ProductionRecipeType ProductionRecipeType { get; } = ProductionRecipeType.Scaffolds;
    public float ProductionDurationBase { get; } = 64f;
    public float UnitVolume { get; } = 9f;
    public Dictionary<StorableType, float> BaseProductionCost { get; } = new Dictionary<StorableType, float>
    {
        [StorableType.IronMineral] = 50f,
        [StorableType.Cable] = 2f
    };
}

public class ConcreteSlabRecipe : IProductionRecipe
{
    public string RecipeName { get; } = "Concrete Slab";
    public string StorableName { get; } = "Concrete Slab";
    public StorableType StorableType { get; } = StorableType.ConcreteSlab;
    public StorableCategory StorableCategory { get; } = StorableCategory.Manufactured;
    public string StorableInfoString { get; } = "Can be manufactured on planets from silicates and iron";
    public ProductionRecipeCategory ProductionRecipeCategory { get; } = ProductionRecipeCategory.ConstructionComponents;
    public ProductionRecipeType ProductionRecipeType { get; } = ProductionRecipeType.ConcreteSlab;
    public float ProductionDurationBase { get; } = 48f;
    public float UnitVolume { get; } = 4f;
    public Dictionary<StorableType, float> BaseProductionCost { get; } = new Dictionary<StorableType, float>
    {
        [StorableType.SilicatesMineral] = 30f,
        [StorableType.IronMineral] = 3f
    };
}

public class WorkerDroneRecipe : IProductionRecipe
{
    public string RecipeName { get; } = "Drone";
    public string StorableName { get; } = "Drone";
    public StorableType StorableType { get; } = StorableType.WorkerDrone;
    public StorableCategory StorableCategory { get; } = StorableCategory.Drones;
    public string StorableInfoString { get; } = "Your everyday workhorse, can be manufactured on planets from cables and iron";
    public ProductionRecipeCategory ProductionRecipeCategory { get; } = ProductionRecipeCategory.DroneModels;
    public ProductionRecipeType ProductionRecipeType { get; } = ProductionRecipeType.WorkerDrone;
    public float ProductionDurationBase { get; } = 140f;
    public float UnitVolume { get; } = 74f;
    public Dictionary<StorableType, float> BaseProductionCost { get; } = new Dictionary<StorableType, float>
    {
        [StorableType.Cable] = 16f,
        [StorableType.IronMineral] = 160f
    };
}

public class RocketFuelRecipe : IProductionRecipe
{
    public string RecipeName { get; } = "Rocket Fuel";
    public string StorableName { get; } = "Rocket Fuel";
    public StorableType StorableType { get; } = StorableType.RocketFuel;
    public StorableCategory StorableCategory { get; } = StorableCategory.Manufactured;
    public string StorableInfoString { get; } = "Rocket fuel is used to fuel rockets. mostly. can be manufactured on planets in a variety of ways";
    public ProductionRecipeCategory ProductionRecipeCategory { get; } = ProductionRecipeCategory.TechComponents;
    public ProductionRecipeType ProductionRecipeType { get; } = ProductionRecipeType.RocketFuel;
    public float ProductionDurationBase { get; } = 40f;
    public float UnitVolume { get; } = 2.3f;
    public Dictionary<StorableType, float> BaseProductionCost { get; } = new Dictionary<StorableType, float>
    {
        [StorableType.SilicatesMineral] = 2f,
        [StorableType.IronMineral] = 6f
    };
}