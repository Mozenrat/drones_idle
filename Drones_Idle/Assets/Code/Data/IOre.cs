﻿using System.Collections.Generic;

public interface IOre : IStorable
{
    string Name { get; }
    OreType OreType { get; }
    int Tier { get; }
    int Hardness { get; }
    int RefineBatch { get; }
    Dictionary<StorableType, float> MineralComposition { get; }
}

public class FerriumOre : IOre
{
    public string Name { get; } = "Ferrium Ore";
    public string StorableName { get; } = "Ferrium Ore";
    public OreType OreType { get; } = OreType.Ferrium;
    public StorableType StorableType { get; } = StorableType.FerriumOre;
    public StorableCategory StorableCategory { get; } = StorableCategory.Ores;
    public string StorableInfoString { get; } = "The most common of ores, can be mined on virtually any planet";
    public int Tier { get; } = 1;
    public int Hardness { get; } = 2;
    public int RefineBatch { get; } = 8;
    public float UnitVolume { get; } = 1f;

    public Dictionary<StorableType, float> MineralComposition { get; } = new Dictionary<StorableType, float>
    {
        [StorableType.IronMineral] = 3.2f,
        [StorableType.SilicatesMineral] = 0.8f
    };
}

public class CupriteOre : IOre
{
    public string Name { get; } = "Cuprite Ore";
    public string StorableName { get; } = "Cuprite Ore";
    public OreType OreType { get; } = OreType.Cuprite;
    public StorableType StorableType { get; } = StorableType.CupriteOre;
    public StorableCategory StorableCategory { get; } = StorableCategory.Ores;
    public string StorableInfoString { get; } = "Can be mined on tier 1 to 3 planets with relative ease";
    public int Tier { get; } = 1;
    public int Hardness { get; } = 3;
    public int RefineBatch { get; } = 10;
    public float UnitVolume { get; } = 1.5f;

    public Dictionary<StorableType, float> MineralComposition { get; } = new Dictionary<StorableType, float>
    {
        [StorableType.CopperMineral] = 0.8f,
        [StorableType.SilicatesMineral] = 0.15f
    };
}

public class BiomassOre : IOre
{
    public string Name { get; } = "Biomass";
    public string StorableName { get; } = "Biomass";
    public OreType OreType { get; } = OreType.BiomassOre;
    public StorableType StorableType { get; } = StorableType.None;
    public StorableCategory StorableCategory { get; } = StorableCategory.Ores;

    public string StorableInfoString { get; } =
        "Plants and wildlife that can be used and harvested on some of the planets, mostly used for rocket fuel";
    public int Tier { get; } = 1;
    public int Hardness { get; } = 1;
    public int RefineBatch { get; } = 0;
    public float UnitVolume { get; } = 0f;

    public Dictionary<StorableType, float> MineralComposition { get; } = new Dictionary<StorableType, float>
    {
        [StorableType.RocketFuel] = 2.5f
    };
}