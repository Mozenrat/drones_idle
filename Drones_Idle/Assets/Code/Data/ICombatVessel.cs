using System.Collections.Generic;

public interface ICombatVessel : IShipyardRecipe
{
    float BaseAttackPower { get; }
    float BaseShieldCapacity { get; }
    float BaseHullHitPoints { get; }
    float BaseShieldRegenValue { get; }
}

public interface IShipyardRecipe
{
    VesselType VesselType { get; }
    string VesselName { get; }
    float ProductionDurationBase { get; }
    Dictionary<StorableType, float> BaseProductionCost { get; }
}

public class CombatVesselT1 : ICombatVessel
{
    public string VesselName { get; } = "Starter Fighter";
    public float ProductionDurationBase { get; } = 30f;
    public VesselType VesselType { get; } = VesselType.StarterFighter;
    public float BaseAttackPower { get; } = DataValues.VesselStatT1AttackPower;
    public float BaseShieldCapacity { get; } = DataValues.VesselStatT1ShieldCapacity;
    public float BaseHullHitPoints { get; } = DataValues.VesselStatT1HullHitPoints;
    public float BaseShieldRegenValue { get; } = DataValues.VesselStatT1ShieldRegenValue;

    public Dictionary<StorableType, float> BaseProductionCost { get; } = new Dictionary<StorableType, float>
    {
        [StorableType.WorkerDrone] = 3f,
        [StorableType.IronMineral] = 20f,
        [StorableType.Cable] = 5f
    };
}

public class CombatVesselT2 : ICombatVessel
{
    public string VesselName { get; } = "Advanced Fighter";
    public float ProductionDurationBase { get; } = 60f;
    public VesselType VesselType { get; } = VesselType.AdvancedFighter;
    public float BaseAttackPower { get; } = DataValues.VesselStatT2AttackPower;
    public float BaseShieldCapacity { get; } = DataValues.VesselStatT2ShieldCapacity;
    public float BaseHullHitPoints { get; } = DataValues.VesselStatT2HullHitPoints;
    public float BaseShieldRegenValue { get; } = DataValues.VesselStatT2ShieldRegenValue;

    public Dictionary<StorableType, float> BaseProductionCost { get; } = new Dictionary<StorableType, float>
    {
        [StorableType.WorkerDrone] = 8f,
        [StorableType.IronMineral] = 96f,
        [StorableType.Cable] = 15f
    };
}

public class CybranInterceptor : ICombatVessel, IUnlockableByResearch
{
    public string VesselName { get; } = "Cybran Interceptor";
    public float ProductionDurationBase { get; } = 60f;
    public VesselType VesselType { get; } = VesselType.CybranInterceptor;
    public float BaseAttackPower { get; } = DataValues.VesselStatT2AttackPower;
    public float BaseShieldCapacity { get; } = DataValues.VesselStatT2ShieldCapacity;
    public float BaseHullHitPoints { get; } = DataValues.VesselStatT2HullHitPoints;
    public float BaseShieldRegenValue { get; } = DataValues.VesselStatT2ShieldRegenValue;

    public Dictionary<StorableType, float> BaseProductionCost { get; } = new Dictionary<StorableType, float>
    {
        [StorableType.WorkerDrone] = 8f,
        [StorableType.IronMineral] = 96f,
        [StorableType.Cable] = 15f
    };

    public List<ResearchableType> GetUnlockCriteria()
    {
        List<ResearchableType> requiredResearches = new List<ResearchableType>
        {
            ResearchableType.T1AlienWeapon,
            ResearchableType.T1AlienShield,
            ResearchableType.T1AlienHull
        };

        return requiredResearches;
    }
}