using System.Collections.Generic;

public interface IUnlockableByResearch
{
    List<ResearchableType> GetUnlockCriteria();
}