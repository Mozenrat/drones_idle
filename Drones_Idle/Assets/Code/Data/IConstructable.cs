﻿using System.Collections.Generic;

public interface IConstructable
{
    string BuildingName { get; }
    string BuildingInfo { get; }
    BuildingType BuildingType { get; }
    float BuildingConstructionDurationBase { get; }
    Dictionary<StorableType, float> BuildingBaseCost { get; }
    float BuildingNextCostCoefficient { get; }
}

public class Refinery : IConstructable
{
    public string BuildingName { get; } = "Refinery";
    public string BuildingInfo { get; } = "Refinery info placeholder";
    public BuildingType BuildingType { get; } = BuildingType.Refinery;
    public float BuildingConstructionDurationBase { get; } = 10f;
    public Dictionary<StorableType, float> BuildingBaseCost { get; } = new Dictionary<StorableType, float>
    {
        [StorableType.FerriumOre] = 80f,
        [StorableType.CupriteOre] = 34f
    };
    public float BuildingNextCostCoefficient { get; } = 1.2f;
}

public class Warehousing : IConstructable
{
    public string BuildingName { get; } = "Warehousing";
    public string BuildingInfo { get; } = "Warehousing info placeholder";
    public BuildingType BuildingType { get; } = BuildingType.Warehousing;
    public float BuildingConstructionDurationBase { get; } = 10f;
    public Dictionary<StorableType, float> BuildingBaseCost { get; } = new Dictionary<StorableType, float>
    {
        [StorableType.IronMineral] = 1420f,
        [StorableType.CopperMineral] = 830f,
        [StorableType.FerriumOre] = 230f
    };
    public float BuildingNextCostCoefficient { get; } = 1.32f;
}

public class BasicMiningFacility : IConstructable
{
    public string BuildingName { get; } = "Basic Mining Facility";
    public string BuildingInfo { get; } = "Basic facilities to help your drones mine better. Increase mining yield for T1 and T2 ores on this planet.";
    public BuildingType BuildingType { get; } = BuildingType.BasicMiningFacility;
    public float BuildingConstructionDurationBase { get; } = 24f;
    public Dictionary<StorableType, float> BuildingBaseCost { get; } = new Dictionary<StorableType, float>
    {
        [StorableType.FerriumOre] = 35f,
        [StorableType.CupriteOre] = 15f
    };
    public float BuildingNextCostCoefficient { get; } = 1.4f;
}