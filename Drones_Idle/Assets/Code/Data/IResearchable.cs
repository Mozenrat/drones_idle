using UnityEngine;

public interface IResearchable
{
    ResearchableType ResearchableType { get; }
    string ResearchDetailsText { get; }
    float ResearchDurationBase { get; }
    string ResearchableName { get; }
}

public interface IUpgradableTechnology : IResearchable
{
    void ApplyResearchEffect();
}

public interface IResearchArtifact : IStorable, IResearchable { }

public class AlienShipWeaponResearchArtifactT1 : IResearchArtifact
{
    public ResearchableType ResearchableType { get; } = ResearchableType.T1AlienWeapon;
    public string StorableName { get; } = "Tier 1 Alien Weaponry";
    public float UnitVolume { get; } = 142f;
    public StorableType StorableType { get; } = StorableType.AlienShipWeaponT1;
    public StorableCategory StorableCategory { get; } = StorableCategory.ResearchMaterial;
    public string StorableInfoString { get; } = "Alien ship weaponry that your drones scavenged after space battle";
    public string ResearchDetailsText { get; } = "<b>Can research</b> to improve our own weaponry and maybe even develop new combat vessels";
    public float ResearchDurationBase { get; } = 10f;
    public string ResearchableName => StorableName;
}

public class AlienShipShieldResearchArtifactT1 : IResearchArtifact
{
    public ResearchableType ResearchableType { get; } = ResearchableType.T1AlienShield;
    public string StorableName { get; } = "Tier 1 Alien Shield";
    public float UnitVolume { get; } = 142f;
    public StorableType StorableType { get; } = StorableType.AlienShipShieldT1;
    public StorableCategory StorableCategory { get; } = StorableCategory.ResearchMaterial;
    public string StorableInfoString { get; } = "Alien ship shield modules that your drones scavenged after space battle";
    public string ResearchDetailsText { get; } = "<b>Can research</b> to improve our own shields and maybe even develop new combat vessels";
    public float ResearchDurationBase { get; } = 10f;
    public string ResearchableName => StorableName;
}

public class AlienShipHullResearchArtifactT1 : IResearchArtifact
{
    public ResearchableType ResearchableType { get; } = ResearchableType.T1AlienHull;
    public string StorableName { get; } = "Tier 1 Alien Hull";
    public float UnitVolume { get; } = 142f;
    public StorableType StorableType { get; } = StorableType.AlienShipHullT1;
    public StorableCategory StorableCategory { get; } = StorableCategory.ResearchMaterial;
    public string StorableInfoString { get; } = "Alien ship hull parts that your drones scavenged after space battle";
    public string ResearchDetailsText { get; } = "<b>Can research</b> to improve our vessel's hulls and maybe even develop new combat vessels";
    public float ResearchDurationBase { get; } = 100f;
    public string ResearchableName => StorableName;
}

public class TechnologyDroneWorkSpeed : IUpgradableTechnology
{
    public ResearchableType ResearchableType { get; } = ResearchableType.TechDroneWorkSpeed;
    public string ResearchDetailsText { get; } = "Research to increase your drone work speed by 1";
    public float ResearchDurationBase { get; } = 50f;
    public string ResearchableName { get; } = "Tier 1 Drone Work Speed";
    public void ApplyResearchEffect()
    {
        Debug.Log("Effect not implemented");
    }
}