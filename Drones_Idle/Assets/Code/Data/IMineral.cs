﻿public interface IMineral : IStorable
{
    string Name { get; }
    MineralType MineralType { get; }
}

public class IronMineral : IMineral
{
    public string Name { get; } = "Iron";
    public string StorableName { get; } = "Iron";
    public MineralType MineralType { get; } = MineralType.Iron;
    public StorableType StorableType { get; } = StorableType.IronMineral;
    public StorableCategory StorableCategory { get; } = StorableCategory.Minerals;
    public string StorableInfoString { get; } = "Iron can be obtained on most planets by refining certain ores";
    public float UnitVolume { get; } = .1f;
}

public class CopperMineral : IMineral
{
    public string Name { get; } = "Copper";
    public string StorableName { get; } = "Copper";
    public MineralType MineralType { get; } = MineralType.Copper;
    public StorableType StorableType { get; } = StorableType.CopperMineral;
    public StorableCategory StorableCategory { get; } = StorableCategory.Minerals;
    public string StorableInfoString { get; } = "Copper can be obtained on most planets by refining certain ores";
    public float UnitVolume { get; } = .1f;
}

public class SilicatesMineral : IMineral
{
    public string Name { get; } = "Silicates";
    public string StorableName { get; } = "Silicates";
    public MineralType MineralType { get; } = MineralType.Silicates;
    public StorableType StorableType { get; } = StorableType.SilicatesMineral;
    public StorableCategory StorableCategory { get; } = StorableCategory.Minerals;
    public string StorableInfoString { get; } = "Silicates come as a by-product of refining ores and many other processes";
    public float UnitVolume { get; } = .1f;
}