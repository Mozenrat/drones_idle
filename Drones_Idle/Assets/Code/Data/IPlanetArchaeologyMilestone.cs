using UnityEngine;

public interface IPlanetArchaeologyMilestone
{
    string MilestoneDisplayName { get; }
    ArchaeologyMilestoneType MilestoneType { get; }
    int MilestoneUnlockDepth { get; }
    float MilestoneLevelUpCost { get; }
}

public class PlanetDefaultDig : IPlanetArchaeologyMilestone
{
    public string MilestoneDisplayName { get; } = "Develop planet WIP";
    public ArchaeologyMilestoneType MilestoneType { get; } = ArchaeologyMilestoneType.InitialDig;
    public int MilestoneUnlockDepth { get; } = 0;
    public float MilestoneLevelUpCost { get; } = 4;
}

public class PlanetFerriumVeinsMilestone : IPlanetArchaeologyMilestone
{
    public string MilestoneDisplayName { get; } = "Develop ferrium ore veins";
    public ArchaeologyMilestoneType MilestoneType { get; } = ArchaeologyMilestoneType.FerriumVeins;
    public int MilestoneUnlockDepth { get; } = 10;
    public float MilestoneLevelUpCost { get; } = 100;
}

public class PlanetCupriteVeinsMilestone : IPlanetArchaeologyMilestone
{
    public string MilestoneDisplayName { get; } = "Develop cuprite ore veins";
    public ArchaeologyMilestoneType MilestoneType { get; } = ArchaeologyMilestoneType.CupriteVeins;
    public int MilestoneUnlockDepth { get; } = 15;
    public float MilestoneLevelUpCost { get; } = 120;
}