﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class DataInstanceFactory
{
    private static readonly WorkerDrone Drone;
    private static readonly Dictionary<StorableType, IStorable> Storables;
    private static readonly Dictionary<MineralType, IMineral> Minerals;
    private static readonly Dictionary<OreType, IOre> Ores;
    private static readonly Dictionary<BuildingType, IConstructable> Buildings;
    private static readonly Dictionary<ProductionRecipeType, IProductionRecipe> ProductionRecipes;
    private static readonly Dictionary<VesselType, ICombatVessel> CombatVessels;
    private static readonly Dictionary<ResearchableType, IResearchable> Researchables;
    private static readonly Dictionary<ArchaeologyMilestoneType, IPlanetArchaeologyMilestone> ArchaeologyMilestones; 
    
    static DataInstanceFactory()
    {
        Drone = new WorkerDrone();
        Storables = new Dictionary<StorableType, IStorable>();
        Minerals = new Dictionary<MineralType, IMineral>();
        Ores = new Dictionary<OreType, IOre>();
        Buildings = new Dictionary<BuildingType, IConstructable>();
        ProductionRecipes = new Dictionary<ProductionRecipeType, IProductionRecipe>();
        CombatVessels = new Dictionary<VesselType, ICombatVessel>();
        Researchables = new Dictionary<ResearchableType, IResearchable>();
        ArchaeologyMilestones = new Dictionary<ArchaeologyMilestoneType, IPlanetArchaeologyMilestone>();
        
        foreach (StorableType storable in (StorableType[])Enum.GetValues(typeof(StorableType)))
        {
            if (storable == StorableType.None) continue;
            
            Storables.Add(storable, CreateStorableInstance(storable));
        }

        foreach (MineralType mineralType in (MineralType[])Enum.GetValues(typeof(MineralType)))
        {
            Minerals.Add(mineralType, CreateMineralInstance(mineralType));
        }
        
        foreach (OreType oreType in (OreType[])Enum.GetValues(typeof(OreType)))
        {
            if (oreType == OreType.None) continue;
            
            Ores.Add(oreType, CreateOreInstance(oreType));
        }

        foreach (BuildingType buildingType in (BuildingType[]) Enum.GetValues(typeof(BuildingType)))
        {
            Buildings.Add(buildingType, CreateConstructableInstance(buildingType));
        }

        foreach (ProductionRecipeType productionRecipeType in (ProductionRecipeType[]) Enum.GetValues(typeof(ProductionRecipeType)))
        {
            if (productionRecipeType == ProductionRecipeType.None) continue;
            
            ProductionRecipes.Add(productionRecipeType, CreatePlanetProductionRecipeInstance(productionRecipeType));
        }

        foreach (VesselType vesselType in (VesselType[]) Enum.GetValues(typeof(VesselType)))
        {
            CombatVessels.Add(vesselType, CreateCombatVesselInstance(vesselType));
        }

        foreach (ResearchableType researchableType in (ResearchableType[]) Enum.GetValues(typeof(ResearchableType)))
        {
            Researchables.Add(researchableType, CreateResearchableInstance(researchableType));
        }

        foreach (ArchaeologyMilestoneType milestoneType in (ArchaeologyMilestoneType[]) Enum.GetValues(typeof(ArchaeologyMilestoneType)))
        {
            ArchaeologyMilestones.Add(milestoneType, CreateArchaeologyMilestoneInstance(milestoneType));
        }
    }
    
    public static WorkerDrone GetDroneInstanceByStorableType(StorableType storableType)
    {
        return new WorkerDrone();
    }

    public static ICombatVessel GetCombatVesselInstance(VesselType vesselType)
    {
        return CombatVessels[vesselType];
    }

    public static IProductionRecipe GetPlanetProductionRecipeInstance(ProductionRecipeType productionRecipeType)
    {
        return ProductionRecipes[productionRecipeType];
    }

    public static IConstructable GetConstructableInstance(BuildingType buildingType)
    {
        return Buildings[buildingType];
    }

    public static IOre GetOreInstance(OreType oreType)
    {
        return Ores[oreType];
    }

    public static IMineral GetMineralInstance(MineralType mineralType)
    {
        return Minerals[mineralType];
    }

    public static List<IStorable> GetStorableInstancesByCategory(StorableCategory storableCategory)
    {
        return Storables.Values.ToList().FindAll(item => item.StorableCategory == storableCategory);
    }

    public static IStorable GetStorableInstance(StorableType storableType)
    {
        return Storables[storableType];
    }

    public static IResearchable GetResearchableInstance(ResearchableType researchableType)
    {
        return Researchables[researchableType];
    }

    public static IPlanetArchaeologyMilestone GetArchaeologyMilestoneInstance(ArchaeologyMilestoneType milestoneType)
    {
        return ArchaeologyMilestones[milestoneType];
    }

    #region Internals

    private static ICombatVessel CreateCombatVesselInstance(VesselType vesselType)
    {
        switch (vesselType)
        {
            case VesselType.StarterFighter:
                return new CombatVesselT1();
            case VesselType.AdvancedFighter:
                return new CombatVesselT2();
            case VesselType.CybranInterceptor:
                return new CybranInterceptor();
            default:
                throw new ArgumentOutOfRangeException(nameof(vesselType), vesselType, null);
        }
    }
    
    private static IProductionRecipe CreatePlanetProductionRecipeInstance(ProductionRecipeType productionRecipeType)
    {
        switch (productionRecipeType)
        {
            case ProductionRecipeType.None:
                Debug.Log("Attempted to get ProductionRecipe instance for ProductionRecipeType.None!");
                return null;
            case ProductionRecipeType.WorkerDrone:
                return new WorkerDroneRecipe();
            case ProductionRecipeType.ConcreteSlab:
                return new ConcreteSlabRecipe();
            case ProductionRecipeType.Scaffolds:
                return new ScaffoldsRecipe();
            case ProductionRecipeType.Cable:
                return new CableRecipe();
            case ProductionRecipeType.Electrolytes:
                return new ElectrolytesRecipe();
            case ProductionRecipeType.RocketFuel:
                return new RocketFuelRecipe();
            default:
                throw new ArgumentException("Invalid Argument for ProductionRecipe Instance Factory");
        }
    }
    
    private static IConstructable CreateConstructableInstance(BuildingType buildingType)
    {
        switch (buildingType)
        {
            case BuildingType.BasicMiningFacility:
                return new BasicMiningFacility();
            case BuildingType.Warehousing:
                return new Warehousing();
            case BuildingType.Refinery:
                return new Refinery();
            default:
                throw new ArgumentException("Invalid Argument for Constructable Instance Factory");
        }
    }
    
    private static IOre CreateOreInstance(OreType oreType)
    {
        switch (oreType)
        {
            case OreType.None:
                Debug.Log("Attempted to get ore instance for OreType.None!");
                return null;
            case OreType.Ferrium:
                return new FerriumOre();
            case OreType.Cuprite:
                return new CupriteOre();
            case OreType.BiomassOre:
                return new BiomassOre();
            default:
                throw new ArgumentException("Invalid Argument for Ore Instance Factory");
        }
    }

    private static IMineral CreateMineralInstance(MineralType mineralType)
    {
        switch (mineralType)
        {
            case MineralType.Iron:
                return new IronMineral();
            case MineralType.Copper:
                return new CopperMineral();
            case MineralType.Silicates:
                return new SilicatesMineral();
            default:
                throw new ArgumentException("Invalid Argument for Mineral Instance Factory");
        }
    }

    private static IStorable CreateStorableInstance(StorableType storableType)
    {
        switch (storableType)
        {
            case StorableType.None:
                Debug.Log("Attempted to get storable instance for StorableType.None!");
                return null;
            case StorableType.FerriumOre:
                return new FerriumOre();
            case StorableType.CupriteOre:
                return new CupriteOre();
            case StorableType.IronMineral:
                return new IronMineral();
            case StorableType.CopperMineral:
                return new CopperMineral();
            case StorableType.SilicatesMineral:
                return new SilicatesMineral();
            case StorableType.WorkerDrone:
                return new WorkerDroneRecipe();
            case StorableType.ConcreteSlab:
                return new ConcreteSlabRecipe();
            case StorableType.Scaffolds:
                return new ScaffoldsRecipe();
            case StorableType.Cable:
                return new CableRecipe();
            case StorableType.Electrolytes:
                return new ElectrolytesRecipe();
            case StorableType.RocketFuel:
                return new RocketFuelRecipe();
            case StorableType.AlienShipWeaponT1:
                return new AlienShipWeaponResearchArtifactT1();
            case StorableType.AlienShipShieldT1:
                return new AlienShipShieldResearchArtifactT1();
            case StorableType.AlienShipHullT1:
                return new AlienShipHullResearchArtifactT1();
            default:
                throw new ArgumentException("Invalid Argument for Storable Instance Factory");
        }
    }

    private static IResearchable CreateResearchableInstance(ResearchableType researchableType)
    {
        switch (researchableType)
        {
            case ResearchableType.T1AlienWeapon:
                return new AlienShipWeaponResearchArtifactT1();
            case ResearchableType.T1AlienShield:
                return new AlienShipShieldResearchArtifactT1();
            case ResearchableType.T1AlienHull:
                return new AlienShipHullResearchArtifactT1();
            case ResearchableType.TechDroneWorkSpeed:
                return new TechnologyDroneWorkSpeed();
            default:
                throw new ArgumentOutOfRangeException(nameof(researchableType), researchableType, null);
        }
    }

    private static IPlanetArchaeologyMilestone CreateArchaeologyMilestoneInstance(ArchaeologyMilestoneType archaeologyMilestoneType)
    {
        switch (archaeologyMilestoneType)
        {
            case ArchaeologyMilestoneType.FerriumVeins:
                return new PlanetFerriumVeinsMilestone();
            case ArchaeologyMilestoneType.CupriteVeins:
                return new PlanetCupriteVeinsMilestone();
            case ArchaeologyMilestoneType.InitialDig:
                return new PlanetDefaultDig();
            default:
                throw new ArgumentOutOfRangeException(nameof(archaeologyMilestoneType), archaeologyMilestoneType, null);
        }
    }

    #endregion
}