using UnityEngine;

public static class DataValues
{
    #region UiStateConsts

    public const int StatusLogLength = 30;
    public const int StatusLogSavedHistoryLength = 50;
    public const int CombatLogLength = 70;
    public const int CombatLogSavedHistoryLength = 90;

    public const float NotificationVisibilityDuration = 4f;
    
    public static readonly Color32 FleetListItemBackgroundColor = new Color32(83, 92, 104, 255);
    public static readonly Color32 FleetListItemHoverBackgroundColor = new Color32(53, 59, 72, 255);
    public static readonly Color32 FleetListItemStatusInExpeditionColor = new Color32(255, 192, 72, 255);
    public static readonly Color32 FleetListItemStatusInStagingAreaColor = new Color32(106, 176, 76, 255);
    
    public static readonly Color32 CargoholdListItemTileBackgroundColor = new Color32(113, 128, 147, 255);
    public static readonly Color32 CargoholdListItemTileHoverBackgroundColor = new Color32(83, 92, 104, 255);
    
    public static readonly Color32 RecipeListItemBackgroundRegularColor = new Color32(53,59,72,255);
    public static readonly Color32 RecipeListItemBackgroundHoverColor = new Color32(34, 47, 62, 255);
    public static readonly Color32 SelectedRecipeIngredientEnoughInStorageColor = new Color32(255, 177, 66, 255);
    public static readonly Color32 SelectedRecipeIngredientNotEnoughInStorageColor = new Color32(232, 65, 24, 255);
    
    public static readonly Color32 UiButtonRegularTextColor = new Color32(255, 177, 66, 255);
    public static readonly Color32 UiButtonHoverTextColor = new Color32(255, 150, 0, 255);
    
    public static readonly Color32 NotificationNegativeColor = new Color32(194,54,22,255);
    public static readonly Color32 NotificationPositiveColor = new Color32(122, 172, 221, 255);
    
    public const string ProductionPageUiSelectRecipeText = "Select production recipe";
    public const string ShipyardPageUiSelectRecipeText = "Select ship recipe";

    #endregion

    #region VesselStats

    public const int FleetStatMaxVesselTypesInFleet = 3;

    public const float VesselStatT1AttackPower = 1.4f;
    public const float VesselStatT1ShieldCapacity = 12.3f;
    public const float VesselStatT1HullHitPoints = 2.4f;
    public const float VesselStatT1ShieldRegenValue = 0.006f;
    
    public const float VesselStatT2AttackPower = 8f;
    public const float VesselStatT2ShieldCapacity = 29f;
    public const float VesselStatT2HullHitPoints = 5f;
    public const float VesselStatT2ShieldRegenValue = 0.012f;

    #endregion
    
    #region calculationConsts

    public const float CalculationsUpdatePeriod = 0.02f;

    public const float MiningCalculationOreTierDurationFactor = 4f;
    public const float MiningCalculationBaseFactor = 2f;

    public const float RefiningCalculationBiomassWorkSpeedFactor = 6f;
    public const float RefiningCalculationRefiningProcessDurationFactor = 8f;

    public const int RocketCargoBaysAmount = 4;
    public const float RocketCargoholdCapacity = 820f;
    public const float RocketFuelCostPerM3Cargo = 1.6f;
    public const float RocketFlightTimeToMothership = 120f;

    public const float ResearchQueueLengthBase = 36000f; // 10 hours

    //Product(1+0.9^n)

    #endregion

    #region notificationStrings

    public const string GameSavedMessage = "Game saved successfully";
    
    public const string NoFreeDronesAvailableText = "No free drones available";
    
    public const string NoProductionRecipeSelected = "Please select a production recipe";
    public const string NoValidProductionAmount = "Please specify production amount";
    public const string SelectedRecipeAlreadyInProduction = "Selected recipe is already in production";
    public const string ProductionTaskCreationSuccessful = "New production task successfully created";

    public const string LaunchPadRocketFull = "Rocket is full";
    public const string LaunchPadAllRocketCargoBaysFull = "All rocket cargo bays are occupied";
    public const string LaunchPadNoResourceToAddToRocket = "No transportable resource types found on the planet";
    public const string LaunchPadAllAvailableResourcesAlreadyAdded = "All transportable resource types available on the planet are already in cargo";

    public const string NoShipyardRecipeSelected = "Please select a shipyard recipe";
    public const string NoValidShipyardProductionAmount = "Please specify ship amount to produce";
    public const string ShipyardTaskCreationSuccessful = "New shipyard task successfully created";

    public const string ManageFleetsOnlyEditInStaging = "Can only edit fleets in staging area";
    public const string ManageFleetsNameCantBeEmpty = "Fleet name can't be empty";
    public const string ManageFleetsNotUniqueName = "Fleet with that name already exists";
    public const string ManageFleetsAtLeastOneShip = "At least one ship must remain in fleet";
    public const string ManageFleetsMaximumShipTypesExceeded = "Maximum allowed ship types in a single fleet is ";

    #endregion
}