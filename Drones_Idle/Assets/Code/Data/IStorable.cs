﻿public interface IStorable
{
    string StorableName { get; }
    float UnitVolume { get; }
    StorableType StorableType { get; }
    StorableCategory StorableCategory { get; }
    string StorableInfoString { get; }
}