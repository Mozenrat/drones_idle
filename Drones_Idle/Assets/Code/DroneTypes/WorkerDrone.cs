﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkerDrone
{
    public string Name { get; } = "Drone";
    public StorableType StorableType { get; } = StorableType.WorkerDrone;
    public float BandwidthRequired { get; } = 4f;
    public float WorkSpeedBase { get; } = .6f;
}