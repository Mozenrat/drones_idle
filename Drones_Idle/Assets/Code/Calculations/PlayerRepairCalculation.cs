using UnityEngine;

public class PlayerRepairCalculation
{
    public void Execute(float tickDelta)
    {
        foreach (var fleet in GlobalGameStateController.Instance.MothershipEntity.ActivePlayerFleets)
        {
            if (fleet.ActiveExpeditionZone?.ZoneTier == 0)
            {
                fleet.ProcessPlayerFleetRegenZone0();
                continue;
            }
            fleet.ProcessRegen();
        }
    }
}