using UnityEngine;

public class ResearchCalculation
{
    public string ResearchName => thisResearchable.ResearchableName;
    public float TimeToCycleCompletion => ResearchTaskCycleTotalProgress - ResearchTaskCycleCurrentProgress;
    public ResearchableType PeekType => thisResearchable.ResearchableType;
    public int ResearchCount { get; private set; }

    public float ResearchTaskCycleCurrentProgress;
    public float ResearchTaskCycleTotalProgress;

    private readonly BonusController bonusController;
    private readonly MothershipEntity mothershipEntity;
    private readonly IResearchable thisResearchable;

    public ResearchCalculation(IResearchable researchable)
    {
        bonusController = GlobalGameStateController.Instance.BonusController;
        mothershipEntity = GlobalGameStateController.Instance.MothershipEntity;
        thisResearchable = researchable;
        ResearchCount = 1;
        
        ResearchTaskCycleTotalProgress = bonusController.GetResearchDuration(thisResearchable.ResearchableType);
        ResearchTaskCycleCurrentProgress = 0f;
    }

    public void Execute(float tickDelta)
    {
        ResearchTaskCycleCurrentProgress += tickDelta;
        if (!(ResearchTaskCycleCurrentProgress >= ResearchTaskCycleTotalProgress)) return;
        
        mothershipEntity.TechnologyController.AddResearchableToDone(thisResearchable.ResearchableType);
        if (thisResearchable is IUpgradableTechnology upgradableTech)
        {
            upgradableTech.ApplyResearchEffect();
        }
        ResearchCount--;

        if (ResearchCount <= 0)
        {
            mothershipEntity.TechnologyController.RemoveFromActiveResearch(thisResearchable.ResearchableType, false);
        }

        ResearchTaskCycleTotalProgress = bonusController.GetResearchDuration(thisResearchable.ResearchableType);
        ResearchTaskCycleCurrentProgress = 0f;
    }

    public void ReduceResearchCount(int count)
    {
        ResearchCount -= count;
    }

    public void AddResearchCount(int count)
    {
        ResearchCount += count;
    }
}