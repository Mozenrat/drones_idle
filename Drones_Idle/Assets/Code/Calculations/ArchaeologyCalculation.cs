public class ArchaeologyCalculation
{
    private readonly PlanetContents thisPlanet;
    private BonusController bonusController;
    
    public ArchaeologyCalculation(PlanetContents planet)
    {
        thisPlanet = planet;
        bonusController = GlobalGameStateController.Instance.BonusController;
    }
    
    public void Execute(float tickDelta)
    {
        var dronesBusyCount = thisPlanet.PlanetDroneManager.GetAssignedDronesCount(
            DroneRole.Archaeology, OreType.None, ProductionRecipeType.None);
        
        if (dronesBusyCount < 1) return;

        var (_, droneEffect) = thisPlanet.PlanetDroneManager.GetDroneStats(DroneRole.Archaeology, OreType.None, ProductionRecipeType.None);
        //basically divide archaeology increment over deltaTick which is 50
        thisPlanet.PlanetArchaeologyManager.ActiveArchaeologyObject.AddProgress(droneEffect * tickDelta);
    }
}