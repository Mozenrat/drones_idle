using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Random = UnityEngine.Random;

public class WarfareCalculation
{
    private readonly WarfareController warfareController;
    private readonly MothershipStorageManager mothershipStorageManager;
    private readonly PlayerVesselFleet playerFleet;
    private readonly WarfareZone warfareZone;
    private readonly StringBuilder stringBuilder;
    
    private VesselFleet enemyFleet;

    private float CooldownBetweenFightsTotal = 4f;
    private float CooldownBetweenPlayerAttacksTotal = 1f;
    private float CooldownBetweenEnemyAttacksTotal = 1.2f;

    private float CooldownBetweenFightsCurrent = 0f;
    private float CooldownBetweenPlayerAttacksCurrent = 0f;
    private float CooldownBetweenEnemyAttacksCurrent = 0f;

    private readonly List<WarfareLootObject> droppedLoot;
    private WarfareController.OnWarfareNewFightStartedEventArgs newFightArgs;
    
    public WarfareCalculation(PlayerVesselFleet playerVesselFleet, VesselFleet enemyFleet)
    {
        warfareController = GlobalGameStateController.Instance.WarfareController;
        mothershipStorageManager = GlobalGameStateController.Instance.MothershipEntity.MothershipStorageManager;
        stringBuilder = new StringBuilder(200);
        
        playerFleet = playerVesselFleet;
        warfareZone = playerVesselFleet.ActiveExpeditionZone;
        this.enemyFleet = enemyFleet;

        droppedLoot = new List<WarfareLootObject>();
        newFightArgs = new WarfareController.OnWarfareNewFightStartedEventArgs(playerVesselFleet, enemyFleet);
    }

    public void Execute(float tickDelta)
    {
        CooldownBetweenFightsCurrent += tickDelta;
        if (!(CooldownBetweenFightsCurrent >= CooldownBetweenFightsTotal)) return;
        
        warfareController.OnWarfareNewFightStarted?.Invoke(null, newFightArgs);
        
        CooldownBetweenEnemyAttacksCurrent += tickDelta;
        CooldownBetweenPlayerAttacksCurrent += tickDelta;
        
        ProcessPlayerAttack();
        ProcessEnemyAttack();
        enemyFleet.ProcessRegen();
    }

    private void ProcessPlayerAttack()
    {
        if (CooldownBetweenPlayerAttacksCurrent >= CooldownBetweenPlayerAttacksTotal)
        {
            VesselAttack(playerFleet, enemyFleet);
            CooldownBetweenPlayerAttacksCurrent = 0f;

            if (!enemyFleet.IsAlive)
            {
                EnemyDied();
            }
        }
    }
    
    private void ProcessEnemyAttack()
    {
        if (CooldownBetweenEnemyAttacksCurrent >= CooldownBetweenEnemyAttacksTotal)
        {
            VesselAttack(enemyFleet, playerFleet);
            CooldownBetweenEnemyAttacksCurrent = 0f;

            if (!playerFleet.IsAlive)
            {
                PlayerDied();
            }
        }
    }

    private void VesselAttack(VesselFleet damageDealer, VesselFleet damageTaker)
    {
        damageTaker.TakeDamage(damageDealer.FleetAttackPower);
    }

    private void EnemyDied()
    {
        float lootRoll = Random.Range(0f, 100f);
        var lootProbabilitiesThisZone = warfareZone.Loot.Keys;
        
        droppedLoot.Clear();
        foreach (var probability in lootProbabilitiesThisZone)
        {
            if (probability >= lootRoll)
            {
                WarfareLootObject lootObject = warfareZone.Loot[probability];
                mothershipStorageManager.AddUnitsToStorage(lootObject.StorableType, lootObject.Quantity);
                droppedLoot.Add(lootObject);
            }
        }

        if (droppedLoot.Count > 0)
        {
            LogLootMessage(droppedLoot.Count);
        }
        
        CooldownBetweenFightsCurrent = 0f;
        CooldownBetweenEnemyAttacksCurrent = 0f;
        enemyFleet.ResetFleetHp();
        
        enemyFleet = warfareZone.GetRandomEnemyFleet();
        newFightArgs = new WarfareController.OnWarfareNewFightStartedEventArgs(playerFleet, enemyFleet);
        warfareController.OnWarfareFleetDied?.Invoke(null,
            new WarfareController.OnWarfareFleetDiedEventArgs(playerFleet));
    }

    private void PlayerDied()
    {
        enemyFleet.ResetFleetHp();
        StatusLogController.LogCombatMessage_Static($"Your fleet {playerFleet.FleetName} has been defeated at {warfareZone.ZoneName}");
        warfareController.ChangeActiveWarfareZoneForPlayerFleet(WarfareData.GetStagingZone(), playerFleet);
        warfareController.OnWarfareFleetDied?.Invoke(null,
            new WarfareController.OnWarfareFleetDiedEventArgs(playerFleet));
    }

    private void LogLootMessage(int amountOfObjects)
    {
        stringBuilder.Clear();
        
        if (amountOfObjects == 1)
        {
            var lootObject = droppedLoot.Single();

            stringBuilder.Append("Your fleet ").Append(playerFleet.FleetName).Append(" looted: ").Append(lootObject.Quantity);
            string singleOrPlural = (lootObject.Quantity > 1) ? " units of " : " unit of ";
            stringBuilder.Append(singleOrPlural);
            stringBuilder.Append(lootObject.StorableName);
            StatusLogController.LogCombatMessage_Static(stringBuilder.ToString());
            return;
        }

        stringBuilder.Append("Your fleet ").Append(playerFleet.FleetName).Append(" looted: ");
        for (var i = 0; i < droppedLoot.Count; i++)
        {
            var lootObject = droppedLoot[i];
            
            stringBuilder.Append(lootObject.Quantity);
            string singleOrPlural = (lootObject.Quantity > 1) ? " units of " : " unit of ";
            stringBuilder.Append(singleOrPlural);
            stringBuilder.Append(lootObject.StorableName);
            
            if (i < droppedLoot.Count - 1)
            {
                stringBuilder.Append(", ");
            }
        }

        StatusLogController.LogCombatMessage_Static(stringBuilder.ToString());
    }
}