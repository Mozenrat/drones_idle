﻿using System.Collections.Generic;
using System.Linq;
using Code.Utility;
using UnityEngine;

public class RefiningCalculation
{
    public RefiningCalculationPersistenceData.RefiningCalcDataObject GetPersistenceCalculationData =>
        new RefiningCalculationPersistenceData.RefiningCalcDataObject
        {
            OreType = (int) OreData.OreType,
            RefineCycleCurrent = RefiningCycleCurrent,
            RefiningProcess = (int) refiningProcess
        };

    public bool IsActive => planet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Refining, OreData.OreType, ProductionRecipeType.None) >= 1;

    public bool IsEnoughResourcesInStorage => RefineBatchWithBusyDrones <= planet.PlanetStorageManager.GetUnitsStored(OreData.StorableType);
    public int RefineBatchWithBusyDrones
    {
        get
        {
            var (dronesAmount, _) = GetRefiningDronesStats();
            return OreData.RefineBatch * (int) dronesAmount;
        }
    }

    public Dictionary<StorableType, float> MineralBatchToGet
    {
        get
        {
            var batch = new Dictionary<StorableType, float>();
            var totalMineralsRefined = TotalMineralsToBeRefined();
            var totalMineralsInOre = OreData.MineralComposition.Values.Sum();
            foreach (var item in OreData.MineralComposition)
            {
                float mineralAmount = ((item.Value / totalMineralsInOre) * totalMineralsRefined).ToTwoDigitFloat();
                batch.Add(item.Key, mineralAmount);
            }

            return batch;
        }
    }

    public readonly int CalcHash;
    
    public float RefiningCycleCurrent;
    public float RefiningCycleDuration;

    private readonly PlanetContents planet;
    private readonly IOre OreData;
    private readonly RefiningProcess refiningProcess;

    public RefiningCalculation(int calcHash, PlanetContents planet, OreType oreType, RefiningProcess refiningProcess)
    {
        CalcHash = calcHash;
        OreData = DataInstanceFactory.GetOreInstance(oreType);
        this.planet = planet;
        this.refiningProcess = refiningProcess;
        RefiningCycleDuration = EvaluateTotalCycleDuration();
    }

    public void Execute(float tickDelta)
    {
        if (!IsRefineCycleDone(tickDelta)) return;

        if (OreData.OreType == OreType.BiomassOre)
            DoRefineBiomass();
        else
            DoRefineOre();
    }

    private void DoRefineOre()
    {
        float totalMineralsRefined = TotalMineralsToBeRefined();

        var (dronesAmount, _) = GetRefiningDronesStats();
        var totalMineralsInOre = OreData.MineralComposition.Values.Sum();
        planet.PlanetStorageManager.SubtractUnitsFromStorage(OreData.StorableType, OreData.RefineBatch * dronesAmount);
        foreach (var item in OreData.MineralComposition)
        {
            planet.PlanetStorageManager.AddUnitsToStorage(item.Key, (item.Value / totalMineralsInOre) * totalMineralsRefined);
        }
    }

    private float TotalMineralsToBeRefined()
    {
        var (dronesAmount, dronesWorkSpeed) = GetRefiningDronesStats();
        var speedFactor = dronesAmount + Mathf.Pow(.9f, dronesWorkSpeed);
        var oreToughness = OreData.Hardness * OreData.Tier;
        var totalMineralsRefined = Mathf.Round(speedFactor * Mathf.Sqrt(dronesAmount) / oreToughness) * (int) refiningProcess;
        return totalMineralsRefined;
    }

    private void DoRefineBiomass()
    {
        var (_, dronesWorkSpeed) = GetRefiningDronesStats();
        var speedFactor = dronesWorkSpeed * DataValues.RefiningCalculationBiomassWorkSpeedFactor;
        var planetFactored = speedFactor * planet.BiomassAbundance / 100;
        
        foreach (var item in OreData.MineralComposition)
        {
            planet.PlanetStorageManager.AddUnitsToStorage(item.Key, item.Value * planetFactored);
        }
    }

    private (float, float) GetRefiningDronesStats()
    {
        return planet.PlanetDroneManager.GetDroneStats(DroneRole.Refining, OreData.OreType, ProductionRecipeType.None);
    }

    private float EvaluateTotalCycleDuration()
    {
        var (_, workSpeed) = GetRefiningDronesStats();
        var defaultDuration = (int) refiningProcess * DataValues.RefiningCalculationRefiningProcessDurationFactor;

        if (workSpeed <= 1f)
        {
            return defaultDuration;
        }

        return Mathf.Clamp(defaultDuration - Mathf.Log(workSpeed), DataValues.CalculationsUpdatePeriod, 3600f);
    }

    private bool IsRefineCycleDone(float tickDelta)
    {
        if (!IsActive) return false;
        
        if (!IsEnoughResourcesInStorage)
        {
            RefiningCycleCurrent = 0f;
            return false;
        }

        RefiningCycleDuration = EvaluateTotalCycleDuration();
        RefiningCycleCurrent += tickDelta;
        
        if (RefiningCycleCurrent >= RefiningCycleDuration)
        {
            RefiningCycleCurrent = 0f;
            if (OreData.RefineBatch <= planet.PlanetStorageManager.GetUnitsStored(OreData.StorableType))
            {
                return true;
            }
        }

        return false;
    }
}