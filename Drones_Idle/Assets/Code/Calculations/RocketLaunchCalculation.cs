using System;
using System.Collections.Generic;
using Code.Utility;

public class RocketLaunchCalculation
{
    public PlanetRocketLaunchPersistenceData.PlanetRocketLaunchDataObject GetPersistenceCalculationData =>
        new PlanetRocketLaunchPersistenceData.PlanetRocketLaunchDataObject
        {
            RocketCargo = rocketCargoContents,
            CurrentFlightTime = CurrentFlightDuration
        };

    public Dictionary<StorableType, float> GetRocketCargoholdContents => rocketCargoContents;
    public string GetOriginPlanetName => originPlanet.PlanetName;
    public float GetFlightTimeLeft => (TotalFlightDuration - CurrentFlightDuration).ToTwoDigitFloat();
    
    public float CurrentFlightDuration;
    
    private readonly Dictionary<StorableType, float> rocketCargoContents;
    private readonly PlanetContents originPlanet;
    private readonly MothershipStorageManager mothershipStorage;

    private float TotalFlightDuration = DataValues.RocketFlightTimeToMothership;
    
    public RocketLaunchCalculation(Dictionary<StorableType, float> cargoContents, PlanetContents planet)
    {
        rocketCargoContents = cargoContents;
        originPlanet = planet;
        mothershipStorage = GlobalGameStateController.Instance.MothershipEntity.MothershipStorageManager;
    }

    public void AbortRocketLaunch()
    {
        foreach (var cargoItem in rocketCargoContents)
        {
            originPlanet.PlanetStorageManager.AddUnitsToStorage(cargoItem.Key, cargoItem.Value * .8f);
        }
        DisposeCalculation();
    }

    public void Execute(float tickDelta)
    {
        CurrentFlightDuration += tickDelta;
        if (CurrentFlightDuration >= TotalFlightDuration)
        {
            foreach (var cargoItem in rocketCargoContents)
            {
                mothershipStorage.AddUnitsToStorage(cargoItem.Key, cargoItem.Value);
            }
            
            StatusLogController.LogStatusMessage_Static($"Rocket from {originPlanet.PlanetName} successfully reached its destination");
            DisposeCalculation();
        }
    }

    private void DisposeCalculation()
    {
        originPlanet.PlanetCalculationsManager.DisposeRocketLaunchCalculation(this);
    }
}