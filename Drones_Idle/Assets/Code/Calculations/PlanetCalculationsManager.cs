﻿using System;
using System.Collections.Generic;
using System.Linq;
using Code.Utility;

public class PlanetCalculationsManager
{
    public class OnConstructionQueueChangedEventArgs : EventArgs
    {
        public ConstructionCalculation CalcRef;
        public bool IsCalculationBeingAdded;
    }

    public class OnRocketLaunchEventArgs : EventArgs
    {
        public RocketLaunchCalculation CalcRef;
        public bool IsCalculationBeingAdded;
    }

    public class OnProductionChangeEventArgs : EventArgs
    {
        public ProductionCalculation CalcRef;
        public bool IsCalculationBeingAdded;
    }

    public event EventHandler<OnConstructionQueueChangedEventArgs> OnConstructionQueueChanged;
    public event EventHandler<OnRocketLaunchEventArgs> OnRocketLaunchQuantityChanged;
    public event EventHandler<OnProductionChangeEventArgs> OnProductionTasksChanged;

    public int ProductionCalculationsCountOnPlanet => ProductionCalculations.Count;
    public List<ConstructionCalculation> GetConstructionCalculations => ConstructionCalculations;
    public List<ProductionCalculation> GetProductionCalculations => ProductionCalculations;

    private ArchaeologyCalculation ArchaeologyCalculation;
    private readonly List<RefiningCalculation> RefiningCalculations = new List<RefiningCalculation>();
    private readonly List<ProductionCalculation> ProductionCalculations = new List<ProductionCalculation>();
    private readonly List<MiningCalculation> MiningCalculations = new List<MiningCalculation>();
    private readonly List<RocketLaunchCalculation> RocketLaunchCalculations = new List<RocketLaunchCalculation>();
    private readonly List<ConstructionCalculation> ConstructionCalculations = new List<ConstructionCalculation>();

    public List<MiningCalculationsPersistenceData.MiningCalcDataObject> AggregateMiningCalculationData()
    {
        var calcDataList = new List<MiningCalculationsPersistenceData.MiningCalcDataObject>();
        foreach (var calc in MiningCalculations)
        {
            calcDataList.Add(calc.GetPersistenceCalculationData);
        }
        return calcDataList;
    }

    public List<RefiningCalculationPersistenceData.RefiningCalcDataObject> AggregateRefiningCalculationData()
    {
        var calcDataList = new List<RefiningCalculationPersistenceData.RefiningCalcDataObject>();
        foreach (var calc in RefiningCalculations)
        {
            calcDataList.Add(calc.GetPersistenceCalculationData);
        }
        return calcDataList;
    }

    public List<ProductionCalculationPersistenceData.ProductionCalcDataObject> AggregateProductionCalculationData()
    {
        var calcDataList = new List<ProductionCalculationPersistenceData.ProductionCalcDataObject>();
        foreach (var calc in ProductionCalculations)
        {
            calcDataList.Add(calc.GetPersistenceCalculationData);
        }
        return calcDataList;
    }

    public List<PlanetRocketLaunchPersistenceData.PlanetRocketLaunchDataObject> AggregateRocketLaunchCalculationData()
    {
        var calcDataList = new List<PlanetRocketLaunchPersistenceData.PlanetRocketLaunchDataObject>();
        foreach (var calc in RocketLaunchCalculations)
        {
            calcDataList.Add(calc.GetPersistenceCalculationData);
        }
        return calcDataList;
    }

    public List<ConstructionCalculationPersistenceData.ConstructionCalcDataObject> AggregateConstructionCalculationData()
    {
        var calcDataList = new List<ConstructionCalculationPersistenceData.ConstructionCalcDataObject>();
        foreach (var calculation in ConstructionCalculations)
        {
            calcDataList.Add(calculation.GetPersistenceCalculationData);
        }

        return calcDataList;
    }

    public void InitArchaeologyCalculation(PlanetContents planet)
    {
        if (ArchaeologyCalculation != null) return;
        ArchaeologyCalculation = new ArchaeologyCalculation(planet);
    }

    public void InitRocketLaunchCalculation(Dictionary<StorableType, float> rocketCargo, PlanetContents planet, out RocketLaunchCalculation calcRef)
    {
        calcRef = new RocketLaunchCalculation(rocketCargo, planet);
        RocketLaunchCalculations.Add(calcRef);
        
        OnRocketLaunchQuantityChanged?.Invoke(this, new OnRocketLaunchEventArgs
        {
            CalcRef = calcRef, 
            IsCalculationBeingAdded = true
        });
    }

    public void InitProductionCalculation(IProductionRecipe productionRecipe, PlanetContents planet, int amountToProduce, out ProductionCalculation calcRef)
    {
        calcRef = new ProductionCalculation(productionRecipe, planet, amountToProduce);
        ProductionCalculations.Add(calcRef);
        
        OnProductionTasksChanged?.Invoke(this, new OnProductionChangeEventArgs
        {
            CalcRef = calcRef,
            IsCalculationBeingAdded = true 
        });
    }

    public void InitRefiningCalculation(PlanetContents planet, OreType oreType, RefiningProcess refiningProcess, out RefiningCalculation refCalc)
    {
        var calcHash = StringUtility.GetStringIntIntHash(refiningProcess.ToString(), (int) oreType, planet.PlanetID);
        var calcOperation = RefiningCalculations.Find(item => item.CalcHash == calcHash);
        if (calcOperation != null)
        {
            refCalc = calcOperation;
        }
        else
        {
            refCalc = new RefiningCalculation(calcHash, planet, oreType, refiningProcess);
            RefiningCalculations.Add(refCalc);
        }
    }

    public void InitMiningCalculation(PlanetContents planet, OreType oreType, out MiningCalculation miningCalc)
    {
        var calcHash = StringUtility.GetStringIntIntHash(oreType.ToString(), (int) oreType, planet.PlanetID);
        var calcOperation = MiningCalculations.Find(item => item.CalcHash == calcHash);
        if (calcOperation != null)
        {
            miningCalc = calcOperation;
        }
        else
        {
            miningCalc = new MiningCalculation(calcHash, planet, oreType);
            MiningCalculations.Add(miningCalc);
        }
    }

    public void InitConstructionCalculation(PlanetContents planet, BuildingType buildingType, out ConstructionCalculation calcRef)
    {
        calcRef = new ConstructionCalculation(planet, buildingType);
        ConstructionCalculations.Add(calcRef);
        
        OnConstructionQueueChanged?.Invoke(this, new OnConstructionQueueChangedEventArgs
        {
            CalcRef = calcRef,
            IsCalculationBeingAdded = true
        });
    }

    public void DisposeRocketLaunchCalculation(RocketLaunchCalculation calc)
    {
        RocketLaunchCalculations.Remove(calc);
        
        OnRocketLaunchQuantityChanged?.Invoke(this, new OnRocketLaunchEventArgs
        {
            CalcRef = calc,
            IsCalculationBeingAdded = false
        });
    }

    public void OnLaunchpadPageFirstRender()
    {
        OnRocketLaunchEventArgs launchArgs = new OnRocketLaunchEventArgs {IsCalculationBeingAdded = true};
        
        foreach (var calc in RocketLaunchCalculations)
        {
            launchArgs.CalcRef = calc;
            OnRocketLaunchQuantityChanged?.Invoke(this, launchArgs);
        }
    }

    public void DisposeProductionCalculation(ProductionCalculation calc)
    {
        ProductionCalculations.Remove(calc);
        
        OnProductionTasksChanged?.Invoke(this, new OnProductionChangeEventArgs
        {
            CalcRef = calc,
            IsCalculationBeingAdded = false 
        });
    }

    public void DisposeConstructionCalculation(ConstructionCalculation calc)
    {
        ConstructionCalculations.Remove(calc);
        
        OnConstructionQueueChanged?.Invoke(this, new OnConstructionQueueChangedEventArgs
        {
            CalcRef = calc,
            IsCalculationBeingAdded = false 
        });
    }

    public void UpdateCalculations(float tickDelta)
    {
        ExecuteArchaeology(tickDelta);
        ExecuteMining(tickDelta);
        ExecuteRefining(tickDelta);
        ExecuteProduction(tickDelta);
        ExecuteRocketLaunch(tickDelta);
        ExecuteConstruction(tickDelta);
    }

    private void ExecuteArchaeology(float tickDelta)
    {
        ArchaeologyCalculation?.Execute(tickDelta);
    }

    private void ExecuteMining(float tickDelta)
    {
        for (var i = MiningCalculations.Count - 1; i >= 0; i--)
        {
            MiningCalculations[i].Execute(tickDelta);
        }
    }

    private void ExecuteProduction(float tickDelta)
    {
        for (var i = ProductionCalculations.Count - 1; i >= 0; i--)
        {
            ProductionCalculations[i].Execute(tickDelta);
        }
    }

    private void ExecuteRefining(float tickDelta)
    {
        for (var i = RefiningCalculations.Count - 1; i >= 0; i--)
        {
            RefiningCalculations[i].Execute(tickDelta);
        }
    }

    private void ExecuteRocketLaunch(float tickDelta)
    {
        for (var i = RocketLaunchCalculations.Count - 1; i >= 0; i--)
        {
            RocketLaunchCalculations[i].Execute(tickDelta);
        }
    }

    private void ExecuteConstruction(float tickDelta)
    {
        if (ConstructionCalculations.Count > 0)
            ConstructionCalculations[0].Execute(tickDelta);
    }
}