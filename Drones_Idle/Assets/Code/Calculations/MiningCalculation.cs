﻿using System;
using System.Collections.Generic;
using Code.Utility;
using UnityEngine;

public class MiningCalculation
{
    public MiningCalculationsPersistenceData.MiningCalcDataObject GetPersistenceCalculationData =>
        new MiningCalculationsPersistenceData.MiningCalcDataObject
        {
            OreType = (int)oreData.OreType,
            MiningCycleCurrent = MiningCycleCurrent
        };

    public float CurrentMiningProgress => MiningCycleDuration - MiningCycleCurrent;
    public float MinedPerOperation => CalculateMinedDelta().ToTwoDigitFloat();
    public float MinedPerSecond => (CalculateMinedDelta() / MiningCycleDuration).ToTwoDigitFloat();

    public readonly int CalcHash;

    public float MiningCycleCurrent;
    public float MiningCycleDuration;
    
    private readonly IOre oreData;
    private readonly OreBucket oreBucket;
    private readonly PlanetContents planet;

    public MiningCalculation(int calcHash, PlanetContents planet, OreType oreType)
    {
        CalcHash = calcHash;
        this.planet = planet;
        oreData = DataInstanceFactory.GetOreInstance(oreType);
        oreBucket = planet.GetBucketByOreType(oreType);
        MiningCycleCurrent = 0f;
        MiningCycleDuration = oreData.Tier * DataValues.MiningCalculationOreTierDurationFactor;
    }

    public void Execute(float tickDelta)
    {
        if (!IsMiningCycleDone(tickDelta)) return;
        
        var minedDelta = CalculateMinedDelta();

        planet.PlanetStorageManager.AddUnitsToStorage(oreData.StorableType, minedDelta);
        oreBucket.OrePlanetSurveyed -= minedDelta;
    }

    private float CalculateMinedDelta()
    {
        var (dronesAmount, dronesWorkSpeed) = GetMiningDronesStats();

        float speedTierFactor = dronesWorkSpeed / DataValues.MiningCalculationBaseFactor * oreData.Tier;
        float volumeFactor = dronesAmount / oreData.UnitVolume;
        float minedDelta = speedTierFactor * volumeFactor;
        
        return minedDelta;
    }

    private (float, float) GetMiningDronesStats()
    {
        return planet.PlanetDroneManager.GetDroneStats(DroneRole.Mining, oreData.OreType, ProductionRecipeType.None);
    }

    private bool IsMiningCycleDone(float tickDelta)
    {
        if (planet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Mining, oreData.OreType, ProductionRecipeType.None) < 1)
            return false;
        
        MiningCycleDuration = EvaluateTotalCycleDuration();
        MiningCycleCurrent += tickDelta;
        
        if (MiningCycleCurrent >= MiningCycleDuration)
        {
            MiningCycleCurrent = 0f;
            return true;
        }
        return false;
    }
    
    private float EvaluateTotalCycleDuration()
    {
        var (_, workSpeed) = GetMiningDronesStats();
        var defaultDuration = oreData.Tier * DataValues.MiningCalculationOreTierDurationFactor;

        if (workSpeed <= 1f)
        {
            return defaultDuration;
        }

        return Mathf.Clamp(defaultDuration - Mathf.Log(workSpeed), DataValues.CalculationsUpdatePeriod, 3600f);
    }
}