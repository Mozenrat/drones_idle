using System.Collections.Generic;

public class ConstructionCalculation
{
    public ConstructionCalculationPersistenceData.ConstructionCalcDataObject GetPersistenceCalculationData =>
        new ConstructionCalculationPersistenceData.ConstructionCalcDataObject
        {
            BuildingType = (int) thisConstructable.BuildingType,
            ConstructionDurationCurrent = ConstructionDurationCurrent 
        };
    
    public IConstructable PeekConstructable => thisConstructable;
    public float TimeToConstructionCompletion => ConstructionDurationTotal - ConstructionDurationCurrent;
    
    public float ConstructionDurationTotal;
    public float ConstructionDurationCurrent;

    private readonly Dictionary<StorableType, float> costRefundPack;
    private readonly PlanetContents thisPlanet;
    private readonly IConstructable thisConstructable;
    
    public ConstructionCalculation(PlanetContents planet, BuildingType buildingType)
    {
        thisPlanet = planet;
        thisConstructable = DataInstanceFactory.GetConstructableInstance(buildingType);
        var bonusController = GlobalGameStateController.Instance.BonusController;
        
        costRefundPack = bonusController.GetBuildingConstructionCost(buildingType, planet);
        ConstructionDurationTotal = bonusController.GetBuildingConstructionDuration(thisConstructable.BuildingType, thisPlanet);
        ConstructionDurationCurrent = 0f;
    }
    
    public void Execute(float tickDelta)
    {
        ConstructionDurationCurrent += tickDelta;
        if (ConstructionDurationCurrent >= ConstructionDurationTotal)
        {
            ConstructionDurationCurrent = 0f;
            
            thisPlanet.PlanetBuildingManager.ConstructBuilding(thisConstructable.BuildingType, 1);
            DisposeThisCalculation();
        }
    }

    public void CancelThisConstructionTask()
    {
        foreach (var costItem in costRefundPack)
        {
            thisPlanet.PlanetStorageManager.AddUnitsToStorage(costItem.Key, costItem.Value);
        }
        DisposeThisCalculation();
    }

    private void DisposeThisCalculation()
    {
        thisPlanet.PlanetCalculationsManager.DisposeConstructionCalculation(this);
    }
}