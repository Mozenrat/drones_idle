using System.Collections.Generic;
using System.Linq;

public class WarfareCalculationsManager
{
    private readonly Dictionary<string, WarfareCalculation> WarfareCalculations;
    private readonly PlayerRepairCalculation PlayerRepairCalculation;

    private List<KeyValuePair<string, WarfareCalculation>> calculationsList;

    public WarfareCalculationsManager()
    {
        WarfareCalculations = new Dictionary<string, WarfareCalculation>();
        PlayerRepairCalculation = new PlayerRepairCalculation();
    }
    
    public void InitNewWarfareCalculation(WarfareZone zone, PlayerVesselFleet playerFleet, VesselFleet enemyFleet)
    {
        if (zone.ZoneTier == 0)
        {
            WarfareCalculations.Remove(playerFleet.FleetName);
            return;
        }
        
        var calc = new WarfareCalculation(playerFleet, enemyFleet);
        if (WarfareCalculations.ContainsKey(playerFleet.FleetName))
        {
            WarfareCalculations[playerFleet.FleetName] = calc;
        }
        else
        {
            WarfareCalculations.Add(playerFleet.FleetName, calc);
        }
    }

    public void UpdateCalculations(float tickDelta)
    {
        calculationsList = WarfareCalculations.ToList();
        foreach (var warfareCalculation in calculationsList)
        {
            warfareCalculation.Value.Execute(tickDelta);
        }
        PlayerRepairCalculation?.Execute(tickDelta);
    }
}