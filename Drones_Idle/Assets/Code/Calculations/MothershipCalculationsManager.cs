using System;
using System.Collections.Generic;

public class MothershipCalculationsManager
{
    public class OnShipyardTaskListChangedEventArgs : EventArgs
    {
        public MothershipShipyardCalculation CalcRef;
        public bool IsCalculationBeingAdded;
    }
    
    public event EventHandler<OnShipyardTaskListChangedEventArgs> OnShipyardTaskListChanged;
    
    public readonly List<MothershipShipyardCalculation> ShipyardCalculations = new List<MothershipShipyardCalculation>();
    public readonly List<ResearchCalculation> ResearchCalculations = new List<ResearchCalculation>();

    public List<ShipyardCalculationPersistenceData> AggregateShipyardCalculationData()
    {
        var calcDataList = new List<ShipyardCalculationPersistenceData>();
        foreach (var calc in ShipyardCalculations)
        {
            calcDataList.Add(calc.GetPersistenceCalculationData);
        }
        return calcDataList;
    }

    public void InitResearchCalculation(IResearchable researchable, out ResearchCalculation calcRef)
    {
        calcRef = new ResearchCalculation(researchable);
        ResearchCalculations.Add(calcRef);
    }

    public void InitShipyardCalculation(IShipyardRecipe vesselRecipe, int amountToProduce, out MothershipShipyardCalculation calcRef)
    {
        calcRef = new MothershipShipyardCalculation(vesselRecipe, amountToProduce);
        ShipyardCalculations.Add(calcRef);
        
        OnShipyardTaskListChanged?.Invoke(this, new OnShipyardTaskListChangedEventArgs
        {
            CalcRef = calcRef,
            IsCalculationBeingAdded = true
        });
    }

    public void DisposeResearchCalculation(ResearchCalculation calc)
    {
        ResearchCalculations.Remove(calc);
    }

    public ResearchCalculation GetActiveResearchCalculation()
    {
        return ResearchCalculations[0];
    }

    public void DisposeShipyardCalculation(MothershipShipyardCalculation calc)
    {
        ShipyardCalculations.Remove(calc);
        
        OnShipyardTaskListChanged?.Invoke(this, new OnShipyardTaskListChangedEventArgs
        {
            CalcRef = calc,
            IsCalculationBeingAdded = false
        });
    }

    public void OnShipyardPageFirstRender()
    {
        OnShipyardTaskListChangedEventArgs launchArgs = new OnShipyardTaskListChangedEventArgs {IsCalculationBeingAdded = true};
        
        foreach (var calc in ShipyardCalculations)
        {
            launchArgs.CalcRef = calc;
            OnShipyardTaskListChanged?.Invoke(this, launchArgs);
        }
    }
    
    public void UpdateCalculations(float tickDelta)
    {
        ExecuteShipyardProduction(tickDelta);
        ExecuteResearch(tickDelta);
    }

    private void ExecuteShipyardProduction(float tickDelta)
    {
        for (var i = ShipyardCalculations.Count - 1; i >= 0; i--)
        {
            ShipyardCalculations[i].Execute(tickDelta);
        }
    }

    private void ExecuteResearch(float tickDelta)
    {
        if (ResearchCalculations.Count > 0)
        {
            ResearchCalculations[0].Execute(tickDelta);
        }
    }
}