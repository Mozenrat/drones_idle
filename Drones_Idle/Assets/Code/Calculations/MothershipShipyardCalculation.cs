using System.Collections.Generic;
using Code.Utility;
using UnityEngine;

public class MothershipShipyardCalculation
{
    public ShipyardCalculationPersistenceData GetPersistenceCalculationData =>
        new ShipyardCalculationPersistenceData
        {
            ShipyardRecipe = (int) VesselRecipe.VesselType,
            ShipyardCycleCurrent = this.ShipyardTaskCycleCurrent,
            UnitsToProduce = this.UnitsToProduce,
            IsActive = IsActive
        };
    
    public float TimeToCompletion => (ShipyardTaskCycleDuration - ShipyardTaskCycleCurrent).ToTwoDigitFloat();
    public float TimeForOneCycle => ShipyardTaskCycleDuration.ToTwoDigitFloat();

    public bool IsEnoughResourcesToStart
    {
        get
        {
            var cost = bonusController.GetShipyardRecipeProductionCost(VesselRecipe);
            foreach (var item in cost)
            {
                if (mothershipEntity.MothershipStorageManager.GetUnitsStored(item.Key) < item.Value) return false;
            }
            return true;
        }
    }

    public readonly IShipyardRecipe VesselRecipe;
    public int UnitsToProduce;
    public float ShipyardTaskCycleCurrent;
    public float ShipyardTaskCycleDuration;
    public bool IsActive;

    private readonly BonusController bonusController;
    private readonly MothershipEntity mothershipEntity;
    
    public MothershipShipyardCalculation(IShipyardRecipe vesselRecipe, int quantityToProduce)
    {
        bonusController = GlobalGameStateController.Instance.BonusController;
        mothershipEntity = GlobalGameStateController.Instance.MothershipEntity;
        VesselRecipe = vesselRecipe;
        UnitsToProduce = quantityToProduce;
    }

    public void Execute(float tickDelta)
    {
        //if enough resources in storage, deduct those and set this calculation to active
        if (!IsActive && IsEnoughResourcesToStart)
        {
            IsActive = true;
            SubtractCostsFromStorage();
        }
        
        if (!IsActive) return;
        
        ShipyardTaskCycleDuration = bonusController.GetShipyardRecipeSpeed(VesselRecipe);

        ShipyardTaskCycleCurrent += tickDelta;
        if (ShipyardTaskCycleCurrent >= ShipyardTaskCycleDuration)
        {
            UnitsToProduce--;
            ShipyardTaskCycleCurrent = 0f;
            mothershipEntity.MothershipFleetStaging.AddShipToFleet(VesselRecipe.VesselType, 1);
            mothershipEntity.OnPlayerFleetDataChanged?.Invoke(null,
                new MothershipEntity.PlayerFleetChangedEventArgs(mothershipEntity.MothershipFleetStaging));
            if (UnitsToProduce <= 0)
            {
                StatusLogController.LogStatusMessage_Static($"Shipyard finished building {VesselRecipe.VesselName}");
                CancelThisShipyardTask();
                return;
            }

            IsActive = false;
        }
    }
    
    public void CancelThisShipyardTask()
    {
        mothershipEntity.MothershipCalculationsManager.DisposeShipyardCalculation(this);
    }
    
    private void SubtractCostsFromStorage()
    {
        var cost = bonusController.GetShipyardRecipeProductionCost(VesselRecipe);
        foreach (var item in cost)
        {
            mothershipEntity.MothershipStorageManager.SubtractUnitsFromStorage(item.Key, item.Value);
        }
    }
}