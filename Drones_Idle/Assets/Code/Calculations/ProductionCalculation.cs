﻿using System;
using System.Collections.Generic;
using Code.Utility;
using UnityEngine;

public class ProductionCalculation
{
    public ProductionCalculationPersistenceData.ProductionCalcDataObject GetPersistenceCalculationData =>
        new ProductionCalculationPersistenceData.ProductionCalcDataObject
        {
            ProductionRecipe = (int) Recipe.ProductionRecipeType,
            ProductionCycleCurrent = this.ProductionCycleCurrent,
            UnitsToProduce = this.UnitsToProduce,
            IsActive = IsActive
        };

    public float TimeToCompletion => (ProductionCycleDuration - ProductionCycleCurrent).ToTwoDigitFloat();
    public float TimeForOneCycle => ProductionCycleDuration.ToTwoDigitFloat();

    public bool IsEnoughResourcesToStart
    {
        get
        {
            foreach (var item in Recipe.BaseProductionCost)
            {
                if (thisPlanet.PlanetStorageManager.GetUnitsStored(item.Key) < item.Value) return false;
            }
            return true;
        }
    }

    public readonly IProductionRecipe Recipe;
    public int UnitsToProduce;
    public float ProductionCycleCurrent;
    public float ProductionCycleDuration;
    public bool IsActive;

    private readonly PlanetContents thisPlanet;
    private float activeDronesAmount;
    private float activeDronesWorkSpeed;

    public ProductionCalculation(IProductionRecipe productionRecipe, PlanetContents planet, int amountToProduce)
    {
        if (amountToProduce <= 0) throw new InvalidOperationException("UnitsToProduce can't be zero or negative");
        Recipe = productionRecipe;
        thisPlanet = planet;
        UnitsToProduce = amountToProduce;
    }

    public void Execute(float tickDelta)
    {
        //if enough resources in storage, deduct those and set this calculation to active
        if (!IsActive && IsEnoughResourcesToStart)
        {
            IsActive = true;
            SubtractCostsFromStorage();
        }

        //if calc is not active or no drones are assigned to this production task return
        if (thisPlanet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Production, OreType.None, Recipe.ProductionRecipeType) < 1 || !IsActive) return;

        CalculateWorkingDroneStats();
        var droneDurationEffect = (activeDronesAmount + Mathf.Sqrt(activeDronesWorkSpeed)) * 2f;
        ProductionCycleDuration =
            Mathf.Clamp(Recipe.ProductionDurationBase * thisPlanet.PlanetCalculationsManager.ProductionCalculationsCountOnPlanet - droneDurationEffect, 1f,
                float.MaxValue);

        ProductionCycleCurrent += tickDelta;
        if (ProductionCycleCurrent >= ProductionCycleDuration)
        {
            UnitsToProduce--;
            ProductionCycleCurrent = 0f;
            thisPlanet.PlanetStorageManager.AddUnitsToStorage(Recipe.StorableType, 1f);
            if (UnitsToProduce <= 0)
            {
                StatusLogController.LogStatusMessage_Static(
                    $"Production of {Recipe.StorableName} completed on {thisPlanet.PlanetName}");
                CancelThisProductionTask();
                return;
            }

            IsActive = false;
        }
    }

    public void CancelThisProductionTask()
    {
        var assignedDronesCount = thisPlanet.PlanetDroneManager.GetAssignedDronesCount(DroneRole.Production, OreType.None, Recipe.ProductionRecipeType);
        thisPlanet.PlanetDroneManager.UnassignDronesFromRole(DroneRole.Production, OreType.None, Recipe.ProductionRecipeType, assignedDronesCount);

        thisPlanet.PlanetCalculationsManager.DisposeProductionCalculation(this);
    }

    private void CalculateWorkingDroneStats()
    {
        (activeDronesAmount, activeDronesWorkSpeed) = 
            thisPlanet.PlanetDroneManager.GetDroneStats(DroneRole.Production, OreType.None, Recipe.ProductionRecipeType);
    }

    private void SubtractCostsFromStorage()
    {
        foreach (var item in Recipe.BaseProductionCost)
        {
            thisPlanet.PlanetStorageManager.SubtractUnitsFromStorage(item.Key, item.Value);
        }
    }
}