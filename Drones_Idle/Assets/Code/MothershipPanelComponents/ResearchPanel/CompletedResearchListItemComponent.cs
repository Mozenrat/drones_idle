﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CompletedResearchListItemComponent : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Image ItemBackgroundImage;
    [SerializeField] private TextMeshProUGUI CompletedResearchNameLabel;
    [SerializeField] private TextMeshProUGUI CompletedResearchLevel;
    [SerializeField] private TextMeshProUGUI ResearchDurationLabel;

    private BonusController bonusController;
    private MothershipEntity mothershipEntity;
    private ResearchPanelComponent researchPanelReference;

    private IResearchable thisResearchable;
    
    public void InitCompletedResearchListItem(ResearchableType researchableType, ResearchPanelComponent researchPanelRef)
    {
        bonusController = GlobalGameStateController.Instance.BonusController;
        mothershipEntity = GlobalGameStateController.Instance.MothershipEntity;
        researchPanelReference = researchPanelRef;
        thisResearchable = DataInstanceFactory.GetResearchableInstance(researchableType);
        
        CompletedResearchNameLabel.SetText(thisResearchable.ResearchableName);
        
        if (thisResearchable is IResearchArtifact || thisResearchable is IUpgradableTechnology)
        {
            int levelValue = mothershipEntity.TechnologyController.GetTechnologyLevel(researchableType);
            if (levelValue != 0)
            {
                CompletedResearchLevel.SetText("{0}", levelValue);
            }
            ResearchDurationLabel.SetText("{0}", bonusController.GetResearchDuration(researchableType));
        }
        else
        {
            CompletedResearchLevel.SetText("");
            ResearchDurationLabel.SetText("");
        }
    }
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        ItemBackgroundImage.color = DataValues.RecipeListItemBackgroundHoverColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ItemBackgroundImage.color = DataValues.RecipeListItemBackgroundRegularColor;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        researchPanelReference.InitDetailsForSelectedResearch(thisResearchable);
    }
}