﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResearchSelectorCategoryComponent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI ResearchCategoryLabel;

    public void InitResearchCategoryHeader(string categoryName)
    {
        ResearchCategoryLabel.SetText(categoryName);
    }
}