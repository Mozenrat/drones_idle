﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ResearchQueueComponent : MonoBehaviour
{
    [SerializeField] private GameObject ResearchQueueListItemsContent;
    [SerializeField] private ResearchQueueListItemComponent ResearchQueueListItemPrefab;

    [SerializeField] private GameObject ResearchQueueTimerContainer;
    [SerializeField] private Image ResearchQueueFill;
    [SerializeField] private TextMeshProUGUI ResearchQueueTimerLeftLabel;

    private Dictionary<ResearchableType, ResearchQueueListItemComponent> ActiveResearchQueueListItems;

    private MothershipEntity mothershipEntity;
    
    private float labelUpdateInterval;
    private float labelUpdateIntervalMax = .02f;
    
    private void Awake()
    {
        mothershipEntity = GlobalGameStateController.Instance.MothershipEntity;
        ActiveResearchQueueListItems = new Dictionary<ResearchableType, ResearchQueueListItemComponent>();

        mothershipEntity.TechnologyController.OnResearchTaskRemoved += RemoveQueueListItem;
        mothershipEntity.TechnologyController.OnResearchTaskAdded += AddQueueListItem;
        
        RenderResearchQueue();
    }
    
    private void Update()
    {
        labelUpdateInterval -= Time.deltaTime;
        if (labelUpdateInterval < 0)
        {
            labelUpdateInterval = labelUpdateIntervalMax;
            UpdateQueueTimer();
        }
    }

    private void UpdateQueueTimer()
    {
        var assignedResearchDuration = mothershipEntity.TechnologyController.TotalAssignedResearchDuration;
        if (Math.Abs(assignedResearchDuration) < 0.01f)
        {
            ResearchQueueTimerContainer.SetActive(false);
            return;
        }

        ResearchQueueTimerContainer.SetActive(true);
        TimeSpan assignedResearchTime = TimeSpan.FromSeconds(assignedResearchDuration);
        ResearchQueueTimerLeftLabel.SetText(assignedResearchTime.ToString("hh':'mm':'ss"));
        var barFill = Mathf.InverseLerp(0f, DataValues.ResearchQueueLengthBase,
            assignedResearchDuration);
        ResearchQueueFill.fillAmount = barFill;
    }

    private void AddQueueListItem(object sender, TechnologyController.OnResearchTaskAddedArgs e)
    {
        var listItem = Instantiate(ResearchQueueListItemPrefab, ResearchQueueListItemsContent.transform);
        listItem.InitResearchQueueItem(e.ResearchCalculation);
        ActiveResearchQueueListItems.Add(e.ResearchableType, listItem);
    }
    
    private void RemoveQueueListItem(object sender, TechnologyController.OnResearchTaskRemovedArgs e)
    {
        Destroy(ActiveResearchQueueListItems[e.ResearchableType].gameObject);
        ActiveResearchQueueListItems.Remove(e.ResearchableType);
    }

    private void RenderResearchQueue()
    {
        foreach (var activeResearchItem in mothershipEntity.TechnologyController.ActiveResearch)
        {
            var listItem = Instantiate(ResearchQueueListItemPrefab, ResearchQueueListItemsContent.transform);
            listItem.InitResearchQueueItem(activeResearchItem.Value);
            ActiveResearchQueueListItems.Add(activeResearchItem.Key, listItem);
        }
    }
}