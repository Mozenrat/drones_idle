﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ResearchDetailsComponent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI ResearchableNameLabel;
    [SerializeField] private TextMeshProUGUI ResearchableDetailsLabel;
    [SerializeField] private TextMeshProUGUI ResearchableStatsLabel;

    [SerializeField] private Button StartResearchButton;

    private BonusController bonusController;
    private MothershipEntity mothershipEntity;
    
    private IResearchable selectedResearchable;

    private void Awake()
    {
        bonusController = GlobalGameStateController.Instance.BonusController;
        mothershipEntity = GlobalGameStateController.Instance.MothershipEntity;
        StartResearchButton.onClick.AddListener(StartResearch);
    }

    public void InitDetails(IResearchable researchable)
    {
        selectedResearchable = researchable;
        
        ResearchableNameLabel.SetText(selectedResearchable.ResearchableName);
        ResearchableDetailsLabel.SetText(selectedResearchable.ResearchDetailsText);

        if (selectedResearchable is IUpgradableTechnology || selectedResearchable is IResearchArtifact)
        {
            var completedCount = mothershipEntity.TechnologyController.GetTechnologyLevel(selectedResearchable.ResearchableType);
            ResearchableStatsLabel.SetText("Research time: {0}\nTimes Completed: {1}", 
                bonusController.GetResearchDuration(researchable.ResearchableType), completedCount);
        }
        else
        {
            ResearchableStatsLabel.SetText("");
            StartResearchButton.gameObject.SetActive(false);
        }
    }

    private void StartResearch()
    {
        mothershipEntity.TechnologyController.AddToActiveResearch(selectedResearchable.ResearchableType);
    }
}