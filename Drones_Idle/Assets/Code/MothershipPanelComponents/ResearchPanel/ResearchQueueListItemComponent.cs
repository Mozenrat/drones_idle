﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ResearchQueueListItemComponent : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private TextMeshProUGUI ResearchItemName;
    [SerializeField] private TextMeshProUGUI ResearchItemQueueCount;
    [SerializeField] private TextMeshProUGUI ResearchItemTimeLeft;
    [SerializeField] private Image ResearchItemProgress;

    [SerializeField] private Button ResearchCountPlusButton;
    [SerializeField] private Button ResearchCountMinusButton;
    [SerializeField] private Button ResearchCancelButton;

    private MothershipEntity mothershipEntity;
    
    private ResearchCalculation thisCalculation;

    private readonly StringBuilder stringBuilder = new StringBuilder(100);
    
    private float labelUpdateInterval;
    private float labelUpdateIntervalMax = .02f;

    private void Update()
    {
        labelUpdateInterval -= Time.deltaTime;
        if (labelUpdateInterval < 0)
        {
            labelUpdateInterval = labelUpdateIntervalMax;
            UpdateListItem();
        }
    }
    
    public void InitResearchQueueItem(ResearchCalculation researchCalculation)
    {
        mothershipEntity = GlobalGameStateController.Instance.MothershipEntity;
        
        thisCalculation = researchCalculation;
        
        gameObject.AddComponent<TooltipProvider>().InitTooltip(GetTooltipString);
        
        ResearchCancelButton.onClick.RemoveAllListeners();
        ResearchCancelButton.onClick.AddListener(CancelThisResearch);
        ResearchCountMinusButton.onClick.RemoveAllListeners();
        ResearchCountMinusButton.onClick.AddListener(ReduceCountThisResearch);
        ResearchCountPlusButton.onClick.RemoveAllListeners();
        ResearchCountPlusButton.onClick.AddListener(AddCountThisResearch);
        UpdateListItem();
    }

    private void UpdateListItem()
    {
        ResearchItemName.SetText(thisCalculation.ResearchName);
        ResearchItemTimeLeft.SetText("{0}s", thisCalculation.TimeToCycleCompletion); //TODO: proper time func needed
        if (thisCalculation.ResearchCount > 1)
        {
            ResearchItemQueueCount.SetText("x{0}", thisCalculation.ResearchCount);
        }
        else
        {
            ResearchItemQueueCount.SetText("");
        }
        var progressCurrent =
            Mathf.InverseLerp(0f, thisCalculation.ResearchTaskCycleTotalProgress, thisCalculation.ResearchTaskCycleCurrentProgress);
        ResearchItemProgress.fillAmount = progressCurrent;
    }

    private string GetTooltipString()
    {
        stringBuilder.Clear();

        var dataInstance = DataInstanceFactory.GetResearchableInstance(thisCalculation.PeekType);
        stringBuilder.AppendLine("<b>Research Effect:</b>");
        stringBuilder.AppendLine(dataInstance.ResearchDetailsText);
        stringBuilder.AppendLine("Applied research speed bonuses: TODO");

        return stringBuilder.ToString();
    }

    private void CancelThisResearch()
    {
        mothershipEntity.TechnologyController.RemoveFromActiveResearch(thisCalculation.PeekType, true);
    }

    private void ReduceCountThisResearch()
    {
        if (thisCalculation.ResearchCount > 1)
        {
            thisCalculation.ReduceResearchCount(1);
        }
        if (thisCalculation.ResearchCount == 1)
        {
            ResearchCountMinusButton.gameObject.SetActive(false);
        }
    }

    private void AddCountThisResearch()
    {
        thisCalculation.AddResearchCount(1);
        if (thisCalculation.ResearchCount > 1)
        {
            ResearchCountMinusButton.gameObject.SetActive(true);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (thisCalculation.ResearchCount > 1)
        {
            ResearchCountMinusButton.gameObject.SetActive(true);
        }
        ResearchCancelButton.gameObject.SetActive(true);
        ResearchCountPlusButton.gameObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ResearchCancelButton.gameObject.SetActive(false);
        ResearchCountPlusButton.gameObject.SetActive(false);
        ResearchCountMinusButton.gameObject.SetActive(false);
    }
}