﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ResearchSelectorListItemComponent : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Image ItemBackgroundImage;
    [SerializeField] private TextMeshProUGUI ListItemNameLabel;
    [SerializeField] private TextMeshProUGUI ListItemArtifactLevelLabel;
    [SerializeField] private TextMeshProUGUI ListItemResearchDurationLabel;

    private BonusController bonusController;
    private MothershipEntity mothershipEntity;
    private ResearchPanelComponent researchPanelReference;

    private IResearchable thisResearchable;

    public void InitResearchSelectorListItem(ResearchableType researchableType, ResearchPanelComponent researchPanelRef)
    {
        bonusController = GlobalGameStateController.Instance.BonusController;
        mothershipEntity = GlobalGameStateController.Instance.MothershipEntity;
        researchPanelReference = researchPanelRef;
        thisResearchable = DataInstanceFactory.GetResearchableInstance(researchableType);

        ListItemNameLabel.SetText(thisResearchable.ResearchableName);
        ListItemResearchDurationLabel.SetText("{0}", bonusController.GetResearchDuration(thisResearchable.ResearchableType));

        if (thisResearchable is IResearchArtifact || thisResearchable is IUpgradableTechnology)
        {
            int levelValue = mothershipEntity.TechnologyController.GetTechnologyLevel(researchableType);
            if (levelValue != 0)
            {
                ListItemArtifactLevelLabel.SetText("{0}", levelValue);
            }
            else
            {
                ListItemArtifactLevelLabel.SetText("");
            }
        }
        else
        {
            ListItemArtifactLevelLabel.SetText("");
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ItemBackgroundImage.color = DataValues.RecipeListItemBackgroundHoverColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ItemBackgroundImage.color = DataValues.RecipeListItemBackgroundRegularColor;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        researchPanelReference.InitDetailsForSelectedResearch(thisResearchable);
    }
}