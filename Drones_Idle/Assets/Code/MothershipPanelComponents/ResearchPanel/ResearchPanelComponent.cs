﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResearchPanelComponent : MonoBehaviour
{
    [SerializeField] private GameObject NewResearchPanel;
    [SerializeField] private GameObject CompletedResearchPanel;
    
    [SerializeField] private GameObject NewResearchSelectorContent;
    [SerializeField] private GameObject CompletedResearchSelectorContent;
    
    [SerializeField] private Button NewResearchButton;
    [SerializeField] private Button CompletedResearchButton;
    [SerializeField] private Button ResearchQueueButton;

    [SerializeField] private ResearchQueueComponent ResearchQueueComponent;
    [SerializeField] private ResearchDetailsComponent ResearchDetailsComponent;
    
    [SerializeField] private ResearchSelectorCategoryComponent ResearchSelectorCategoryPrefab;
    [SerializeField] private ResearchSelectorListItemComponent NewResearchSelectorListItemPrefab;
    [SerializeField] private CompletedResearchListItemComponent CompletedResearchListItemPrefab;

    private List<GameObject> CurrentAvailableTechListItems;
    private List<GameObject> CurrentCompletedTechListItems;

    private MothershipEntity mothershipEntity;
    
    private void Awake()
    {
        mothershipEntity = GlobalGameStateController.Instance.MothershipEntity;
        CurrentAvailableTechListItems = new List<GameObject>();
        CurrentCompletedTechListItems = new List<GameObject>();
        
        NewResearchButton.onClick.AddListener(OpenNewResearchPanel);
        CompletedResearchButton.onClick.AddListener(OpenCompletedResearchPanel);
        ResearchQueueButton.onClick.AddListener(OpenResearchQueue);
    }

    private void OnEnable()
    {
        ResearchDetailsComponent.gameObject.SetActive(false);
        ResearchQueueComponent.gameObject.SetActive(true);
        
        RenderAvailableTechListItems();
    }
    
    public void InitDetailsForSelectedResearch(IResearchable researchable)
    {
        ResearchQueueComponent.gameObject.SetActive(false);
        ResearchDetailsComponent.gameObject.SetActive(true);
        
        ResearchDetailsComponent.InitDetails(researchable);
    }
    
    private void RenderAvailableTechListItems()
    {
        foreach (var listItem in CurrentAvailableTechListItems)
        {
            Destroy(listItem);
        }
        
        //render available tech from controller
        var availableTechHeader = Instantiate(ResearchSelectorCategoryPrefab, NewResearchSelectorContent.transform);
        availableTechHeader.InitResearchCategoryHeader("Technology advancements");
        CurrentAvailableTechListItems.Add(availableTechHeader.gameObject);
        foreach (var researchableType in mothershipEntity.TechnologyController.GetAvailableResearch())
        {
            var listItem = Instantiate(NewResearchSelectorListItemPrefab, NewResearchSelectorContent.transform);
            listItem.InitResearchSelectorListItem(researchableType, this);
            CurrentAvailableTechListItems.Add(listItem.gameObject);
        }

        //render research artifacts from the cargohold
        var artifactsCategoryHeader = Instantiate(ResearchSelectorCategoryPrefab, NewResearchSelectorContent.transform);
        artifactsCategoryHeader.InitResearchCategoryHeader("Research artifacts");
        CurrentAvailableTechListItems.Add(artifactsCategoryHeader.gameObject);
        foreach (var storableType in mothershipEntity.MothershipStorageManager.GetStoredTypes())
        {
            var storableInstance = DataInstanceFactory.GetStorableInstance(storableType);
            if (storableInstance is IResearchArtifact artifactInstance)
            {
                var listItem = Instantiate(NewResearchSelectorListItemPrefab, NewResearchSelectorContent.transform);
                listItem.InitResearchSelectorListItem(artifactInstance.ResearchableType, this);
                CurrentAvailableTechListItems.Add(listItem.gameObject);
            }
        }
    }

    private void RenderCompletedTechListItems()
    {
        foreach (var listItem in CurrentCompletedTechListItems)
        {
            Destroy(listItem);
        }

        foreach (var completedTech in mothershipEntity.TechnologyController.CompletedResearch)
        {
            var listItem = Instantiate(CompletedResearchListItemPrefab, CompletedResearchSelectorContent.transform);
            listItem.InitCompletedResearchListItem(completedTech, this);
            CurrentCompletedTechListItems.Add(listItem.gameObject);
        }
    }

    private void OpenNewResearchPanel()
    {
        CompletedResearchPanel.SetActive(false);
        NewResearchPanel.SetActive(true);
        
        RenderAvailableTechListItems();
        OpenResearchQueue();
    }

    private void OpenCompletedResearchPanel()
    {
        NewResearchPanel.SetActive(false);
        CompletedResearchPanel.SetActive(true);
        
        RenderCompletedTechListItems();
    }

    private void OpenResearchQueue()
    {
        ResearchDetailsComponent.gameObject.SetActive(false);
        ResearchQueueComponent.gameObject.SetActive(true);
    }
}