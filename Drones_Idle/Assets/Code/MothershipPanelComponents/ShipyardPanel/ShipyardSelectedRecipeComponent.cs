﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipyardSelectedRecipeComponent : MonoBehaviour
{
    [SerializeField] private GameObject SelectedRecipeContainer;
    [SerializeField] private GameObject SelectedRecipeIngredientListItemPrefab;

    private BonusController bonusController;
    
    private void Awake()
    {
        bonusController = GlobalGameStateController.Instance.BonusController;
    }

    public void PopulateSelectedRecipeIngredients(IShipyardRecipe shipyardVesselRecipe)
    {
        ClearContainer();

        var cost = bonusController.GetShipyardRecipeProductionCost(shipyardVesselRecipe);
        foreach (var costItem in cost)
        {
            var ingredientItem = Instantiate(SelectedRecipeIngredientListItemPrefab, SelectedRecipeContainer.transform);
            ingredientItem.GetComponent<SelectedVesselRecipeIngredientComponent>().SetLabels(costItem.Key, costItem.Value);
        }
    }
    
    private void ClearContainer()
    {
        for (int i = 0; i < SelectedRecipeContainer.transform.childCount; i++)
        {
            Destroy(SelectedRecipeContainer.transform.GetChild(i).gameObject);
        }
    }
}
