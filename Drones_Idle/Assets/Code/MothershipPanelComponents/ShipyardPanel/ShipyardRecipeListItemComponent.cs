﻿using System;
using System.Collections;
using System.Collections.Generic;
using Code.Utility;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ShipyardRecipeListItemComponent : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Image ItemBackgroundImage;
    
    [SerializeField] private TextMeshProUGUI RecipeName;
    [SerializeField] private TextMeshProUGUI RecipeDurationBase;

    private IShipyardRecipe thisItemRecipe;

    private ShipyardPanelComponent shipyardPanelReference;

    private BonusController bonusController;

    public void OnPointerClick(PointerEventData eventData)
    {
        shipyardPanelReference.SelectRecipe(thisItemRecipe);
    }

    public void InitListItem(ShipyardPanelComponent shipyardPanelRef, IShipyardRecipe recipeItem)
    {
        bonusController = GlobalGameStateController.Instance.BonusController;
        shipyardPanelReference = shipyardPanelRef;
        thisItemRecipe = recipeItem;

        var recipeDuration = bonusController.GetShipyardRecipeSpeed(recipeItem);
        TimeSpan timespan = TimeSpan.FromSeconds(recipeDuration);
        string formattedDuration = timespan.ToString(recipeDuration > 3600f ? @"h\:m\:ss" : @"m\:ss");

        RecipeName.SetText(thisItemRecipe.VesselName);
        RecipeDurationBase.SetText(formattedDuration);
    }
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        ItemBackgroundImage.color = DataValues.RecipeListItemBackgroundHoverColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ItemBackgroundImage.color = DataValues.RecipeListItemBackgroundRegularColor;
    }
}
