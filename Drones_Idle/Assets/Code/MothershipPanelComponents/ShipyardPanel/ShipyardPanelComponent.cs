﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShipyardPanelComponent : MonoBehaviour
{
    [SerializeField] private ShipyardSelectedRecipeComponent ShipyardSelectedRecipeComponent;

    [SerializeField] private Button NewShipyardTaskButton;
    [SerializeField] private Button ActiveShipyardTasksButton;

    [SerializeField] private GameObject NewShipyardTaskPanel;
    [SerializeField] private GameObject ActiveShipyardTasksPanel;

    [SerializeField] private GameObject ActiveShipyardTaskContainer;
    [SerializeField] private GameObject ActiveShipyardTaskListItemPrefab;
    
    [SerializeField] private GameObject VesselRecipeListContainer;
    [SerializeField] private GameObject VesselRecipeListItemPrefab;

    [SerializeField] private TextMeshProUGUI SelectedRecipeLabel;
    [SerializeField] private TMP_InputField ShipyardProductionQuantityInput;
    [SerializeField] private Button SendToShipyardQueueButton;

    private IShipyardRecipe selectedRecipe;
    
    private Stack<ActiveShipyardTaskListItemComponent> activeShipyardTaskItemsPool;
    private List<ActiveShipyardTaskListItemComponent> activeShipyardTaskListItems;

    private MothershipEntity mothershipEntity;
    
    public void SelectRecipe(IShipyardRecipe shipyardRecipe)
    {
        selectedRecipe = shipyardRecipe;
        ShipyardSelectedRecipeComponent.PopulateSelectedRecipeIngredients(shipyardRecipe);
        SelectedRecipeLabel.SetText(selectedRecipe.VesselName);
    }
    
    public void SendSelectedRecipeInShipyardQueue()
    {
        if (selectedRecipe == null)
        {
            NotificationController.ShowNotification_Static(DataValues.NoShipyardRecipeSelected, true);
            return;
        }

        if (!int.TryParse(ShipyardProductionQuantityInput.text, out int parseResult) ||
            parseResult == 0)
        {
            NotificationController.ShowNotification_Static(DataValues.NoValidShipyardProductionAmount, true);
            return;
        }

        mothershipEntity.MothershipCalculationsManager.InitShipyardCalculation(
            selectedRecipe,
            parseResult,
            out _);
        
        selectedRecipe = null;
        SelectedRecipeLabel.SetText(DataValues.ShipyardPageUiSelectRecipeText);
        ShipyardProductionQuantityInput.text = "";
        
        NotificationController.ShowNotification_Static(DataValues.ShipyardTaskCreationSuccessful, false);
    }
    
    private void Awake()
    {
        mothershipEntity = GlobalGameStateController.Instance.MothershipEntity;
        
        activeShipyardTaskListItems = new List<ActiveShipyardTaskListItemComponent>();
        activeShipyardTaskItemsPool = FillActiveShipyardTaskItemsPool();

        mothershipEntity.MothershipCalculationsManager.OnShipyardTaskListChanged += RenderActiveShipyardItem;
        
        NewShipyardTaskButton.onClick.AddListener(GoToNewShipyardTask);
        ActiveShipyardTasksButton.onClick.AddListener(GoToActiveShipyardTasks);
        
        SendToShipyardQueueButton.onClick.AddListener(SendSelectedRecipeInShipyardQueue);
    }

    private void Start()
    {
        RenderKnownShipyardRecipes();
        
        mothershipEntity.MothershipCalculationsManager.OnShipyardPageFirstRender();
    }
    
    private void RenderActiveShipyardItem(object sender, MothershipCalculationsManager.OnShipyardTaskListChangedEventArgs onShipyardTaskListChangedEventArgs)
    {
        if (onShipyardTaskListChangedEventArgs.IsCalculationBeingAdded)
        {
            var listItem = activeShipyardTaskItemsPool.Pop();
            listItem.gameObject.SetActive(true);
            listItem.InitActiveShipyardTaskListItem(onShipyardTaskListChangedEventArgs.CalcRef);
            listItem.transform.SetAsLastSibling();
            activeShipyardTaskListItems.Add(listItem);
        }
        else
        {
            var listItem = activeShipyardTaskListItems.Find(item => item.PeekShipyardCalculation == onShipyardTaskListChangedEventArgs.CalcRef);
            if (listItem == null) 
                return;
            
            activeShipyardTaskListItems.Remove(listItem);
            listItem.gameObject.SetActive(false);
            activeShipyardTaskItemsPool.Push(listItem);
        }
    }
    
    private Stack<ActiveShipyardTaskListItemComponent> FillActiveShipyardTaskItemsPool()
    {
        var itemsPool = new Stack<ActiveShipyardTaskListItemComponent>();

        //TODO: concerned that only 10 active list items is possible
        for (int i = 0; i < 10; i++)
        {
            var activeListItem = Instantiate(ActiveShipyardTaskListItemPrefab, ActiveShipyardTaskContainer.transform);
            itemsPool.Push(activeListItem.GetComponent<ActiveShipyardTaskListItemComponent>());
            activeListItem.SetActive(false);
        }

        return itemsPool;
    }

    private void RenderKnownShipyardRecipes()
    {
        foreach (var vesselRecipe in mothershipEntity.ShipyardVesselRecipes)
        {
            var listItem = Instantiate(VesselRecipeListItemPrefab, VesselRecipeListContainer.transform);
            listItem.GetComponent<ShipyardRecipeListItemComponent>().InitListItem(this, vesselRecipe);
        }
    }

    private void GoToNewShipyardTask()
    {
        ActiveShipyardTasksPanel.SetActive(false);
        NewShipyardTaskPanel.SetActive(true);
    }

    private void GoToActiveShipyardTasks()
    {
        NewShipyardTaskPanel.SetActive(false);
        ActiveShipyardTasksPanel.SetActive(true);
    }
}