﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Code.Utility;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectedVesselRecipeIngredientComponent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI IngredientItemName;
    [SerializeField] private TextMeshProUGUI Quantity;

    private bool IsEnoughOnMotherShip => mothershipEntity.MothershipStorageManager.GetUnitsStored(thisStorable.StorableType) >= quantityNeeded;
    private bool IsEnoughOnAnyPlanet
    {
        get
        {
            foreach (var planet in GlobalGameStateController.Instance.ActivePlanetList)
            {
                if (planet.PlanetStorageManager.GetUnitsStored(thisStorable.StorableType) >= quantityNeeded)
                    return true;
            }
            return false;
        }
    }

    private readonly Color32 enoughOnMothershipColor = new Color32(115, 147, 224, 255);
    private readonly Color32 enoughOnPlanetColor = new Color32(224, 203, 115, 255);
    private readonly Color32 notEnoughAnywhereColor = new Color32(219, 72, 69, 255);

    private IStorable thisStorable;
    private float quantityNeeded;
    private MothershipEntity mothershipEntity;
    
    private readonly StringBuilder stringBuilder = new StringBuilder(100);

    private float labelUpdateInterval;
    private float labelUpdateIntervalMax = 1f;

    public void SetLabels(StorableType ingredientStorable, float quantity)
    {
        mothershipEntity = GlobalGameStateController.Instance.MothershipEntity;
        thisStorable = DataInstanceFactory.GetStorableInstance(ingredientStorable);
        quantityNeeded = quantity;
        
        IngredientItemName.SetText(thisStorable.StorableName);
        IngredientItemName.margin = new Vector4(10, 4, 4, 4);
        Quantity.SetText("{0:0}", quantity);
        if (!IsEnoughOnMotherShip)
        {
            Quantity.color = IsEnoughOnAnyPlanet ? enoughOnPlanetColor : notEnoughAnywhereColor;
        }
        else
        {
            Quantity.color = enoughOnMothershipColor;
        }
        
        gameObject.AddComponent<TooltipProvider>().InitTooltip(GetTooltip);
    }

    private void Update()
    {
        labelUpdateInterval += Time.deltaTime;
        if (labelUpdateInterval >= labelUpdateIntervalMax)
        {
            if (!IsEnoughOnMotherShip)
            {
                Quantity.color = IsEnoughOnAnyPlanet ? enoughOnPlanetColor : notEnoughAnywhereColor;
            }
            else
            {
                Quantity.color = enoughOnMothershipColor;
            }
            labelUpdateInterval = 0f;
        }
    }

    private string GetTooltip()
    {
        stringBuilder.Clear();
        
        if (!IsEnoughOnMotherShip)
        {
            stringBuilder.AppendLine("Not enough in storage on mothership");
            stringBuilder.AppendLine();
        }
        
        stringBuilder.AppendLine($"Storage overview for {thisStorable.StorableName}:");
        stringBuilder.Append("    Mothership: ")
            .Append(mothershipEntity.MothershipStorageManager.GetUnitsStored(thisStorable.StorableType).ToTwoDigitFloat())
            .AppendLine();
        foreach (var planet in GlobalGameStateController.Instance.ActivePlanetList)
        {
            stringBuilder.Append($"    {planet.PlanetName}: ")
                .Append(planet.PlanetStorageManager.GetUnitsStored(thisStorable.StorableType).ToTwoDigitFloat())
                .AppendLine();
        }

        stringBuilder.AppendLine();
        stringBuilder.AppendLine(thisStorable.StorableInfoString);

        return stringBuilder.ToString();
    }
}
