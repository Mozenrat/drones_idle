﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using Code.Utility;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ActiveShipyardTaskListItemComponent : MonoBehaviour
{
    [SerializeField] private Image ShipyardTaskProgress;
    [SerializeField] private TextMeshProUGUI RecipeNameLabel;
    [SerializeField] private TextMeshProUGUI RemainingTextLabel;
    [SerializeField] private TextMeshProUGUI InProductionQuantityLabel;
    [SerializeField] private Button CancelThisShipyardTaskButton;
    
    public MothershipShipyardCalculation PeekShipyardCalculation => thisCalculation;

    private BonusController bonusController;
    private MothershipStorageManager mothershipStorageManager;
    private MothershipShipyardCalculation thisCalculation;
    
    private float labelUpdateInterval;
    private float labelUpdateIntervalMax = .02f;
    
    private readonly StringBuilder stringBuilder = new StringBuilder(100);
    
    public void InitActiveShipyardTaskListItem(MothershipShipyardCalculation shipyardCalculation)
    {
        bonusController = GlobalGameStateController.Instance.BonusController;
        mothershipStorageManager = GlobalGameStateController.Instance.MothershipEntity.MothershipStorageManager;
        thisCalculation = shipyardCalculation;
        
        CancelThisShipyardTaskButton.onClick.RemoveAllListeners();
        CancelThisShipyardTaskButton.onClick.AddListener(CancelThisShipyardTask);
        
        var nameTooltip = RecipeNameLabel.gameObject.AddComponent<TooltipProvider>();
        nameTooltip.InitTooltip(GetTooltipString);
        var progressTooltip = RemainingTextLabel.gameObject.AddComponent<TooltipProvider>();
        progressTooltip.InitTooltip(GetTooltipString);
    }

    private void Update()
    {
        labelUpdateInterval -= Time.deltaTime;
        if (labelUpdateInterval < 0)
        {
            labelUpdateInterval = labelUpdateIntervalMax;
            UpdateListItem();
        }
    }
    
    private void OnDisable()
    {
        if (MouseUtility.IsMouseOverUiGameObject(gameObject))
        {
            TooltipController.HideTooltip_Static();
        }
    }
    
    private string GetTooltipString()
    {
        stringBuilder.Clear();
        
        if (!thisCalculation.IsActive && !thisCalculation.IsEnoughResourcesToStart)
        {
            stringBuilder.AppendLine("Not enough resources in mothership storage to start production");
            stringBuilder.AppendLine();

            var cost = bonusController.GetShipyardRecipeProductionCost(thisCalculation.VesselRecipe);
            foreach (var item in cost)
            {
                var storable = DataInstanceFactory.GetStorableInstance(item.Key);
                stringBuilder.Append($"    {storable.StorableName} required: ").Append(item.Value)
                    .Append(" / ")
                    .Append(mothershipStorageManager.GetUnitsStored(item.Key).ToTwoDigitFloat()).AppendLine();
            }

            return stringBuilder.ToString();
        }

        stringBuilder.Append("Time to produce one unit: ").Append(thisCalculation.TimeForOneCycle).AppendLine(" sec");
        stringBuilder.Append("Remaining: ").Append(thisCalculation.TimeToCompletion).AppendLine(" sec");
        return stringBuilder.ToString();
    }

    private void CancelThisShipyardTask()
    {
        StatusLogController.LogStatusMessage_Static($"Shipyard task for {thisCalculation.UnitsToProduce} units of {thisCalculation.VesselRecipe.VesselName} cancelled");
        thisCalculation.CancelThisShipyardTask();
    }
    
    private void UpdateListItem()
    {
        RecipeNameLabel.SetText(thisCalculation.VesselRecipe.VesselName);
        ShipyardTaskProgress.fillAmount = Mathf.InverseLerp(0, thisCalculation.ShipyardTaskCycleDuration, thisCalculation.ShipyardTaskCycleCurrent);
        InProductionQuantityLabel.SetText("{0}", thisCalculation.UnitsToProduce);
    }
}
