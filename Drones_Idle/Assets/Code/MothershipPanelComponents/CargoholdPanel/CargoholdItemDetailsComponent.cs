﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CargoholdItemDetailsComponent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI StorableNameLabel;
    [SerializeField] private TextMeshProUGUI StorableItemInfoLabel;
    [SerializeField] private TextMeshProUGUI StorableItemStatsLabel;
    [SerializeField] private Button SendToForgeButton;
    [SerializeField] private Button SendToResearchButton;

    private StringBuilder stringBuilder = new StringBuilder();

    private void OnEnable()
    {
        StorableNameLabel.SetText("");
        StorableItemInfoLabel.SetText("");
        StorableItemStatsLabel.SetText("");
        
        SendToForgeButton.gameObject.SetActive(false);
        SendToResearchButton.gameObject.SetActive(false);
    }

    public void PopulateStorableDetails(IStorable storable)
    {
        SendToForgeButton.gameObject.SetActive(true);
        
        StorableNameLabel.SetText(storable.StorableName);

        if (storable is IResearchArtifact researchable)
        {
            var researchDuration =
                GlobalGameStateController.Instance.BonusController.GetResearchDuration(researchable.ResearchableType);
            stringBuilder.Clear();
            stringBuilder.Append(researchable.StorableInfoString)
                .Append("\n\n")
                .Append(researchable.ResearchDetailsText)
                .Append("\n")
                .Append("<b>Research time to next level:</b> ")
                .Append(researchDuration);
            
            StorableItemInfoLabel.SetText(stringBuilder.ToString());
            SendToResearchButton.gameObject.SetActive(true);
        }
        else
        {
            StorableItemInfoLabel.SetText(storable.StorableInfoString);
            SendToResearchButton.gameObject.SetActive(false);
        }
        
        StorableItemStatsLabel.SetText("<b>Unit Volume:</b> {0:2} m3\n<b>Stored in Mothership:</b> {1:0}\n<b>Stored at planets:</b> {2:0}",
            storable.UnitVolume,
            GlobalGameStateController.Instance.MothershipEntity.MothershipStorageManager.GetUnitsStored(
                storable.StorableType),
            GlobalGameStateController.Instance.GetPlanetById(1)
                .PlanetStorageManager.GetUnitsStored(storable.StorableType));
    }
}