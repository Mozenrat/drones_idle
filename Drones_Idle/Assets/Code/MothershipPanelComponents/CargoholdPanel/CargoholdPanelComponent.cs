﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CargoholdPanelComponent : MonoBehaviour
{
    [SerializeField] private GameObject CargoholdMaterialsContainer;
    [SerializeField] private GameObject CargoholdArtifactsContainer;

    [SerializeField] private GameObject CargoholdArtifactsContentContainer;
    [SerializeField] private GameObject CargoholdMaterialsContentContainer;
    [SerializeField] private GameObject CargoholdMaterialsSpacer;
    [SerializeField] private CargoholdItemDetailsComponent CargoholdItemDetailsComponent;

    [SerializeField] private Button CargoholdMaterialButton;
    [SerializeField] private Button CargoholdArtifactsButton;

    [SerializeField] private CargoholdMaterialsListItemComponent CargoholdMaterialsListItemPrefab;
    [SerializeField] private CargoholdArtifactsListItemComponent CargoholdArtifactsListItemPrefab;

    private Dictionary<StorableType, CargoholdMaterialsListItemComponent> existingMaterialsCargoholdItems;
    private Dictionary<StorableType, CargoholdArtifactsListItemComponent> existingArtifactsCargoholdItems;
    
    private MothershipStorageManager mothershipStorageManager;
    
    private void Awake()
    {
        mothershipStorageManager = GlobalGameStateController.Instance.MothershipEntity.MothershipStorageManager;
        existingMaterialsCargoholdItems = new Dictionary<StorableType, CargoholdMaterialsListItemComponent>();
        existingArtifactsCargoholdItems = new Dictionary<StorableType, CargoholdArtifactsListItemComponent>();
        
        CargoholdMaterialButton.onClick.AddListener(SwitchToCargoholdMaterialsSection);
        CargoholdArtifactsButton.onClick.AddListener(SwitchToCargoholdArtifactsSection);
    }

    private void OnEnable()
    {
        RePopulateMaterialCargoholdContents();
        RePopulateArtifactsCargoholdContents();

        mothershipStorageManager.OnMothershipStorageContentChanged += ChangeStorableQuantityLabel;
    }

    private void OnDisable()
    {
        mothershipStorageManager.OnMothershipStorageContentChanged -= ChangeStorableQuantityLabel;
    }

    public void PopulateStorableDetails(IStorable storableInstance)
    {
        CargoholdItemDetailsComponent.PopulateStorableDetails(storableInstance);
    }

    private void ChangeStorableQuantityLabel(object sender, MothershipStorageManager.OnMothershipStorageContentChangedEventArgs e)
    {
        if (e.Storable is IResearchArtifact)
        {
            if (existingArtifactsCargoholdItems.ContainsKey(e.Storable.StorableType))
            {
                existingArtifactsCargoholdItems[e.Storable.StorableType].ChangeListItemQuantity(e.Quantity);
            }
            else
            {
                CreateNewArtifactsListItem(e.Storable.StorableType);
            }
        }
        else
        {
            if (existingMaterialsCargoholdItems.ContainsKey(e.Storable.StorableType))
            {
                existingMaterialsCargoholdItems[e.Storable.StorableType].ChangeListItemQuantity(e.Quantity);
            }
            else
            {
                CreateNewMaterialsListItem(e.Storable.StorableType);
            }
            CargoholdMaterialsSpacer.transform.SetAsLastSibling();
        }
    }

    private void CreateNewMaterialsListItem(StorableType storable)
    {
        var listItem = Instantiate(CargoholdMaterialsListItemPrefab, CargoholdMaterialsContentContainer.transform);
        listItem.InitCargoholdListItem(storable, mothershipStorageManager.GetUnitsStored(storable), this);
        listItem.transform.SetAsLastSibling();
        existingMaterialsCargoholdItems.Add(storable, listItem);
    }

    private void CreateNewArtifactsListItem(StorableType storable)
    {
        var listItem = Instantiate(CargoholdArtifactsListItemPrefab, CargoholdArtifactsContentContainer.transform);
        listItem.InitArtifactsListItem(storable, mothershipStorageManager.GetUnitsStored(storable), this);
        listItem.transform.SetAsLastSibling();
        existingArtifactsCargoholdItems.Add(storable, listItem);
    }

    private void RePopulateMaterialCargoholdContents()
    {
        if (existingMaterialsCargoholdItems.Count > 0)
        {
            foreach (var listItem in existingMaterialsCargoholdItems)
            {
                Destroy(listItem.Value.gameObject);
            }
            existingMaterialsCargoholdItems.Clear();
        }

        foreach (var storable in mothershipStorageManager.GetStoredTypes())
        {
            var storableInstance = DataInstanceFactory.GetStorableInstance(storable);
            if (storableInstance is IResearchArtifact) continue;

            CreateNewMaterialsListItem(storable);
        }
        CargoholdMaterialsSpacer.transform.SetAsLastSibling();
    }

    private void RePopulateArtifactsCargoholdContents()
    {
        if (existingArtifactsCargoholdItems.Count > 0)
        {
            foreach (var listItem in existingArtifactsCargoholdItems)
            {
                Destroy(listItem.Value.gameObject);
            }
            existingArtifactsCargoholdItems.Clear();
        }

        foreach (var storable in mothershipStorageManager.GetStoredTypes())
        {
            var storableInstance = DataInstanceFactory.GetStorableInstance(storable);
            if (storableInstance is IResearchArtifact)
            {
                CreateNewArtifactsListItem(storable);
            }
        }
    }

    private void SwitchToCargoholdMaterialsSection()
    {
        if (CargoholdMaterialsContainer.activeSelf) return;
        
        CargoholdArtifactsContainer.SetActive(false);
        CargoholdMaterialsContainer.SetActive(true);
    }

    private void SwitchToCargoholdArtifactsSection()
    {
        if (CargoholdArtifactsContainer.activeSelf) return;
        
        CargoholdMaterialsContainer.SetActive(false);
        CargoholdArtifactsContainer.SetActive(true);
    }
}