﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CargoholdMaterialsListItemComponent : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Image ItemBackgroundImage;
    [SerializeField] private TextMeshProUGUI ListItemStorableNameLabel;
    [SerializeField] private TextMeshProUGUI ListItemStorableQuantityLabel;

    private CargoholdPanelComponent cargoholdPanelReference;
    private IStorable thisStorableInstance;
    
    public void InitCargoholdListItem(StorableType storableType, float quantity, CargoholdPanelComponent cargoholdPanelRef)
    {
        cargoholdPanelReference = cargoholdPanelRef;
        thisStorableInstance = DataInstanceFactory.GetStorableInstance(storableType);
        ListItemStorableNameLabel.SetText(thisStorableInstance.StorableName);
        ListItemStorableQuantityLabel.SetText("{0}", quantity);
    }

    public void ChangeListItemQuantity(float newQuantity)
    {
        ListItemStorableQuantityLabel.SetText("{0}", newQuantity);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ItemBackgroundImage.color = DataValues.RecipeListItemBackgroundHoverColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ItemBackgroundImage.color = DataValues.RecipeListItemBackgroundRegularColor;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        cargoholdPanelReference.PopulateStorableDetails(thisStorableInstance);
    }
}