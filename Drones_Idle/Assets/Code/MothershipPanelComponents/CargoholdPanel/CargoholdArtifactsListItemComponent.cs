﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CargoholdArtifactsListItemComponent : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Image ListItemBackground;
    [SerializeField] private TextMeshProUGUI ListItemQuantityLabel;
    
    private CargoholdPanelComponent cargoholdPanelReference;
    private IStorable thisStorableInstance;

    public void InitArtifactsListItem(StorableType storable, float quantity, CargoholdPanelComponent cargoholdPanelRef)
    {
        cargoholdPanelReference = cargoholdPanelRef;
        thisStorableInstance = DataInstanceFactory.GetStorableInstance(storable);
        ListItemQuantityLabel.SetText("{0}", quantity);
    }

    public void ChangeListItemQuantity(float newQuantity)
    {
        ListItemQuantityLabel.SetText("{0}", newQuantity);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        cargoholdPanelReference.PopulateStorableDetails(thisStorableInstance);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ListItemBackground.color = DataValues.CargoholdListItemTileHoverBackgroundColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ListItemBackground.color = DataValues.CargoholdListItemTileBackgroundColor;
    }
}