using System;
using System.Collections.Generic;

public class PlanetArchaeologyManager
{
    public class OnMilestoneLevelChangedEventArgs : EventArgs
    {
        public ArchaeologyMilestoneType MilestoneType;
        public int NewLevel;
    }
    
    public class OnNewMilestoneUnlockedEventArgs : EventArgs
    {
        public ArchaeologyMilestoneObject MilestoneObject;
    }
    
    public ArchaeologyMilestoneObject ActiveArchaeologyObject;

    public event EventHandler<OnMilestoneLevelChangedEventArgs> OnMilestoneLevelChanged;
    public event EventHandler<OnNewMilestoneUnlockedEventArgs> OnNewMilestoneUnlocked;

    public List<PlanetArchaeologyPersistenceData.PlanetArchaeologyMilestoneDataObject> AggregatePlanetArchaeologyData()
    {
        var milestoneList = new List<PlanetArchaeologyPersistenceData.PlanetArchaeologyMilestoneDataObject>();
        foreach (var milestone in PlanetMilestones)
        {
            milestoneList.Add(new PlanetArchaeologyPersistenceData.PlanetArchaeologyMilestoneDataObject
            {
                MilestoneType = milestone.Value.MilestoneType,
                IsUnlocked = milestone.Value.IsUnlocked,
                MilestoneLevel = milestone.Value.MilestoneLevel,
                CurrentMilestoneProgress = milestone.Value.MilestoneCurrentPointsProgress
            });
        }

        return milestoneList;
    }

    public float GetArchaeologyMilestoneIncrement
    {
        get
        {
            var (_, droneEffect) = thisPlanet.PlanetDroneManager.GetDroneStats(DroneRole.Archaeology, OreType.None, ProductionRecipeType.None);
            //droneEffect in this instance is amount of progress points a milestone will get over a period of one second 
            return droneEffect;
        }
    }

    private readonly Dictionary<ArchaeologyMilestoneType, ArchaeologyMilestoneObject> PlanetMilestones;
    private readonly PlanetContents thisPlanet;

    public PlanetArchaeologyManager(PlanetContents planet)
    {
        PlanetMilestones = new Dictionary<ArchaeologyMilestoneType, ArchaeologyMilestoneObject>();
        thisPlanet = planet;

        foreach (var milestoneType in (ArchaeologyMilestoneType[]) Enum.GetValues(typeof(ArchaeologyMilestoneType)))
        {
            ArchaeologyMilestoneObject milestoneObject = new ArchaeologyMilestoneObject(this, milestoneType);
            PlanetMilestones.Add(milestoneType, milestoneObject);
        }
        
        ActiveArchaeologyObject = PlanetMilestones[ArchaeologyMilestoneType.InitialDig];
        
        planet.PlanetCalculationsManager.InitArchaeologyCalculation(planet);
    }

    public void SetActiveMilestone(ArchaeologyMilestoneType milestoneType)
    {
        ActiveArchaeologyObject = PlanetMilestones[milestoneType];
    }

    public void TriggerNewMilestoneUnlocked(ArchaeologyMilestoneObject milestoneObject)
    {
        OnNewMilestoneUnlocked?.Invoke(null, new OnNewMilestoneUnlockedEventArgs
        {
            MilestoneObject = milestoneObject
        });
        
        StatusLogController.LogStatusMessage_Static($"New archaeology milestone unlocked at {thisPlanet.PlanetName}!");
    }

    public void TriggerMilestoneLevelUp(ArchaeologyMilestoneType milestoneType, int newLevel)
    {
        OnMilestoneLevelChanged?.Invoke(null, new OnMilestoneLevelChangedEventArgs
        {
            MilestoneType = milestoneType,
            NewLevel = newLevel
        });
    }

    public ArchaeologyMilestoneObject GetMilestoneByType(ArchaeologyMilestoneType milestoneType)
    {
        return PlanetMilestones[milestoneType];
    }

    public Dictionary<ArchaeologyMilestoneType, ArchaeologyMilestoneObject> GetAllMilestones()
    {
        return PlanetMilestones;
    }
}