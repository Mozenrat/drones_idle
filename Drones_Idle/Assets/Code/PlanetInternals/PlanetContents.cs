﻿using System.Collections.Generic;
using System.Linq;

public class PlanetContents
{
    public int PlanetID;
    public string PlanetName;
    public int PlanetTier;
    public int BiomassAbundance;

    public List<OreBucket> OreBuckets = new List<OreBucket>();

    public PlanetDroneManager PlanetDroneManager;
    public PlanetStorageManager PlanetStorageManager;
    public PlanetBuildingManager PlanetBuildingManager;
    public PlanetCalculationsManager PlanetCalculationsManager;
    public PlanetArchaeologyManager PlanetArchaeologyManager;
    
    public OreBucket GetBucketByOreType(OreType oreType)
    {
        return OreBuckets.Single(e => e.BucketOreType == oreType);
    }

    public List<OreBucket> GetBucketsByOreTier(int oreTier)
    {
        return OreBuckets.FindAll(e => e.OreTier == oreTier);
    }
}
