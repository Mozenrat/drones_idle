﻿using System.Collections.Generic;

public class PlanetBuildingManager
{
    private readonly Dictionary<BuildingType, int> existingBuildings;

    public PlanetBuildingManager()
    {
        existingBuildings = new Dictionary<BuildingType, int>();
    }

    public Dictionary<BuildingType, int> GetAllBuildings()
    {
        return existingBuildings;
    }

    public void ConstructBuilding(BuildingType buildingType, int amount)
    {
        if (existingBuildings.ContainsKey(buildingType))
        {
            existingBuildings[buildingType] += amount;
        }
        else
        {
            existingBuildings.Add(buildingType, amount);
        }
    }

    public int GetBuildingAmount(BuildingType buildingType)
    {
        return existingBuildings.ContainsKey(buildingType) ? existingBuildings[buildingType] : 0;
    }
}
