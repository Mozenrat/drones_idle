using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

public class PlanetDroneManager
{
    public float TotalPlanetBandwidthUsage;

    private readonly Dictionary<string, int> activeDrones;
    private readonly Dictionary<string, (float, float)> droneStatsDictionary;

    private readonly BonusController bonusController;
    private readonly PlanetContents thisPlanet;

    private readonly StringBuilder stringBuilder;

    public PlanetDroneManager(BonusController bonusController, PlanetContents planet)
    {
        this.bonusController = bonusController;
        thisPlanet = planet;
        
        activeDrones = new Dictionary<string, int>();
        droneStatsDictionary = new Dictionary<string, (float, float)>(100);
        
        stringBuilder = new StringBuilder(200);
    }

    public void AssignDronesToRole(DroneRole mainRole, OreType oreRole, ProductionRecipeType productionRole, int droneCountToAssign)
    {
        var hash = CalculateDroneRoleHash(mainRole, oreRole, productionRole);

        var planetDroneCount = (int) thisPlanet.PlanetStorageManager.GetUnitsStored(StorableType.WorkerDrone);
        if (planetDroneCount <= 0)
        {
            NotificationController.ShowNotification_Static(DataValues.NoFreeDronesAvailableText, true);
            return;
        }

        var actualAssignCount = droneCountToAssign > planetDroneCount ? planetDroneCount : droneCountToAssign;
        thisPlanet.PlanetStorageManager.SubtractUnitsFromStorage(StorableType.WorkerDrone, actualAssignCount);
        
        if (activeDrones.ContainsKey(hash))
        {
            activeDrones[hash] += actualAssignCount;
        }
        else
        {
            activeDrones.Add(hash, actualAssignCount);
        }
        
        droneStatsDictionary.Clear();
    }

    public void UnassignDronesFromRole(DroneRole mainRole, OreType oreRole, ProductionRecipeType productionRole, int droneCountToAssign)
    {
        var hash = CalculateDroneRoleHash(mainRole, oreRole, productionRole);
        
        if (!activeDrones.ContainsKey(hash)) 
            return;

        activeDrones[hash] -= droneCountToAssign;
        if (activeDrones[hash] <= 0) 
            activeDrones.Remove(hash);
        
        thisPlanet.PlanetStorageManager.AddUnitsToStorage(StorableType.WorkerDrone, droneCountToAssign);
        
        droneStatsDictionary.Clear();
    }

    public int GetAssignedDronesCount(DroneRole mainRole, OreType oreRole, ProductionRecipeType productionRole)
    {
        var hash = CalculateDroneRoleHash(mainRole, oreRole, productionRole);

        return !activeDrones.ContainsKey(hash) ? 0 : activeDrones[hash];
    }

    public int GetUnassignedDronesCount()
    {
        return (int) thisPlanet.PlanetStorageManager.GetUnitsStored(StorableType.WorkerDrone);
    }

    /// <summary>
    /// Get Drones stats
    /// </summary>
    /// <returns>1 - Drones Amount, 2 - Work Speed Total</returns>
    public (float, float) GetDroneStats(DroneRole mainRole, OreType oreRole, ProductionRecipeType productionRole)
    {
        //TODO: move all stat getters into bonus controller probably
        var hash = CalculateDroneRoleHash(mainRole, oreRole, productionRole);

        if (!activeDrones.ContainsKey(hash))
            return (0f, 0f);
        
        if (droneStatsDictionary.ContainsKey(hash))
        {
            return droneStatsDictionary[hash];
        }

        float dronesAmount = activeDrones[hash];
        float workSpeed = bonusController.GetDroneWorkSpeed() * dronesAmount;
        
        droneStatsDictionary.Add(hash, (dronesAmount, workSpeed));
        return (dronesAmount, workSpeed);
    }
    
    public Dictionary<string, int> GetAllDronesData()
    {
        return activeDrones;
    }

    public void ParseDroneHashToEnums(string hash, out DroneRole mainRole, out OreType oreRole, out ProductionRecipeType productionRole)
    {
        var matches = Regex.Split(hash, "-");
        
        mainRole = (DroneRole) int.Parse(matches[0]);
        oreRole = (OreType) int.Parse(matches[1]);
        productionRole = (ProductionRecipeType) int.Parse(matches[2]);
    }

    private string CalculateDroneRoleHash(DroneRole mainRole, OreType oreRole, ProductionRecipeType productionRole)
    {
        stringBuilder.Clear();
        stringBuilder.Append((int) mainRole).Append("-");
        stringBuilder.Append((int) oreRole).Append("-");
        stringBuilder.Append((int) productionRole);
        return stringBuilder.ToString();
    }
}