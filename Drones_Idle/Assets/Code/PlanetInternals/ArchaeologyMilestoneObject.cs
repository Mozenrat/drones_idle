using UnityEngine;

public class ArchaeologyMilestoneObject
{
    public string MilestoneDisplayName;
    public ArchaeologyMilestoneType MilestoneType;

    public bool IsUnlocked;
    public int MilestoneLevel;
    
    public float MilestoneTimeToNextLevel
    {
        get
        {
            if (thisPlanetManager.GetArchaeologyMilestoneIncrement <= 0f) return -1f;
            return (MilestoneNextLevelCost - MilestoneCurrentPointsProgress) / thisPlanetManager.GetArchaeologyMilestoneIncrement;
        }
    }
    public float MilestoneCurrentPointsProgress { get; private set; }
    public float MilestoneNextLevelCost { get; private set; }

    private readonly PlanetArchaeologyManager thisPlanetManager;

    public ArchaeologyMilestoneObject(PlanetArchaeologyManager planetManager, ArchaeologyMilestoneType milestoneType)
    {
        thisPlanetManager = planetManager;
        
        var milestoneInstance = DataInstanceFactory.GetArchaeologyMilestoneInstance(milestoneType);
        MilestoneDisplayName = milestoneInstance.MilestoneDisplayName;
        MilestoneType = milestoneType;

        if (milestoneType != ArchaeologyMilestoneType.InitialDig)
        {
            thisPlanetManager.OnMilestoneLevelChanged += CalculateIsUnlocked;
        }
        else
        {
            IsUnlocked = true;
        }
        
        MilestoneLevel = 0;
        
        MilestoneNextLevelCost = CalculateNextLevelCost();
        MilestoneCurrentPointsProgress = 0f;
    }

    public void AddProgress(float pointDelta)
    {
        MilestoneCurrentPointsProgress += pointDelta;
        if (MilestoneCurrentPointsProgress >= MilestoneNextLevelCost)
        {
            MilestoneLevel++;
            MilestoneNextLevelCost = CalculateNextLevelCost();
            MilestoneCurrentPointsProgress = 0f;
            
            thisPlanetManager.TriggerMilestoneLevelUp(MilestoneType, MilestoneLevel);
        }
    }

    public void InjectData(int injectedLevel, float injectedProgress)
    {
        MilestoneLevel = injectedLevel;
        MilestoneNextLevelCost = CalculateNextLevelCost();
        MilestoneCurrentPointsProgress = injectedProgress;
    }

    private void CalculateIsUnlocked(object sender, PlanetArchaeologyManager.OnMilestoneLevelChangedEventArgs e)
    {
        if (IsUnlocked == true || 
            e.MilestoneType == this.MilestoneType || 
            e.MilestoneType != ArchaeologyMilestoneType.InitialDig) return;
        
        var milestoneInstance = DataInstanceFactory.GetArchaeologyMilestoneInstance(MilestoneType);
        IsUnlocked = milestoneInstance.MilestoneUnlockDepth <= e.NewLevel;
        if (IsUnlocked)
        {
            thisPlanetManager.TriggerNewMilestoneUnlocked(this);
        }
    }

    private float CalculateNextLevelCost()
    {
        return GlobalGameStateController.Instance.BonusController.GetArchaeologyMilestoneNextLevelCost(MilestoneType, MilestoneLevel);
    }
}