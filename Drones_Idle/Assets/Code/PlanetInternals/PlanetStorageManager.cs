﻿using System.Collections.Generic;
using System;
using System.Linq;

public class PlanetStorageManager
{
    public class OnPlanetStorageContentChangedEventArgs : EventArgs
    {
        public IStorable Storable { get; private set; }
        public float Quantity { get; private set; }

        public void ChangeEventArgsData(IStorable newStorable, float newQuantity)
        {
            Storable = newStorable;
            Quantity = newQuantity;
        }
    }

    public EventHandler<OnPlanetStorageContentChangedEventArgs> OnPlanetStorageContentChanged;
    private readonly OnPlanetStorageContentChangedEventArgs contentChangedEventArgs;
    
    private readonly Dictionary<StorableType, float> StorageContents;
    private readonly Dictionary<StorableType, IStorable> allStorableInstances;

    public PlanetStorageManager()
    {
        StorageContents = new Dictionary<StorableType, float>();
        contentChangedEventArgs = new OnPlanetStorageContentChangedEventArgs();
        
        var storablesEnumArray = (StorableType[]) Enum.GetValues(typeof(StorableType));
        allStorableInstances = new Dictionary<StorableType, IStorable>();
        
        for (var i = 0; i < storablesEnumArray.Length; i++)
        {
            if (storablesEnumArray[i] == StorableType.None) continue;
            var storableInstance = DataInstanceFactory.GetStorableInstance(storablesEnumArray[i]);
            allStorableInstances[storablesEnumArray[i]] = storableInstance;
        }
    }
    
    public void AddUnitsToStorage(StorableType storableType, float unitAmount)
    {
        var storableInstance = DataInstanceFactory.GetStorableInstance(storableType);
        
        if (StorageContents.ContainsKey(storableType))
        {
            StorageContents[storableType] += unitAmount;
            contentChangedEventArgs.ChangeEventArgsData(storableInstance, GetUnitsStored(storableType));
            OnPlanetStorageContentChanged?.Invoke(null, contentChangedEventArgs);
        }
        else
        {
            StorageContents.Add(storableType, unitAmount);
            contentChangedEventArgs.ChangeEventArgsData(storableInstance, GetUnitsStored(storableType));
            OnPlanetStorageContentChanged?.Invoke(null, contentChangedEventArgs);
        }
    }

    public void SubtractUnitsFromStorage(StorableType storableType, float unitAmount)
    {
        if (!StorageContents.ContainsKey(storableType) || StorageContents[storableType] < unitAmount)
        {
            throw new InvalidOperationException("Attempted to remove nonexisting StorableType from PlanetStorage!");
        }
        
        var storableInstance = DataInstanceFactory.GetStorableInstance(storableType);
        
        StorageContents[storableType] -= unitAmount;
        
        contentChangedEventArgs.ChangeEventArgsData(storableInstance, GetUnitsStored(storableType));
        OnPlanetStorageContentChanged?.Invoke(null, contentChangedEventArgs);
    }

    public float GetUnitsStored(StorableType storableType)
    {
        return !StorageContents.ContainsKey(storableType) ? 0f : StorageContents[storableType];
    }

    public float GetVolumeStored(StorableType storableType)
    {
        if (!StorageContents.ContainsKey(storableType)) return 0f;
        return StorageContents[storableType] * allStorableInstances[storableType].UnitVolume;
    }

    public float GetTotalCategoryVolumeStored(StorableCategory storableCategory)
    {
        var filtered = allStorableInstances.Values.Where(item => item.StorableCategory == storableCategory).ToList();
        float total = 0f;
        for (int i = 0; i < filtered.Count; i++)
        {
            total += GetUnitsStored(filtered[i].StorableType) * filtered[i].UnitVolume;
        }
        return total;
    }

    public float GetTotalVolumeStored()
    {
        float total = 0f;
        foreach (var item in StorageContents)
        {
            total += allStorableInstances[item.Key].UnitVolume * item.Value;
        }
        return total;
    }

    public StorableType[] GetStoredTypes()
    {
        return StorageContents.Keys.ToArray();
    }
}