using System;
using System.Collections.Generic;
using UnityEngine;

public class WarfareController
{
    public class OnWarfareNewFightStartedEventArgs : EventArgs
    {
        public PlayerVesselFleet PlayerFleet { get; }
        public VesselFleet EnemyFleet { get; }

        public OnWarfareNewFightStartedEventArgs(PlayerVesselFleet playerFleet, VesselFleet enemyFleet)
        {
            PlayerFleet = playerFleet;
            EnemyFleet = enemyFleet;
        }
    }

    public class OnWarfareFleetDiedEventArgs : EventArgs
    {
        public PlayerVesselFleet PlayerFleet { get; }

        public OnWarfareFleetDiedEventArgs(PlayerVesselFleet playerFleet)
        {
            PlayerFleet = playerFleet;
        }
    }

    public EventHandler<OnWarfareFleetDiedEventArgs> OnWarfareFleetDied;
    public EventHandler<OnWarfareNewFightStartedEventArgs> OnWarfareNewFightStarted;

    public WarfareCalculationsManager WarfareCalculationsManager;
    
    public int HighestWarfareTierReached = 2;
    
    public List<WarfareZone> AvailableWarfareZones;

    public void ChangeActiveWarfareZoneForPlayerFleet(WarfareZone zone, PlayerVesselFleet playerVesselFleet)
    {
        playerVesselFleet.ActiveExpeditionZone = zone;
        GlobalGameStateController.Instance.MothershipEntity.OnPlayerFleetDataChanged?.Invoke(null,
            new MothershipEntity.PlayerFleetChangedEventArgs(playerVesselFleet));
        WarfareCalculationsManager.InitNewWarfareCalculation(zone, playerVesselFleet, zone.GetRandomEnemyFleet());
    }

    public void Init()
    {
        AvailableWarfareZones = WarfareData.GetAllZonesUpToTier(HighestWarfareTierReached);
    }
}