using System;
using System.Collections.Generic;

public class PlayerVesselFleet : VesselFleet
{
    public WarfareZone ActiveExpeditionZone;

    public PlayerVesselFleet(){}
    
    public PlayerVesselFleet(Dictionary<VesselType, int> fleetComposition, string fleetName) : base(fleetComposition, fleetName)
    {
        ActiveExpeditionZone = WarfareData.GetStagingZone();
    }

    public int GetVesselCount(VesselType vesselType)
    {
        return FleetComposition.ContainsKey(vesselType) ? FleetComposition[vesselType] : 0;
    }

    public void AddShipToFleet(VesselType vesselType, int quantity)
    {
        if (FleetComposition.ContainsKey(vesselType))
        {
            FleetComposition[vesselType] += quantity;
        }
        else
        {
            FleetComposition.Add(vesselType, quantity);
        }
        RecalculatePlayerFleetStats();
    }

    public void RemoveShipFromFleet(VesselType vesselType, int quantity)
    {
        if (FleetComposition[vesselType] < quantity)
        {
            throw new ArgumentException("Tried to remove non-existing ships from fleet!");
        }
        
        FleetComposition[vesselType] -= quantity;
        if (FleetComposition[vesselType] == 0)
        {
            FleetComposition.Remove(vesselType);
        }
        RecalculatePlayerFleetStats();
    }

    public void ProcessPlayerFleetRegenZone0()
    {
        if (CurrentShieldHp < FleetShieldCapacity)
        {
            CurrentShieldHp += FleetShieldRegenValue * 4;
        }

        if (CurrentHullHp < FleetHullHitPoints)
        {
            CurrentHullHp += FleetShieldRegenValue;
        }
    }
    
    private void RecalculatePlayerFleetStats()
    {
        FleetStats[0] = 0f;
        FleetStats[1] = 0f;
        FleetStats[2] = 0f;
        FleetStats[3] = 0f;
        
        foreach (var fleetMember in FleetComposition)
        {
            var statsObject = DataInstanceFactory.GetCombatVesselInstance(fleetMember.Key);
            
            FleetStats[0] += statsObject.BaseAttackPower * fleetMember.Value;
            FleetStats[1] += statsObject.BaseShieldCapacity * fleetMember.Value;
            FleetStats[2] += statsObject.BaseHullHitPoints * fleetMember.Value;
            FleetStats[3] += statsObject.BaseShieldRegenValue * fleetMember.Value;
        }
    }
}