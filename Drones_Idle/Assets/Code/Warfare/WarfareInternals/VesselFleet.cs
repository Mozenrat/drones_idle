using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class VesselFleet
{
    public readonly Dictionary<VesselType, int> FleetComposition;

    public int FleetTotalVesselCount => FleetComposition.Values.Sum();

    public string FleetName;

    public float FleetAttackPower => FleetStats[0];
    public float FleetShieldCapacity => FleetStats[1];
    public float FleetHullHitPoints => FleetStats[2];
    public float FleetShieldRegenValue => FleetStats[3];

    public float CurrentShieldHp { get; protected set; }
    public float CurrentHullHp { get; protected set; }

    public bool IsAlive => CurrentHullHp > 0;

    /// <summary>
    /// 0 - Attack Power, 1 - Shield HP, 2 - Hull HP, 3 - Shield Regen
    /// </summary>
    protected readonly Dictionary<int, float> FleetStats;

    public VesselFleet()
    {
        FleetComposition = new Dictionary<VesselType, int>();
        FleetStats = new Dictionary<int, float>();
        RecalculateFleetStats();
    }

    public VesselFleet(Dictionary<VesselType, int> fleetComposition, string fleetName)
    {
        FleetName = fleetName;
        FleetComposition = fleetComposition;
        FleetStats = new Dictionary<int, float>();
        RecalculateFleetStats();
    }

    public void TakeDamage(float damageTaken)
    {
        if (CurrentShieldHp >= damageTaken)
        {
            CurrentShieldHp -= damageTaken;
            if (CurrentShieldHp < 0) CurrentShieldHp = 0f;
            
            return;
        }

        CurrentShieldHp = 0f;
        var transferredToHull = Mathf.Abs(CurrentShieldHp - damageTaken);
        CurrentHullHp -= transferredToHull;
        
        if (CurrentHullHp < 0) CurrentHullHp = 0f;
    }

    public void ProcessRegen()
    {
        if (CurrentShieldHp < FleetShieldCapacity)
        {
            CurrentShieldHp += FleetShieldRegenValue;
        }
    }

    public void ResetFleetHp()
    {
        CurrentShieldHp = FleetStats[1];
        CurrentHullHp = FleetStats[2];
    }

    private void RecalculateFleetStats()
    {
        FleetStats[0] = 0f;
        FleetStats[1] = 0f;
        FleetStats[2] = 0f;
        FleetStats[3] = 0f;
        
        foreach (var fleetMember in FleetComposition)
        {
            var statsObject = DataInstanceFactory.GetCombatVesselInstance(fleetMember.Key);
            
            FleetStats[0] += statsObject.BaseAttackPower * fleetMember.Value;
            FleetStats[1] += statsObject.BaseShieldCapacity * fleetMember.Value;
            FleetStats[2] += statsObject.BaseHullHitPoints * fleetMember.Value;
            FleetStats[3] += statsObject.BaseShieldRegenValue * fleetMember.Value;
        }

        CurrentShieldHp = FleetStats[1];
        CurrentHullHp = FleetStats[2];
    }
}