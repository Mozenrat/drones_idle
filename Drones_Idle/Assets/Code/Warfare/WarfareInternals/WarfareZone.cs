using System.Collections.Generic;
using UnityEngine;

public class WarfareZone
{
    public string ZoneName { get; }
    public int ZoneTier { get; }
    public List<VesselFleet> EnemyFleets { get; }
    
    public readonly Dictionary<float, WarfareLootObject> Loot;
    
    public WarfareZone(string zoneName, int zoneTier, List<VesselFleet> enemyFleets, Dictionary<float, WarfareLootObject> lootTable)
    {
        ZoneName = zoneName;
        ZoneTier = zoneTier;
        EnemyFleets = enemyFleets;
        Loot = lootTable;
    }

    public VesselFleet GetRandomEnemyFleet()
    {
        if (EnemyFleets.Count == 0) return null;
        
        var value = Random.Range(0, EnemyFleets.Count);
        return EnemyFleets[value];
    }
}

public class WarfareLootObject
{
    public readonly StorableType StorableType;
    public readonly string StorableName;
    public readonly float Quantity;

    public WarfareLootObject(StorableType storableType, float quantity)
    {
        var storableInstance = DataInstanceFactory.GetStorableInstance(storableType);
        StorableName = storableInstance.StorableName;
        StorableType = storableType;
        Quantity = quantity;
    }
}