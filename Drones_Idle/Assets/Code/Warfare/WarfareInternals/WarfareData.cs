using System.Collections.Generic;

public static class WarfareData
{
    private static readonly List<WarfareZone> AllZones = new List<WarfareZone>
    {
        GetT0Zone(),
        GetT1Zone(),
        GetT2Zone()
    };

    private static WarfareZone GetT0Zone()
    {
        return new WarfareZone("Safe Space", 0, new List<VesselFleet>(), null);
    }

    private static WarfareZone GetT1Zone()
    {
        var fleets = new List<VesselFleet>
        {
            new VesselFleet(new Dictionary<VesselType, int> {[VesselType.StarterFighter] = 3}, "temp fleet name zone1, fleet1"),
            new VesselFleet(new Dictionary<VesselType, int> {[VesselType.StarterFighter] = 4},"temp fleet name zone1, fleet2")
        };
        var lootTable = new Dictionary<float, WarfareLootObject>
        {
            [50f] = new WarfareLootObject(StorableType.IronMineral, 4),
            [3f] = new WarfareLootObject(StorableType.Cable, 1),
            [0.2f] = new WarfareLootObject(StorableType.WorkerDrone, 1),
            [0.12f] = new WarfareLootObject(StorableType.AlienShipWeaponT1, 1),
            [0.11f] = new WarfareLootObject(StorableType.AlienShipHullT1, 1),
            [0.1f] = new WarfareLootObject(StorableType.AlienShipShieldT1, 1)
        };
        var zone = new WarfareZone("Warzone 1", 1, fleets, lootTable);
        return zone;
    }

    private static WarfareZone GetT2Zone()
    {
        var fleets = new List<VesselFleet>
        {
            new VesselFleet(new Dictionary<VesselType, int> {[VesselType.StarterFighter] = 1, [VesselType.AdvancedFighter] = 1}, "temp fleet name zone 2")
        };
        var lootTable = new Dictionary<float, WarfareLootObject>
        {
            [65f] = new WarfareLootObject(StorableType.IronMineral, 9),
            [60f] = new WarfareLootObject(StorableType.CopperMineral, 9),
            [20f] = new WarfareLootObject(StorableType.Cable, 4),
            [3.5f] = new WarfareLootObject(StorableType.WorkerDrone, 1)
        };
        var zone = new WarfareZone("Warzone 2", 2, fleets, lootTable);
        return zone;
    }

    public static List<WarfareZone> GetAllZonesUpToTier(int zoneTier)
    {
        return AllZones.GetRange(0, zoneTier + 1);
    }

    public static WarfareZone GetStagingZone()
    {
        return AllZones[0];
    }

    public static WarfareZone GetZoneByTier(int tier)
    {
        return AllZones[tier];
    }
}